<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<img src="http://www.roadandbike.com/images/fbimage.jpg" style="display:none" /><!--google+-->
<?php 
require_once('Connections/dbConn.php');//DB key
if ($page == "membcareer" || $page == "membrecord" || $page == "membfriend"){
	$MemberID = $_GET['ID']; 
	$FBresult =  mysql_query(" SELECT * FROM tb_members as m WHERE m.MemberID = '$MemberID' ")	or die(mysql_error());
	$FBtitle = mysql_fetch_assoc($FBresult);
}else
if ($page == "teamcareer" || $page == "teamrecord" || $page == "teamfriend"){
	$TeamID = $_GET['ID']; 
	$FBresult =  mysql_query(" SELECT * FROM tb_team as t WHERE t.TeamID = '$TeamID' ") or die(mysql_error());
	$FBtitle = mysql_fetch_assoc($FBresult);
}else
if ($page == "races" || $page == "racecreator" || $page == "team" || $page == "rider"){
	$FBtitle = "Taiwan";
}else
if ($page == "rankY" || $page == "rankT" || $page == "rankR"){
	$FBtitle = "Taiwan";
}else
if ($page == "race" || $page == "racerank"){
	$RaceID = $_GET['ID']; 
	$FBresult =  mysql_query(" SELECT * FROM tb_race as r WHERE r.RaceID = '$RaceID' ") or die(mysql_error());
	$FBtitle = mysql_fetch_assoc($FBresult);
}

switch ($page){//web & share(facebook, google+) title
	case "index":
		$webTitle = "RoadandBike - Tour Your Life";
		break;
		
	case "signup":
		$webTitle = "註冊為車手 - RoadandBike";
		break;
		
	case "signupok":
		$webTitle = "歡迎加入 RoadandBike";
		break;

	case "races":
		$webTitle = $FBtitle." 賽事 - RoadandBike";
		break;
		
	case "racecreator":
		$webTitle = $FBtitle." 賽事 - RoadandBike";
		break;

	case "race":
		$webTitle = $FBtitle['Title']." - RoadandBike";
		break;
		
	case "racecreate":
		$webTitle = "舉辦賽事 - RoadandBike";
		break;

	case "racerank":
		$webTitle = $FBtitle['Title']." 的單站排名 - RoadandBike";
		break;

	case "rider":
		$webTitle = $FBtitle." 車手 - RoadandBike";
		break;

	case "team":
		$webTitle = $FBtitle." 車隊 - RoadandBike";
		break;
		
	case "teamcreate":
		$webTitle = "成立車隊 - RoadandBike";
		break;

	case "rankY":
		$webTitle = $FBtitle." 黃衫排名 - RoadandBike";
		break;

	case "rankT":
		$webTitle = $FBtitle." 團隊排名 - RoadandBike";
		break;

	case "rankP":
		$webTitle = $FBtitle." 粉紅衫排名 - RoadandBike";
		break;

	case "membcareer":
		$webTitle = $FBtitle['MemberName']." 的車手生涯 - RoadandBike";
		break;

	case "membrecord":
		$webTitle = $FBtitle['MemberName']." 的戰績 - RoadandBike";
		break;
		
	case "membfriend":
		$webTitle = $FBtitle['MemberName']." 的車友 - RoadandBike";
		break;
		
	case "teamcareer":
		$webTitle = $FBtitle['MemberName']." 的生涯 - RoadandBike";
		break;

	case "teamrecord":
		$webTitle = $FBtitle['MemberName']." 的戰績 - RoadandBike";
		break;
		
	case "teamfriend":
		$webTitle = $FBtitle['MemberName']." 的車友 - RoadandBike";
		break;

	case "settings":
		$webTitle = "帳戶設定 - RoadandBike";
		break;
		
	case "settingsrider":
		$webTitle = "車手設定 - RoadandBike";
		break;
		
	case "settingsbike":
		$webTitle = "單車設定 - RoadandBike";
		break;
		
	case "settingsteam":
		$webTitle = "車隊設定 - RoadandBike";
		break;
	
	default:
		$webTitle = "RoadandBike - Tour Your Life";
		break;
}
?>
<title><?php echo $webTitle; ?></title><!--for web-->
<meta property="og:title" content="<?php echo $webTitle; ?>" /><!--for facebook & google+-->
<meta property="og:image" content="http://www.roadandbike.com/images/fbimage.jpg" /><!--facebook-->
<meta property="og:type" content="website" />
<meta property="og:url" content="http://www.roadandbike.com" />
<meta property="og:description" content="加入R&B, 跟著我們一起征服您的城市, 成為街頭達人! 比賽不用人擠人, 在這裡天天有比賽, 處處是戰場,您還可以自訂規則, 在您拿手的路段跟各路高手一較高下!" />
<meta name="keywords" content="軌跡、分享、單車、賽事、免費">
<link href="css/page_style.css" rel="stylesheet" type="text/css" /> 
<link rel="shortcut icon" type="image/x-icon" href="images/icon.png">
<!--no JavaScript-->
<noscript>
<META HTTP-EQUIV="Refresh" CONTENT="0;URL=noJavaScript.html">
</noscript>
<!--no JavaScript-->