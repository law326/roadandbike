<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php

// This code demonstrates how to lookup the country, region, city,
// postal code, latitude, and longitude by IP Address.
// It is designed to work with GeoIP/GeoLite City

// Note that you must download the New Format of GeoIP City (GEO-133).
// The old format (GEO-132) will not work.

include("Geoip/geoipcity.inc");
include("Geoip/geoipregionvars.php");

// uncomment for Shared Memory support
// geoip_load_shared_mem("/usr/local/share/GeoIP/GeoIPCity.dat");
// $gi = geoip_open("/usr/local/share/GeoIP/GeoIPCity.dat",GEOIP_SHARED_MEMORY);

//資料庫載入一定要用絕對路徑,否則找不到檔案
$geoipRoot = $_SERVER['DOCUMENT_ROOT'] . "/Geoip/GeoLiteCity.dat";
$gi = geoip_open($geoipRoot,GEOIP_STANDARD);
echo $_SERVER['DOCUMENT_ROOT']."<br>";
echo "您來自於：" . $_SERVER['REMOTE_ADDR'] . "<br>";

$record = geoip_record_by_addr($gi,$_SERVER['REMOTE_ADDR']);
print "●country_name：" . $record->country_name . " " ."<br>"; //country_code . " " . $record->country_code3 . " " .

print "●region" . $record->region ." ___" . $GEOIP_REGION_NAME[$record->country_code][$record->region] . "<br>";

print "●city：" . $record->city  ."<br>" ;
print "●postal_code：" .$record->postal_code ."<br>";
print "●latitude：" .$record->latitude ."<br>";
print "●longitude："   .$record->longitude  ."<br>";
print "●metro_code：" .$record->metro_code  ."<br>";
print "●area_code：" .$record->area_code  ."<br>"	;

geoip_close($gi);

?>
