<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php
$page = "races";
require_once('include_webtitle.php');//標題檔
?>
<?php

session_start();

require_once('Connections/autologin.php');//判斷訪客狀態(登入、未登入)，再決定您"目前所在的城市"資訊
require_once('Connections/find_LatLng.php');//由GeoID找出對應的經緯度 這裡給Gmap

$editFormAction = $_SERVER['PHP_SELF']; //目前正在執行的檔案

/* 經緯度轉換 這裡給Gmap用*/
$latlng = geo_to_latlng($GeoID);

//標題欄位
$result = mysql_query("SELECT SUM(Mileage), count(*) FROM tb_race WHERE GeoID='$GeoID' ")
	or die(mysql_error());
	$rowInfo = mysql_fetch_assoc($result);
	$Mileage = $rowInfo['SUM(Mileage)'];//計算累積里程
	$RaceNum = $rowInfo['count(*)'];//計算當地賽事
	
$now = date("Y-m-d");

$RaceNumEach = 20;//單次顯示數量

if (empty($_GET['Page'])){
	$p = 1;
}else{
	$p = $_GET['Page'];
}

$p = ($p -1) * $RaceNumEach;//讀取比賽數量指標

//MAP & 資訊欄位(顯示數量需要同步)
$resultMark = mysql_query("
	SELECT StartLon, StartLat
	FROM tb_race as r, tb_race_mcareer as rm 
	WHERE r.RaceID = rm.RaceID AND r.GeoID = '$GeoID' AND rm.Status = 1 AND r.RaceExpires >= '$now'
	GROUP BY rm.RaceID ORDER BY r.RaceExpires ASC ")
	or die(mysql_error());
$race_latlon = mysql_data_seek($resultMark, $p);
	
$resultMark2 = mysql_query("
	SELECT StartLon, StartLat
	FROM tb_race as r, tb_race_mcareer as rm 
	WHERE r.RaceID = rm.RaceID AND r.GeoID = '$GeoID' AND rm.Status = 1 AND r.RaceExpires >= '$now'
	GROUP BY rm.RaceID ORDER BY r.RaceExpires ASC ")
	or die(mysql_error());
$rowInfo_latlon2 = mysql_data_seek($resultMark2, $p);

$resultRaceInfo = mysql_query("
	SELECT Title, COUNT(MemberID), AVG(Evaluation), Mileage, RaceExpires, IsGroup, StartLon, StartLat, r.RaceID
	FROM tb_race as r, tb_race_mcareer as rm 
	WHERE r.RaceID = rm.RaceID AND r.GeoID = '$GeoID' AND rm.Status = 1 AND r.RaceExpires >= '$now'
	GROUP BY rm.RaceID ORDER BY r.RaceExpires ASC ")
	or die(mysql_error());//計算資訊
	$RaceNumInfo = mysql_num_rows($resultRaceInfo);//資訊欄位顯示的總數
	//每次抓的數量
if (($RaceNumInfo - $p) > $RaceNumEach){
	$RaceNumSeek = $RaceNumEach;
}else{//最後一頁的數量
	$RaceNumSeek = $RaceNumInfo - $p;
}
	$RacePage = ceil($RaceNumInfo / $RaceNumEach);//顯示頁數
	
	if ($_GET['Page'] > $RacePage) header("Location:  races.php");//頁碼超過實際數量時導回第一頁
	
	$RaceInfo = mysql_data_seek($resultRaceInfo, $p);
	

$resultRaceInfo2 = mysql_query("
	SELECT Title, COUNT(MemberID), AVG(Evaluation), Mileage, RaceExpires, IsGroup, StartLon, StartLat, r.RaceID
	FROM tb_race as r, tb_race_mcareer as rm 
	WHERE r.RaceID = rm.RaceID AND r.GeoID = '$GeoID' AND rm.Status = 1 AND r.RaceExpires >= '$now'
	GROUP BY rm.RaceID ORDER BY r.RaceExpires ASC ")
	or die(mysql_error());//計算資
	$rowInfo_msg = mysql_data_seek($resultRaceInfo2, $p);

list($Country_reg,$City_reg) = geoid_to_city( $GeoID); //將會員的GeoID轉換成City

//車友動態
$resultFriend = mysql_query("SELECT * FROM tb_race as r, tb_race_mcareer as rm, tb_members as m, tb_bike as b, tb_follow_member as fm, tb_bike_brand as bb, tb_bike_model as bm
	WHERE b.BrandID = bb.BrandID AND b.ModelID = bm.ModelID AND m.MemberID = rm.MemberID AND m.MemberID = b.MemberID AND m.MemberID = fm.BeFID AND r.RaceID=rm.RaceID AND r.GeoID='$GeoID' AND fm.FID = '$MemberID' ORDER BY rand() LIMIT 5 ")
	or die(mysql_error());
	$FriendNum = mysql_num_rows($resultFriend);//找出關注車手

//最新賽況
$resultRider = mysql_query("SELECT * FROM tb_race as r, tb_race_mcareer as rm, tb_members as m
	WHERE (m.MemberID=rm.MemberID AND r.RaceID=rm.RaceID AND r.GeoID='$GeoID' AND rm.Status=1) ORDER BY rand() LIMIT 5 ")
	or die(mysql_error());
	$RiderNum = mysql_num_rows($resultRider);//找出完賽車手


?>
<!--google map-->
<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.6/jquery-ui.min.js"></script>

<script type="text/javascript" charset="utf-8">

$(function(){        
	var burnsvilleMN = new google.maps.LatLng(<?php echo $latlng ;?>);
	// Creating a map
    var map = new google.maps.Map($('#map')[0], {
          zoom: 10,
          center: burnsvilleMN,
          disableDefaultUI: true,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        });	
		       
    var boundsChange = google.maps.event.addListener(map, "bounds_changed", function() {
				//地圖置中
		
  		<?php
while ($race_latlon = mysql_fetch_assoc($resultMark))	{
		$m++;
		$lats=$race_latlon['StartLat'];
		$lons=$race_latlon['StartLon'];
		
		$non1=$non1.$lats.",";
		$non2=$non2.$lons.",";
		if ($m == $RaceNumEach) break;
	}
	echo " var lats=[".$non1."]; var lons=[".$non2."]; var size=".$RaceNumSeek."; ";
?>
if (size > 0){
var latsmin = lats[0], latsmax = lats[0];
	for (var i = 1; i < size; i++){
		if (latsmax < lats[i]){
			latsmax = lats[i];
		}else if(latsmin > lats[i]){
			latsmin = lats[i];
		}
	}
	var lonsmin = lons[0], lonsmax = lons[0];
	for (var i = 1; i < size; i++){
		if (lonsmax < lons[i]){
			lonsmax = lons[i];
		}else if(lonsmin > lons[i]){
			lonsmin = lons[i];
		}
	}

	var southWest = new google.maps.LatLng(latsmin,lonsmin);
  	var northEast = new google.maps.LatLng(latsmax,lonsmax);
  	var bounds = new google.maps.LatLngBounds(southWest,northEast);
  	map.fitBounds(bounds);
}//地圖置中結束
  		var markers = [];
  		for (var i = 0; i < <?php echo $RaceNumSeek;?>; i++) {
			var latLng = [
				<?php while ($rowInfo_latlon2 = mysql_fetch_assoc($resultMark2)) {  ?> 
					new google.maps.LatLng(<?php echo $rowInfo_latlon2['StartLat']; ?>,<?php echo $rowInfo_latlon2['StartLon']; ?>),
				<?php }//while End ?> 
			];
  				
			var image = 'images/cycling.png';	
			var marker = new google.maps.Marker({
				position: latLng[i],
				map: map,
				icon: image
			});
				  
			markers[i] = marker;

		}
	
		google.maps.event.removeListener(boundsChange);
					
		$(markers).each(function(i,marker){
			$("<li />").addClass('ui-state-default ui-corner-all')
			.html(+i)//.html("Point "+i)
			.click(function(){
				message.open(marker, i);
			})
			.appendTo("#list" + i);
	
			google.maps.event.addListener(marker, "mouseover", function(){
				message.open(this, i);
			});
		});
  	});
	function MessageWindow(){
  		this.setMap(map);
	}
	MessageWindow.prototype = new google.maps.OverlayView();
	MessageWindow.prototype.onAdd = function() {
		this.$div_ = $('<div id="message" />').appendTo(this.getPanes().floatPane);//.getPanes()傳回能繪製此 OverlayView 的窗格
	}
	MessageWindow.prototype.draw = function() {
		var me = this;
    	var $div = this.$div_;
    	var point = this.getProjection().fromLatLngToDivPixel(this.latlng_);
    	if (point) {
        	$div.css({ left: point.x, top: point.y});//框的位置(x-100/y-120)
		}
	};
	var openTimeout;
	MessageWindow.prototype.open = function(marker, index) {

    	clearTimeout(openTimeout);
		$div = this.$div_.stop().css('opacity',1).hide().empty();
		this.latlng_ = marker.position;
    	this.draw();
        
			
		
		var msgbox_i = "#tabs-template" + index ;
		var closeButton = $(msgbox_i).clone().show().appendTo($div);
		//alert(index);

		var left = map.getBounds().getSouthWest().lat();
		var right = map.getBounds().getNorthEast().lat();
		var offset = (right - left) * 0.25;//視窗的位置
		var newCenter = new google.maps.LatLng(marker.position.lat(), marker.position.lng()+offset);//控制點的座標
		map.panTo(newCenter);
		openTimeout = setTimeout(function(){
			$div.show("drop", { direction:"right" });//滑入方向
		}, 50);//速度
	};
	var message = new MessageWindow();
	
});

function iconHTML(type) {
	switch (type) {
		case "close"	: iconClass = 'ui-icon-closethick'; break;
	}	
}
</script>
<style type="text/css">
	#message{ position:relative; padding: 5px; }
	.mes{ background-color: #fff; width: 192px; padding: 5px; }
	.map_pic{ float: left; margin-right: 5px; height: 50px; width: 50px; }
	.map_text{ line-height: 16px; margin-bottom: 2px; }
	.map_bar{ float: right;	width: 200px; }	
	.map_pics{ float: left; margin-right: 2px; width: 30px; height: 30px; }
</style>
<!--google map-->
<!--所在地區-->
<script src="CollapsiblePanel/CollapsiblePanel.js" type="text/javascript"></script>
<!--所在地區-->


</head>

<body>
<?php require_once('include_header.php'); ?>     
		<div id="container">
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
            	<tr>
                	<td valign="top">
                    	<div id="main">
                            <div id="title">
                            	<div class="button"><a href="races.php" class="word_type_bb14">動態</a>．<a href="races_creator.php" class="word_type_bb14">地頭蛇</a></div>
                           	  <div class="button"></div>
                            <div class="text word_type_bb24"><?php echo $City_reg; ?>, 賽事</div>
                                <div class="box">累積賽事 <span class="word_type_blueb12"><a><?php echo $RaceNum ;?></a></span> 場．當地賽事累積里程 <span class="word_type_blueb12"><a><?php if (empty($Mileage)){ echo "0";}else{ echo $Mileage ;}?></a></span> km</div>
                                
                                    
                                
                        </div>
                            <?php if ($_SESSION["islogin"] == FALSE) require_once('tips/tips.html'); ?>	
                            <div id="intro">
<div class="float">
	<div id="map"></div>
	<!--地圖的訊息欄位-->                  
<?php
	$msg_i=0;
	while ($rowInfo_msg = mysql_fetch_assoc($resultRaceInfo2)) {  
	$n++;
?> 
	<div id="tabs-template<?php echo $msg_i; ?>" style="display:none">
<?php $msg_i = $msg_i + 1; ?>
		<div class="back_sb"></div>
		<div class="map_block mes">
			<table width="100%" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<div class="map_text" style=" height: 16px; overflow: hidden; width:190px;">
                        	<a href="race.php?ID=<?php echo $rowInfo_msg['RaceID']; ?>" id="RaceMsg" class="word_type_bb14"><?php echo $rowInfo_msg['Title']; ?></a>
						</div>
						<div class="button"></div>
						<div class="map_text word_type_g12">完成車手 <?php echo $rowInfo_msg['COUNT(MemberID)']; ?></div>
					</td>
				</tr>
			</table>
		</div>
	</div>
<?php 
		if ($n == $RaceNumEach) break;
	}  
	
?> 
</div>
                            	<div class="cube"></div>
                            </div>
                            <?php require_once('share_box.php'); ?>
                            <div id="event">                           
                            <?php
                      	
                      	if ($RaceNum > 0){
                        //"有關注車手數量"
                        $list=0;
                        while (($RaceInfo = mysql_fetch_assoc($resultRaceInfo)) ) {  
							$OnePage++;          			
                        ?>  
                              <div class="block">
                                <table width="100%" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td>
                                    	<div class="bar">
                                        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr>
                                                <td valign="middle"><div class="boxfloat word_type_g12"><a  class="word_type_bb24"><?php echo $RaceInfo['COUNT(MemberID)']; ?></a><br />
                                                    完成車手</div>
                                                  <div class="boxfloat word_type_g12"><a  class="word_type_bb24"><?php echo floor($RaceInfo['AVG(Evaluation)']); ?>/5</a><br />
                                                    困難度</div>
                                                  <div class="boxfloat word_type_g12"><a  class="word_type_bb24"><?php echo $RaceInfo['Mileage']; ?></a><br />
                                                  總里程 (km)</div>
                                                  <div class="boxfloat word_type_g12"><a  class="word_type_bb24"><?php 
												  $EndDate = $RaceInfo['RaceExpires'];//與副程式連結
require('upLoadTrack/Timer.php');//時間計數器
												   if ($Timer == false){
	echo "End";
}else{
	if ($HaveD > 0){ echo $HaveD."day" ;}elseif ($HaveH > 0){ echo $HaveH."h" ;}elseif ($HaveM > 0){ echo $HaveM."m" ;}else{ echo $HaveS."s" ;}
}   ?>
                                                  
                                                  </a><br />
                                                關門時間</div></td>
                                                <td><span class="buttons"><a id="list<?php echo $list; ?>" class="word_type_wb18"></a></span></td>
                                              </tr>
                                            </table>
                                        </div>
                                      <div style=" height: 16px; overflow: hidden;" class="text"><a href="race.php?ID=<?php echo $RaceInfo['RaceID']; ?>" class="word_type_bb14"><?php echo $RaceInfo['Title']; ?></a></div>
                                      <div class="button"></div>
                                      <div class="text">計時賽</div>
                                      <div class="text word_type_g12"><span class="text"><?php if ($RaceInfo['IsGroup']==0){ echo "個人"; }else{ echo "團隊" ; } ?></span></div></td>
                                  </tr>
                                </table>
                              </div>
                              <?php
							  $list++;		
							  if ($OnePage == $RaceNumEach) break;				
									}//while End
								}
								else{
									?>
                                    <div class="block">
                                <table width="100%" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td>
                                    <?php
									echo "本地還沒有任何比賽！";
									?>
                                    </td>
                                  </tr>
                                </table>
                                </div>
                                    <?php
								}//if ($result) End
								
						?> 
                        <?php if ($RaceNumInfo > $RaceNumEach){ if ($_GET['Page'] == 0 || empty($_GET['Page']))$_GET['Page'] = 1;  $next = $_GET['Page'] + 1; $prev = $_GET['Page'] - 1; ?>
                        <div align="center" style="padding:10px; background-color:#f8f8f8;">
							
                            <span style="padding:10px;"><a <?php if ($_GET['Page'] > 1){ ?>href="<?php echo "races.php?Page=".$prev; ?>"<?php } ?>>上一頁</a></span>
                            
                       		<span style="padding:10px;">．<?php for ($f=1; $f<$RacePage+1; $f++){ ?><a <?php if ($_GET['Page'] == $f) echo " style=\"font-size:18px\" "; ?> href="races.php?Page=<?php echo $f; ?>"><?php echo $f;　?></a>．<?php } ?></span>
							
                            <span style="padding:10px;"><a <?php if ( $_GET['Page'] < $RacePage){ ?>href="<?php echo "races.php?Page=".$next; ?>" <?php ; } ?>>下一頁</a></span></div>
                            
                        <?php } ?>
                            </div>
                        </div>
                    </td>
                    <td valign="top">
                        <?php require_once('include_races_rightSide.php'); ?>
                	</td>
            	</tr>
			</table>
		</div>
		<?php require_once('include_footer.php'); ?>

	</div>
</body>
</html>
