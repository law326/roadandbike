<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php  
$Mode = $_GET['Mode'];

$directory_self = str_replace(basename($_SERVER['PHP_SELF']), '', $_SERVER['PHP_SELF']);

if($Mode == "create"){
	$uploadsDirectory = $_SERVER['DOCUMENT_ROOT'] . $directory_self . 'team_pic/';
}else if($Mode == "bike"){
	$uploadsDirectory = $_SERVER['DOCUMENT_ROOT'] . $directory_self . 'bike_pic/';
}else if($Mode == "team"){
	$uploadsDirectory = $_SERVER['DOCUMENT_ROOT'] . $directory_self . 'team_pic/';
}else{
	$uploadsDirectory = $_SERVER['DOCUMENT_ROOT'] . $directory_self . 'rider_pic/';
}

//若error導向位置
if($Mode == "create"){
	$uploadForm = 'http://' . $_SERVER['HTTP_HOST'] . $directory_self . '../team_'.$Mode.'.php';
}else{
	$uploadForm = 'http://' . $_SERVER['HTTP_HOST'] . $directory_self . '../settings_'.$Mode.'.php';
}


// name of the fieldname used for the file in the HTML form
$fieldname = 'file';

// 可能的PHP upload errors
$errors = array(1 => 'php.ini max file size exceeded', 
                2 => '檔案大小大於' . $_POST["MAX_FILE_SIZE"]. 'byte. ', 
                3 => 'file upload was only partial', 
                4 => 'no file was attached');
						

isset($_POST['submit'])
	or error('the upload form is neaded', $uploadForm);


($_FILES[$fieldname]['error'] == 0)
	or error($errors[$_FILES[$fieldname]['error']], $uploadForm);
	

	
// 確保從 HTTP upload
@is_uploaded_file($_FILES[$fieldname]['tmp_name'])
	or error('not an HTTP upload', $uploadForm);


	
//確定上傳的是img
@getimagesize($_FILES[$fieldname]['tmp_name'])
	or error('您上傳的並非圖片喔', $uploadForm);


//確保上傳暫存檔名唯一性 use time()
$now = time();
while(file_exists($uploadFilename = $uploadsDirectory.$now.'-'.$_FILES[$fieldname]['name']))
{
	$now++;
}

//upload成功導向位置

if($Mode == "create"){
	$uploadSuccess = 'http://' . $_SERVER['HTTP_HOST'] . $directory_self . '../team_'.$Mode.'.php?IMG='. $now.'-'.$_FILES[$fieldname]['name'];
}else{
	$uploadSuccess = 'http://' . $_SERVER['HTTP_HOST'] . $directory_self . '../settings_'.$Mode.'.php?IMG='. $now.'-'.$_FILES[$fieldname]['name'];
}

//移動檔案
$uploadFilename=iconv("utf-8", "big5",$uploadFilename);
@move_uploaded_file($_FILES[$fieldname]['tmp_name'],$uploadFilename)
	or error('receiving directory insuffiecient permission', $uploadForm);

//取得上傳好的位置以session
session_start();
//利用str_replace將上傳後檔名保留下來
$uploadFilename= str_replace($uploadsDirectory,"",$uploadFilename);
$_SESSION["uploadURL"] = 'http://' . $uploadsDirectory .$uploadFilename;

header('Location: ' . $uploadSuccess);

// 錯誤的function
function error($error, $location, $seconds = 5)
{
	header("Refresh: $seconds; URL=\"$location\"");
	echo 
	'<html>'."\n".
	'	<head>'."\n".
	'	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />'."\n\n".
	'	<title>上傳錯誤</title>'."\n\n".
	'	</head>'."\n\n".
	'	<body>'."\n\n".
	'	<div id="Upload">'."\n\n".
	'		<h1>上傳錯誤...</h1>'."\n\n".
	'		<p>錯誤點: '."\n\n".
	'		<span class="red">' . $error . '...</span>'."\n\n".
	'	 	將重新導向上傳頁</p>'."\n\n".
	'	 </div>'."\n\n".
	'</html>';
	exit;
} 
?>