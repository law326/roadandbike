<?php
/* -----bobo註解-----
 * module:
 * note: 點選其他城市，代表將修改session到當地
 * 基本上此檔不會單獨存在，會從其他PHP檔中引入，SO到此之前就已經做過autologin.php
 *
*/

$UrlHere = $_SERVER['REQUEST_URI']; //目前正在執行的檔案
?>
<div id="header">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td> <?php if($_SESSION['islogin'] == "true") { ?> <a href="races.php" ><?php }else {?> </a><a href="index.php" ><?php }?> <img border="0" src="images/logo.png" width="96" height="27" border="0" /></a> <span style=" font-weight:bolder; color:#ddd;" >Beta</span></br></td>
            <td align="right" valign="bottom">
<?php
if($_SESSION['islogin'] == "true") {//判斷登陸與否
	if ($page == "settings" || $page == "settingsrider" || $page == "settingsbike" || $page == "settingsteam"){
?>
				<a href="memb_career.php?ID=<?php echo $MemberID; ?>">回車手頁面</a>
<?php
	}else if ($page == "racecreate"){
?>
				<a href="races.php">回賽事頁面</a>
<?php
	}else if ($page == "teamcreate"){
?>
				<a href="team.php">回車隊頁面</a>
<?php
	}else{
?>
				<a href="memb_career.php?ID=<?php echo $row["MemberID"] ?>" class="word_type_wb12"><?php echo $MemberName ?></a>							
                <span class="word_type_g12">, 歡迎歸隊! <a href="settings.php">設定</a>｜<a href="logout.php">登出</a></span>
<?php 
	}//判斷右欄的格式
}else{//判斷登陸與否
?>
				<span class="word_type_g12">已經擁有帳號? <a class="popup" href="#" rel="popupre_login">登入</a></span>  
<?php 
}//判斷登陸與否
?>
			</td>
		</tr>
	</table>
</div><!--header end-->
<div id="base">
	<div class="back_sb"></div>
<?php
if ($pagestyle != "setting"){
?>
		<div id="menu">
<?php 
if($_SESSION['islogin'] == "true"){//login or logout menu style  
?>
			<div class="bar"><!--bar1-->
				<div class="box">
                	<a href="memb_career.php?ID=<?php echo $row["MemberID"] ?>">生涯</a>
                </div>
                <div class="box">
                    <a href="rank.php?Mode=1">排名</a>
                </div>
                <div class="box">
                    <a href="team.php">車隊</a>
                </div>
                <div class="box">
                    <a href="rider.php">車手</a>
                </div>
                <div class="box">
                    <a href="races.php">賽事</a>
                </div>
                <div class="box" style="display:none;">
                    <a href="news.php">關注</a>
                </div>
            </div><!--bar1-->
<?php 
}else{
?>
			<div class="bar"><!--bar2-->
				<div class="box">
                	<a href="rank.php?Mode=1">排名</a>
				</div>
                <div class="box">
                	<a href="team.php">車隊</a>
				</div>
                <div class="box">
                	<a href="rider.php">車手</a>
				</div>
                <div class="box">
                	<a href="races.php">賽事</a>
				</div>
			</div><!--bar2-->
<?php 
}
?>
			<div id="CollapsiblePanel1" class="CollapsiblePanel">
				<table width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td>
                    		<div class="CollapsiblePanelTab word_type_wb12" tabindex="0">您目前所在的城市 
                            	<span class="word_type_blueb12"><?php echo $Country ;?></span>, 
                                <span class="word_type_blueb12"><?php echo $City ;?></span>
							</div>
						</td>
					</tr>
				</table>
				<div class="CollapsiblePanelContent word_type_wb12">
					<div class="text">
						<table  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td style="width:75%" >
<?php 
require_once('Connections/URL_pwd_code.php');//將GET方式加解密

//列出DB中Taiwan的所有城市
$result_GeoID = mysql_query(" SELECT GeoID,City FROM tb_geography WHERE Country='Taiwan' ")or die(mysql_error());

$i = 1;
$obj_PWD = new URL_pwd_code();
while($row_GeoID = mysql_fetch_assoc($result_GeoID)){//將符合查詢的資料全印出來
	//echo $row_GeoID['GeoID']. " ".$row_GeoID['City'] ."<br>" ; 
	$c[$i] = $obj_PWD->Encrypt_Arr2c ($row_GeoID['GeoID'], $key, $expiry = 0);//GET字串加密

	$City_up = $row_GeoID['City'];
	echo "<a href=\"include_change_city.php?c=$c[$i]\">$City_up</a>．";
	if ( $i % 10 ==0 ) "<br/>"; //每10筆加上一個斷行符號
	$i++;
	if ($i == 9){ echo "<br/>";
	}else 
	if ($i == 17) break;
}
?>
								</td>
							</tr>
						</table>
					</div><!--text end-->
				</div><!--CollapsiblePanelContent end-->
			</div><!--CollapsiblePanel1 end-->
		</div><!--menu end-->
<?php
}//判斷menu是否要顯示出來
?>
<!--lightbox-->
<script type="text/javascript" src="lightbox/custom.js"></script>
<!--lightbox-->
<!--lightbox_login-->
	<div id="popupre_login" class="popupbox">
		<form id="login_form" method="post" action="login_confirm.php">
			<div class="back_sb"></div>
  			<div id="login">
            	<div class="box_top word_type_bb18">歡迎您的歸隊</div>
            	<div class="text">電子信箱<br />
                	<input type="text" id="login_name" name="login_name" size="30" />
				</div>
            	<div class="text">密碼<br />
                	<input type="password" id="login_pwd" name="login_pwd" size="30" />
                </div>
                <div class="text">
            		<input name="remember_me" type="checkbox" id="remember_me" checked="checked" />記住我｜<a href="signup.php">還沒有帳號？</a>
                </div>
                <div class="box_bottom">
                	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                		<tr>
                			<td>
								<img border="0" src="images/fb-login-button.png" width="154" height="22" />
							</td>
                            <td align="right">
                                <input class="word_type_wb12" type="submit" name="submit" id="submit" value="登入" />
                                <input type="hidden" name="is_login" value="permission" />
                                <input type="hidden" name="here" id="here" value="<?php echo $UrlHere; ?>" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </form>
    </div>
	<div id="fade"></div>
<!--lightbox_login-->
<!--所在地區-->
<script type="text/javascript">var CollapsiblePanel1 = new Spry.Widget.CollapsiblePanel("CollapsiblePanel1", {contentIsOpen:false});</script>
<!--所在地區-->
<!--捲動-->
<script type="text/javascript"> 
	$(function(){
		// 先取得 #cart 及其 top 值
		var $cart = $('.float'),
			_top = $cart.offset().top;
		
		// 當網頁捲軸捲動時
		var $win = $(window).scroll(function(){
			// 如果現在的 scrollTop 大於原本 #cart 的 top 時
			if($win.scrollTop() >= _top){
				// 如果 $cart 的座標系統不是 fixed 的話
				if($cart.css('position')!='fixed'){
					// 設定 #cart 的座標系統為 fixed
					$cart.css({
						position: 'fixed',
						top: 0
					});
				}
			}else{
				// 還原 #cart 的座標系統為 absolute
				$cart.css({
					position: 'absolute'
				});
			}
		});
	});
</script> 
<!--捲動-->