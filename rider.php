<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php
$page = "rider";
require_once('include_webtitle.php');//標題檔
?>
<?php
/* -----bobo註解-----
 * module: jQuery-Validation-Engin、GeoIP、jQueryTip
 * note: 等資料表確認好 才能讀取其他資訊
 * map顯示5筆戰績完成位置
 * 計算車齡
*/

session_start();

require_once('Connections/autologin.php');//判斷訪客狀態(登入、未登入)，再決定您"目前所在的城市"資訊
require_once('Connections/find_rider_bike.php');//給車手ID找出對應的車子資訊

list($Country_reg,$City_reg) = geoid_to_city( $row['GeoID']); //將會員的GeoID轉換成City
?>
<!--所在地區-->
<script src="CollapsiblePanel/CollapsiblePanel.js" type="text/javascript"></script>
<!--所在地區-->
<!-- 引用 jQuery 1.5 --> 
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
<!-- 引用 jQuery 1.5 --> 
</head>

<body>
<?php require_once('include_header.php'); ?>      
		<div id="container">
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
            	<tr>
                	<td valign="top">
                    	<div id="main">
<?php require_once('include_rider_title.php'); ?>

<?php //未登入提示趕快加入RoadandBike
if ($_SESSION['islogin'] != TRUE) require_once('tips/tips.html'); ?>
<?php require_once('share_box.php'); ?>
                          <div id="event">
<?php
$result = mysql_query("
	SELECT * FROM tb_event as e, tb_members as m, tb_bike as b, tb_bike_brand as bb, tb_bike_model as bm, tb_race as r, tb_race_mcareer as rm
	WHERE e.GeoID = '$GeoID' AND e.BikeID = b.BikeID AND b.BrandID = bb.BrandID AND b.ModelID = bm.ModelID AND m.MemberID = e.MemberID AND e.MemberCareerID = rm.MemberCareerID AND r.RaceID = rm.RaceID
	ORDER BY e.EventDate DESC LIMIT 20 ") or die(mysql_error());
	$rowNUM = mysql_num_rows($result);

if ($rowNUM == 0){
?>
<div class="block">
	<table width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td width="60" valign="top">
<?php
	echo "征服您的城市，成為街頭達人！";
?>
			</td>
		</tr>
	</table>
</div>
<?php
}else{
while ($row = mysql_fetch_assoc($result)){

switch ($row['EventType'])
	{
	case 14://mcareer2(個人賽事完成後)
	
$rowRace = $row['RaceID'];
	
$resultRank = mysql_query("	
	SELECT * 
	FROM tb_members as m, tb_race as r, tb_race_mcareer as rm
	WHERE r.RaceID = '$rowRace' AND r.RaceID = rm.RaceID AND m.MemberID = rm.MemberID AND rm.Status = 1
	ORDER BY rm.LapTime, JoinDate ASC
	") or die(mysql_error());

	while($rowRank = mysql_fetch_assoc($resultRank)){
		$MemNum++;
		if ($rowRank['MemberID'] == $row['MemberID']){
?>	
<div class="block">
	<table width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td width="120" valign="top">
				<div class="pic"><img border="0" src="<?php echo $row['IDImg']; ?>" width="50" height="50" /></div>
				<div class="pic"><img border="0" src="<?php echo $row['BikeImg']; ?>" width="50" height="50"/></div>
			</td>
			<td>
            	<div class="text"><a href="memb_career.php?ID=<?php echo $row['MemberID']; ?>" class="word_type_bb14"><?php echo $row['MemberName']; ?></a> / <a  class="word_type_bb14"><?php echo $row['BrandName']." ".$row['BikeModel']; ?></a></div>
				<div class="text">以 <?php echo $row['LapTime']; ?> 的成績完成 <a href="race.php?ID=<?php echo $row['RaceID']; ?>"><?php echo $row['Title']; ?> </a>暫居單站#<?php echo $MemNum; ?></div>
				<div class="text word_type_g12"><?php echo $row['EventDate']; ?></div>
			</td>
		</tr>
	</table>
</div>
<?php	
		}
	}
	$MemNum = 0;
	$rowRank = mysql_data_seek($resultRank, 0);
		break;
	case 15://mcareer3(個人賽事結束後)
?>
<div class="block">
	<table width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td width="120" valign="top">
				<div class="pic"><img border="0" src="<?php echo $row['IDImg']; ?>" width="50" height="50" /></div>
				<div class="pic"><img border="0" src="<?php echo $row['BikeImg']; ?>" width="50" height="50"/></div>
			</td>
			<td>
            	<div class="text"><a href="memb_career.php?ID=<?php echo $row['MemberID']; ?>" class="word_type_bb14"><?php echo $row['MemberName']; ?></a> / <a  class="word_type_bb14"><?php echo $row['BrandName']." ".$row['BikeModel']; ?></a></div>
				<div class="text">最終以 <?php echo $row['LapTime']; ?> 的成績拿下 <a href="race.php?ID=<?php echo $row['RaceID']; ?>"><?php echo $row['Title']; ?> </a>的單站冠軍</div>
				<div class="text word_type_g12"><?php echo $row['EventDate']; ?></div>
			</td>
		</tr>
	</table>
</div>
<?php	
		break;
	case 10://race1(個人賽事建立後)
?>
<div class="block">
	<table width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td width="120" valign="top">
				<div class="pic"><img border="0" src="<?php echo $row['IDImg']; ?>" width="50" height="50" /></div>
				<div class="pic"><img border="0" src="<?php echo $row['BikeImg']; ?>" width="50" height="50" /></div>
			</td>
			<td>
            	<div class="text"><a href="memb_career.php?ID=<?php echo $row['MemberID']; ?>" class="word_type_bb14"><?php echo $row['MemberName']; ?></a> / <a  class="word_type_bb14"><?php echo $row['BrandName']." ".$row['BikeModel']; ?></a></div>
				<div class="text">舉辦了 <a href="race.php?ID=<?php echo $row['RaceID']; ?>"><?php echo $row['Title']; ?></a></div>
				<div class="text word_type_g12"><?php echo $row['EventDate']; ?></div>
			</td>
		</tr>
	</table>
</div>
<?php
		break;
	default:
?>
<div class="block">
	<table width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td width="100%" valign="top">
<?php
		echo "No number between 1 and 3";
?>
			</td>
		</tr>
	</table>
</div>
<?php
	}
}//while結束
}//if結束
?>
                          </div>
                    </td>
                    <td valign="top">
<?php 
//載入右方的側欄
require_once('include_rider_rightSide.php'); ?>
                	</td>
            	</tr>
			</table>
		</div>
<?php require_once('include_footer.php'); ?>
</body>
</html>
