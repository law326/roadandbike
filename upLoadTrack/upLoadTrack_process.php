<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php  
$Fit = $_GET['Fit'];//為true時代表車手參加比賽並傳軌跡

$RaceID = $_GET['ID'];

$directory_self = str_replace(basename($_SERVER['PHP_SELF']), '', $_SERVER['PHP_SELF']);
$uploadsDirectory = $_SERVER['DOCUMENT_ROOT'] . $directory_self . 'uploaded_files/';

//若error導向位置
$uploadForm = 'http://' . $_SERVER['HTTP_HOST'] . $directory_self . '../races.php';

//upload成功導向位置
//$uploadSuccess = 'http://' . $_SERVER['HTTP_HOST'] . $directory_self . '../race_create.php';//舉辦比賽

// name of the fieldname used for the file in the HTML form
$fieldname = 'file';

// 可能的PHP upload errors
$errors = array(1 => 'php.ini max file size exceeded', 
                2 => '檔案大小大於' . $_POST["MAX_FILE_SIZE"]. 'byte. ', 
                3 => 'file upload was only partial', 
                4 => 'no file was attached');
						

isset($_POST['submit'])
	or error('the upload form is neaded', $uploadForm);

($_FILES[$fieldname]['error'] == 0)
	or error($errors[$_FILES[$fieldname]['error']], $uploadForm);


// 確保從 HTTP upload
@is_uploaded_file($_FILES[$fieldname]['tmp_name'])
	or error('not an HTTP upload', $uploadForm);

$_FILES[$fieldname]['name'] = strtolower($_FILES[$fieldname]['name']);//軌跡原始檔轉小寫

$TrackType = explode(".", $_FILES[$fieldname]['name']);//以.為區隔將名稱存成陣列, 以確保檔名內有多個符合條件

$realName = array_pop($TrackType);//抓最後一組陣列

//沒有檔名的不予採用, 並讀出真正檔名
if (count($TrackType)>1 || strcmp($realName, "tcx") == 0){ 
	$TrackType = 1;//是在比對兩組數據的大小, 如果0代表比對相等
	//echo 1;
}else if (count($TrackType)>1 || strcmp($realName, "gpx") == 0){ 
	$TrackType = 2;
	//echo 2;
}else{ 
	header('Location: ' . "../races.php?ERROR=0"); 
	//echo 0;
	exit;
}//不是規範中的軌跡檔


//確保上傳暫存檔名唯一性 use time()
$now = time();
while(file_exists($uploadFilename = $uploadsDirectory.$now.'-'.$_FILES[$fieldname]['name']))
{
	$now++;
}

//移動檔案
$uploadFilename=iconv("utf-8", "big5",$uploadFilename);
@move_uploaded_file($_FILES[$fieldname]['tmp_name'],$uploadFilename)
	or error('receiving directory insuffiecient permission', $uploadForm);

//取得上傳好的位置以session
session_start();
//利用str_replace將上傳後檔名保留下來
$uploadFilename= str_replace($uploadsDirectory,"",$uploadFilename);
$_SESSION["uploadURL"] = 'http://' . $_SERVER['HTTP_HOST'] . $directory_self. 'uploaded_files/' .$uploadFilename;


//header('Location: ' . $uploadSuccess);

// 錯誤的function
function error($error, $location, $seconds = 5)
{
	header("Refresh: $seconds; URL=\"$location\"");
	echo 
	'<html>'."\n".
	'	<head>'."\n".
	'	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />'."\n\n".
	'	<title>上傳錯誤</title>'."\n\n".
	'	</head>'."\n\n".
	'	<body>'."\n\n".
	'	<div id="Upload">'."\n\n".
	'		<h1>上傳錯誤...</h1>'."\n\n".
	'		<p>錯誤點: '."\n\n".
	'		<span class="red">' . $error . '...</span>'."\n\n".
	'	 	將重新導向上傳頁</p>'."\n\n".
	'	 </div>'."\n\n".
	'</html>';
	exit;
} 
if ($Fit == true){

session_start();

require_once('../Connections/autologin.php');//判斷訪客狀態(登入、未登入)，再決定您"目前所在的城市

$result = mysql_query("
	SELECT * FROM tb_race as r, tb_race_mcareer as rm 
	WHERE r.RaceID = '$RaceID' AND r.RaceID = rm.RaceID AND r.CreatorID = rm.MemberID ")
	or die(mysql_error()); //參賽單車
	$row = mysql_fetch_assoc($result);
	$GeoID = $row['GeoID'];
	$raceTrack = $row['GPSrecord'];//賽事軌跡
	$joinTrack = $uploadFilename;//參賽軌跡
	$StartLatLon = $row['StartLatLon'];
	$EndLatLon = $row['EndLatLon'];

require_once('Track_Compare.php');//軌跡比對

if ($errorTrack == 1) {
	//die ('您可能偷跑或來錯比賽了, 請從新上傳軌跡!'.header("Refresh: 5; URL=../race.php?ID=".$RaceID));
}else if ($errorTrack == 2) {
	die ('您還沒抵達終點, 請再接再厲吧!'.header("Refresh: 5; URL=../race.php?ID=".$RaceID));
}

$result = mysql_query("
	SELECT BikeID FROM tb_bike WHERE MemberID = '$MemberID' ")
	or die(mysql_error()); //參賽單車
	$row = mysql_fetch_assoc($result);
	$BikeID = $row['BikeID'];	
	$CreateDate=date("Y-m-d H:i:s"); //賽事建立日期
	
	//賽事花費時間
	if ($endH < 10)	$endH = "0".$endH;
	if ($endM < 10)	$endM = "0".$endM;
	if ($endS < 10)	$endS = "0".$endS;
	$MyLapTime = $endH.":".$endM.":".$endS;

	//echo $MyLapTime;		
	
$insertSQL = sprintf("
	INSERT INTO tb_race_mcareer (RaceID, MemberID, JoinDate, CompleteDate, LapTime, GPSrecord, Status, Score ,Evaluation, BikeID)
	VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s') " , 
		$RaceID, 
		$MemberID,
		$CreateDate,
		$endD,//在比對副程式裡
		$MyLapTime,//在比對副程式裡
		$uploadFilename, 
		1,
		0, 
		$_POST['evaluation'], //$Evaluation,
		$BikeID);
					
	$result = mysql_query($insertSQL) or die(mysql_error());

//找出$MemberCareerID
$result = mysql_query(" 
	SELECT MemberCareerID FROM tb_race_mcareer 
	WHERE MemberID = '$MemberID' AND RaceID = '$RaceID' AND JoinDate = '$CreateDate' ")
	or die(mysql_error());
	$row = mysql_fetch_assoc($result);
	$MemberCareerID = $row['MemberCareerID'];

if ($_GET['IsGroup'] == 0){//發布完成個人賽事
$insertSQL = sprintf("
	INSERT INTO tb_event (EventType, MemberID, GeoID, BikeID, TeamID, MemberCareerID, TeamCareerID, TeamAttendeeID ,FollowMemberID, FollowTeamID, EventDate)
	VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s') " , 
		14, 
		$MemberID,
		$GeoID,
		$BikeID,
		0,
		$MemberCareerID, 
		0,
		0, 
		0,
		0,
		$CreateDate);
					
	$result = mysql_query($insertSQL) or die(mysql_error());
}

	if ($_GET['IsGroup'] == 1){//寫入tb_race_mcareer
	
	//找出車隊
	$result = mysql_query("
		SELECT TeamID FROM tb_team_attendee 
		WHERE  MemberID = '$MemberID' ")
		or die(mysql_error());
		$row = mysql_fetch_assoc($result);
		$TeamID = $row['TeamID'];
		
	//找出賽事紀錄
	$result = mysql_query("
		SELECT TeamCareerID FROM tb_race_tcareer
		WHERE RaceID = '$RaceID' AND TeamID = '$TeamID' ")
		or die(mysql_error());
		$row = mysql_fetch_assoc($result);
		$TeamCareerID = $row['TeamCareerID'];
	
	//判斷是否有隊員已經完成比賽
	$result = mysql_query("
		SELECT * FROM tb_race_mcareer as rm, tb_team_attendee as ta, tb_race as r
		WHERE r.IsGroup = 1 AND rm.Status = 1 AND r.RaceID = rm.RaceID AND 
		rm.RaceID = '$RaceID' AND rm.MemberID = ta.MemberID AND ta.TeamID = '$TeamID' AND  rm.MemberID != '$MemberID'
		ORDER BY rm.LapTime ASC LIMIT 3")
		or die(mysql_error());
		$k = 0;
		while ($row = mysql_fetch_assoc($result)){
			$k++;
			$i = true;//為true代表車隊已經有人參賽
			$Mem[$k] = $row['MemberID'];
			$Lap[$k] = $row['LapTime'];
		}
	
	if ($i != true){//車隊第一位參加, 建新資料
		
		$insertSQL = sprintf("
		INSERT INTO tb_race_tcareer 
		(RaceID, TeamID, JoinDate, CompleteDate, LapTime, JoinMem1, JoinMem2, JoinMem3, Status, Score)
		VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s') " , 
			$RaceID, 
			$TeamID,
			$CreateDate,
			$endD,//在比對副程式裡
			$endH.":".$endM.":".$endS,//在比對副程式裡
			$MemberID, 
			0,
			0,
			0,
			0);
	
		$result = mysql_query($insertSQL) or die(mysql_error());
	
	//發布報名團隊賽事
	$insertSQL = sprintf("
		INSERT INTO tb_event (EventType, MemberID, GeoID, BikeID, TeamID, MemberCareerID, TeamCareerID, TeamAttendeeID ,FollowMemberID, FollowTeamID, EventDate)
		VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s') " , 
			31, 
			$MemberID,
			$GeoID,
			0,
			$TeamID,
			0, 
			$TeamCareerID,
			0, 
			0,
			0,
			$CreateDate);
						
		$result = mysql_query($insertSQL) or die(mysql_error());
	
		//在車隊頁面發布個人完成團隊賽
	$insertSQL = sprintf("
		INSERT INTO tb_event (EventType, MemberID, GeoID, BikeID, TeamID, MemberCareerID, TeamCareerID, TeamAttendeeID ,FollowMemberID, FollowTeamID, EventDate)
		VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s') " , 
			32, 
			$MemberID,
			$GeoID,
			$BikeID,
			$TeamID,
			0, 
			$TeamCareerID,
			0, 
			0,
			0,
			$CreateDate);
						
		$result = mysql_query($insertSQL) or die(mysql_error());
		
	}else{//已經有隊友參與, 更新資料
		switch ($k){
			case 3://已經有三位或超過三位參賽隊友
				if ($MyLapTime < $Lap[1]){//比第一還快
					$Lap = $Lap[2];
					$Mem3 = $Mem[2];
					$Mem2 = $Mem[1];
					$Mem1 = $MemberID;
				}else if ($MyLapTime < $Lap[2]){//比第二名快
					$Lap = $Lap[2];
					$Mem3 = $Mem[2];
					$Mem2 = $MemberID;
					$Mem1 = $Mem[1];
				}else if ($MyLapTime < $Lap[3]){//比第三名快
					$Lap = $MyLapTime;
					$Mem3 = $MemberID;
					$Mem2 = $Mem[2];
					$Mem1 = $Mem[1];
				}else{
					$Lap = $Lap[3];
					$Mem3 = $Mem[3];
					$Mem2 = $Mem[2];
					$Mem1 = $Mem[1];
				}
				break;
			case 2://已經有兩位參賽隊友
				if ($MyLapTime < $Lap[1]){//比第一還快
					$Lap = $Lap[2];
					$Mem3 = $Mem[2];
					$Mem2 = $Mem[1];
					$Mem1 = $MemberID;
				}else if ($MyLapTime < $Lap[2]){//比第二名快
					$Lap = $Lap[2];
					$Mem3 = $Mem[2];
					$Mem2 = $MemberID;
					$Mem1 = $Mem[1];
				}else{//比前兩名都慢
					$Lap = $MyLapTime;
					$Mem3 = $MemberID;
					$Mem2 = $Mem[2];
					$Mem1 = $Mem[1];
				}
				break;
			case 1://已經有一位參賽隊友
				if ($MyLapTime < $Lap[1]){//比第一還快
					$Lap = $Lap[1];
					$Mem3 = 0;
					$Mem2 = $Mem[1];
					$Mem1 = $MemberID;
				}else{//比第一名慢
					$Lap = $MyLapTime;
					$Mem3 = 0;
					$Mem2 = $MemberID;
					$Mem1 = $Mem[1];
				}
		}//switch
	
		if ($k > 1){//判斷是否滿足三人參賽
			$status = 1;
		}else{
			$status = 0;
		}
		
		$update_sql = "
			Update tb_race_tcareer as rt SET 
			rt.LapTime = '$Lap',
			rt.JoinMem1 = '$Mem1', 
			rt.JoinMem2 = '$Mem2',
			rt.JoinMem3 = '$Mem3',
			rt.Status = '$status'
			WHERE TeamCareerID = '$TeamCareerID' ";
			mysql_query($update_sql,$dbConn) or die(mysql_error());
			
	
	//在車隊頁面發布個人完成團隊賽
	$insertSQL = sprintf("
		INSERT INTO tb_event (EventType, MemberID, GeoID, BikeID, TeamID, MemberCareerID, TeamCareerID, TeamAttendeeID ,FollowMemberID, FollowTeamID, EventDate)
		VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s') " , 
			32, 
			$MemberID,
			$GeoID,
			$BikeID,
			$TeamID,
			0, 
			$TeamCareerID,
			0, 
			0,
			0,
			$CreateDate);
						
		$result = mysql_query($insertSQL) or die(mysql_error());
	
	if ($k == 2){//發布已經完成團隊賽事	
	
	$insertSQL = sprintf("
		INSERT INTO tb_event (EventType, MemberID, GeoID, BikeID, TeamID, MemberCareerID, TeamCareerID, TeamAttendeeID ,FollowMemberID, FollowTeamID, EventDate)
		VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s') " , 
			33, 
			$MemberID,
			$GeoID,
			0,
			$TeamID,
			0, 
			$TeamCareerID,
			0, 
			0,
			0,
			$CreateDate);
						
		$result = mysql_query($insertSQL) or die(mysql_error());	
	}
		
	}//if判斷寫入或更新DB
		
	}//if寫入DB
if ($result){
//插入成功
	$insertGoTo = "../race.php?ID=" . $RaceID;
	header('Location: ' . $insertGoTo);
	//echo "fit";
}else{
	echo "資料庫寫入失敗"; 
	exit;
}

}else{
$insertGoTo = "../race_create.php?route=".$uploadFilename."&Type=".$TrackType."&Evaluation=".$_POST['evaluation'];
$_SESSION["create"] = 1;
			  if (isset($_SERVER['QUERY_STRING'])) {
				$insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
				$insertGoTo .= $_SERVER['QUERY_STRING'];
			  }
			  header(sprintf("Location: %s", $insertGoTo));
			  //echo "nfit";
}
?>