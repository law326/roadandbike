<?php
session_start();
$dom1=new DOMDocument();

//判斷是要給race_create還是給race用
if ($upload == true){
	$dom1->load($_SESSION["uploadURL"]);
}else{
	$dom1->load("upLoadTrack/uploaded_files/".$uploadRute);
}


	$root1=$dom1->documentElement;
	
	$nodes1 = $root1->getElementsByTagName("trkpt");

	//時間
	$count=0;
	foreach( $nodes1 as $rt ) {
		$ele = $rt->getElementsByTagName( "ele" );
		$nValue_ele = $ele->item(0)->nodeValue;
		
		$time = $rt->getElementsByTagName( "time" );
		$nValue_time = $time->item(0)->nodeValue;
		
		//echo "dom1第 $count 點 ele= $nValue_ele  【 $nValue_time 】 <br>";
		$count =1+$count;
		$tcx=$tcx.$nValue_time.",";
	}
	//exit;
		
	$node1_length=$nodes1->length;
	
	$record1_length=$nodes1->length-1;
	
	for($s=0;$s<=$record1_length;$s++)
	{
		
		$points=$nodes1->item($s);
		
		$lats=$points->getAttribute("lat");
		$lons=$points->getAttribute("lon");
		
		$non=$non."new google.maps.LatLng(".$lats.",".$lons."),";
		$non1=$non1.$lats.",";
		$non2=$non2.$lons.",";
		
	}
echo "<script type='text/javascript'> var tracks=[".$non."]; var lats=[".$non1."]; var lons=[".$non2."];  var y=".$record1_length."; </script>";

?>

<script type="text/javascript" charset="utf-8">
  var geocoder;
  var ttmstr = "<?php echo $tcx; ?>";
  var ttm = ttmstr.split(",");
  var berlin = new google.maps.LatLng(25.0346990,121.6846020);
  var p = new Array(2);
  p[0] = 0;
  p[1] = y;
  var map;
  
  function initialize() {
    var mapOptions = {
      zoom: 13,
	  disableDefaultUI: true,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      center: berlin
    };

    map = new google.maps.Map(document.getElementById("map_canvas"),mapOptions);
	path();
	
  }
 
  function path() {
	var size = p[1]-p[0];
	var newPoints = new Array(size);
	var newtimes = new Array(size);
	var R = 6378137; // In meters
	var c = 0;

	$("#StartLon").attr("value",lons[p[0]]);
	$("#StartLat").attr("value",lats[p[0]]);
	$("#EndLon").attr("value",lons[p[1]]);
	$("#EndLat").attr("value",lats[p[1]]);

	geoinfo(lats[p[0]],lons[p[0]]);

	for (var i = 0; i < size; i++) { 
		newPoints[i] = tracks[ p[0] + i ];
						
	    var dLat = (lats[p[0] + i + 1] - lats[p[0] + i]) * Math.PI / 180;
        var dLon = (lons[p[0] + i + 1] - lons[p[0] + i]) * Math.PI / 180;
        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(lats[p[0] + i] * Math.PI / 180) * Math.cos(lats[p[0] + i + 1] * Math.PI / 180) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
        c = c + (2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a)));
      
    }
	var d = R * c;
	var getkm = (Math.round(d)/1000).toFixed(1);
	document.getElementById("sRes").innerHTML = getkm+"km";
	$("#Mileage").attr("value",getkm);
	
	var endtime = (
		(
			ttm[ p[1] ].substr(11,2)*3600+
			ttm[ p[1] ].substr(14,2)*60+
			ttm[ p[1] ].substr(17,2)*1
		)-(
			ttm[ p[0] ].substr(11,2)*3600+
			ttm[ p[0] ].substr(14,2)*60+
			ttm[ p[0] ].substr(17,2)*1
		)
	);
	var racetime = (
		ttm[ p[1] ].substr(0,4)+"/"+
		ttm[ p[1] ].substr(5,2)+"/"+
		ttm[ p[1] ].substr(8,2)
	);	
	var endhour = Math.floor(endtime/3600);
	var endmin = Math.floor((endtime-(endhour*3600))/60);
	var endsec = Math.floor(endtime-(endhour*3600)-(endmin*60));
	document.getElementById("sRes2").innerHTML = endhour+"h"+endmin+"m"+endsec+"s";
	$("#CompleteH").attr("value",endhour);
	$("#CompleteM").attr("value",endmin);
	$("#CompleteS").attr("value",endsec);
	$("#CompleteDate").attr("value",racetime);
	
		var latsmin = lats[p[0]], latsmax = lats[p[0]];
	for (var i = 1; i < size; i++){
		if (latsmax < lats[p[0]+i]){
			latsmax = lats[p[0]+i];
		}else if(latsmin > lats[p[0]+i]){
			latsmin = lats[p[0]+i]
		}
	}
	var lonsmin = lons[p[0]], lonsmax = lons[p[0]];
	for (var i = 1; i < size; i++){
		if (lonsmax < lons[p[0]+i]){
			lonsmax = lons[p[0]+i];
		}else if(lonsmin > lons[p[0]+i]){
			lonsmin = lons[p[0]+i]
		}
	}

	var southWest = new google.maps.LatLng(latsmin,lonsmin);
  	var northEast = new google.maps.LatLng(latsmax,lonsmax);
  	var bounds = new google.maps.LatLngBounds(southWest,northEast);
  	map.fitBounds(bounds);
	
    var flightPath = new google.maps.Polyline({
      path: newPoints,
	  strokeColor: "#00ADE2",
	  strokeOpacity: 0.8,
	  strokeWeight: 5
	});
    flightPath.setMap(map);
	
	
  }
	
	function updateClock(p1, p2){
		$('#clock').html(p1); 
		$('#clock2').html(p2);
	}
  
	$(function() {
		$( "#Slider4" ).slider({
			range: true,
			min: 0,
			max: y,
			values: [ 0, y ],
			slide: function( event, ui ) {
				p[0] = parseInt( ui.values[ 0 ], 10 );
				p[1] = parseInt( ui.values[ 1 ], 10 );
				updateClock(p[0],p[1]);
				initialize();
			}
		});
		
		updateClock(p[0],p[1]);
		initialize();
	});
	
	
	  
  function geoinfo(lat, lon) {
	geocoder = new google.maps.Geocoder();
    var latlng = new google.maps.LatLng(lat,lon);
    geocoder.geocode({'latLng': latlng}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        if (results[1]) {
          document.getElementById("sRes3").innerHTML = results[4].formatted_address; //results[3]陣列可以調整地址的詳細程度
		  $("#Cityaddress").attr("value",results[4].formatted_address);
        } else {
          alert("false");
        }
      } else {
        alert("false");
      }
    });
  }
</script> 