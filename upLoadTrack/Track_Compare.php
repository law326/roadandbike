<?php

$dom1=new DOMDocument();
$dom2=new DOMDocument();

if(($dom1->load("uploaded_files/".$raceTrack))&($dom2->load("uploaded_files/".$joinTrack)))
{
	$root1=$dom1->documentElement;
	$root2=$dom2->documentElement;
	
	//地頭蛇的檔案判斷
	$TrackTypeMain = explode(".", $raceTrack);//以.為區隔將名稱存成陣列, 以確保檔名內有多個符合條件
	
	$realName = array_pop($TrackType);//抓最後一組陣列
	
	//沒有檔名的不予採用, 並讀出真正檔名
	if (count($TrackTypeMain)>1 || strcmp($realName, "tcx") == 0){ 
		$TrackType1 = 1;//是在比對兩組數據的大小, 如果0代表比對相等
		//echo "@";
	}else if (count($TrackTypeMain)>1 || strcmp($realName, "gpx") == 0){ 
		$TrackType1 = 2;
		//echo "~";
	}//地頭蛇的檔案判斷
	
	//地頭蛇
	if ($TrackType1 == 2){//gpx1
		$nodes1 = $root1->getElementsByTagName("trkpt");
		
	}else if ($TrackType1 == 1){//tcx1
		$nodes1 = $root1->getElementsByTagName("Trackpoint");	
		
			$count1=0;
	foreach( $nodes1 as $rt1 ) {
		$TrackLat1 = $rt1->getElementsByTagName( "LatitudeDegrees" );
		$nValue_lat1 = $TrackLat1->item(0)->nodeValue;		
		
		$TrackLon1 = $rt1->getElementsByTagName( "LongitudeDegrees" );
		$nValue_lon1 = $TrackLon1->item(0)->nodeValue;		
		
		if (empty($nValue_lat1) || empty($nValue_lon1)){ $nValue_lat1 = $temp_lat1; $nValue_lon1 = $temp_lon1;}//如果沒有軌跡就拿上一點回補
		//echo "$".$nValue_lat1."$".$nValue_lon1."<br>";
		$temp_lat1 = $nValue_lat1;
		$temp_lon1 = $nValue_lon1;
		
		//$count =1+$count;
		$lats1=$lats1.$nValue_lat1.",";
		$lons1=$lons1.$nValue_lon1.",";
		
	}//地頭蛇
	//echo "@".$lats1."@".$lons1."<br>";
	$NewLats1 = explode(",", $lats1);
	$NewLons1 = explode(",", $lons1);
	}//gpx or tcx1
	
	
	//參賽者
	if ($TrackType == 2){//gpx2
		$nodes2 = $root2->getElementsByTagName("trkpt");
		
	}else if ($TrackType == 1){//tcx2
		$nodes2 = $root2->getElementsByTagName("Trackpoint");	
		
			$count=0;
	foreach( $nodes2 as $rt ) {
		$TrackLat = $rt->getElementsByTagName( "LatitudeDegrees" );
		$nValue_lat = $TrackLat->item(0)->nodeValue;		
		
		$TrackLon = $rt->getElementsByTagName( "LongitudeDegrees" );
		$nValue_lon = $TrackLon->item(0)->nodeValue;		
		
		if (empty($nValue_lat) || empty($nValue_lon)){ $nValue_lat = $temp_lat; $nValue_lon = $temp_lon;}//如果沒有軌跡就拿上一點回補
		
		$temp_lat = $nValue_lat;
		$temp_lon = $nValue_lon;
		
		//$count =1+$count;
		$lats=$lats.$nValue_lat.",";
		$lons=$lons.$nValue_lon.",";
		
	}//參賽者
	
	$NewLats = explode(",", $lats);
	$NewLons = explode(",", $lons);
	}//gpx or tcx2
		
	$record1_length = $EndLatLon - $StartLatLon + 1 ;//賽事軌跡數
	$record2_length = $nodes2->length-1;
	//echo $record1_length."!!!!".$record2_length."<br>";
	$km=20;//每單位取樣一次

	$fitj=0;
	$fit=0;
	$nonfit=0;
	
	//取樣次數
	$x=intval($record1_length/$km);
	
	for($i=$StartLatLon;$i<=$EndLatLon;$i)//i
	{
		$isFit=false;
		$isBreak=false;
		$lowerbound=0;
		
			//gpx or tcx
			if ($TrackType1 == 2){//gpx
				$point1=$nodes1->item($i);
				$lat1=$point1->getAttribute("lat");
				$lon1=$point1->getAttribute("lon");
			}else if ($TrackType1 == 1){//tcx
				$lat1=$NewLats1[$i];
				$lon1=$NewLons1[$i];
			}
			//gpx or tcx
			//echo $i."#".$lat1."$".$lon1."<br>";
		
		//if ($j-1 == $record2_length) $errorTrack = 2;//您還沒抵達終點

		for($j=$fitj;$j<=$record2_length;$j++)//j
		{
			//gpx or tcx
			if ($TrackType == 2){//gpx
				$point2=$nodes2->item($j);
				$lat2=$point2->getAttribute("lat");
				$lon2=$point2->getAttribute("lon");
			}else if ($TrackType == 1){//tcx
				$lat2=$NewLats[$j];
				$lon2=$NewLons[$j];
			}
			//gpx or tcx
			
			$lat_differ=abs($lat1-$lat2);
			$lon_differ=abs($lon1-$lon2);

			if(($lat_differ<=0.00025)&&($lon_differ<=0.00025))//0.00025為二十公尺,羅誠預設(($lat_differ<=0.00013889)&&($lon_differ<=0.00013889))//if1
			{
				if ($fit == 0) $startFit = $j;//抓起始點
				$fit++;
				$isFit=true;
				$fitj=$j;

				break;
			}
			else if(($lat_differ<=0.0007)&&($lon_differ<=0.0007))
			{


				//gpx or tcx
				if ($TrackType == 2){//gpx
					$point3=$nodes2->item($j+1);
					if($record2_length<$j+1)$point3=$nodes2->item($j-1);
					$lat3=$point3->getAttribute("lat");
					$lon3=$point3->getAttribute("lon");
				}else if ($TrackType == 1){//tcx
					$lat3=$NewLats[$j+1];
					$lon3=$NewLons[$j+1];
					if($record2_length<$j+1){
						$lat3=$NewLats[$j-1];
						$lon3=$NewLons[$j-1];
					}
				}
				//gpx or tcx


				//與前一點之間再分成四個點比對
				for($k=1;$k<=4;$k++)//k
				{
					$lat_new=$lat2+($lat3-$lat2)*$k/5;
					$lon_new=$lon2+($lon3-$lon2)*$k/5;

					$lat_differ=abs($lat1-$lat_new);
					$lon_differ=abs($lon1-$lon_new);	
					

					if(($lat_diffe<=0.00025)&&($lon_differ<=0.00025))//if2
					{
						if ($fit2 == 0) $startFit2 = $j;//抓起始點
						$fit2++;
						$isFit=true;
						$fitj=$j;
						$isBreak=true;

						break;
					}else{
						//if ($i == $EndLatLon && $k == 4) $errorTrack = 2;//您還沒抵達終點
					}//if2
				}//k
				if($isBreak)break;
			}//if1
		}//j
		//echo "j".$j."re".$record2_length."i".$i."en".$record1_length."<br>";
		
		
		if($isFit==false)
		{					
			$non=$non."new google.maps.LatLng(".$lat1.",".$lon1."),";
			$nonfit++;
		}
		$i=$i+$km;
		//確保終點也有被比對到, 並抓出參賽者的終點值(因為適用取樣地所以可能會跳過終點座標)
		if ($i == $EndLatLon + $km){ 
			break;
		}else if ($i >= $EndLatLon){
			$i = $EndLatLon;
		}
	}//i

	if (($fit+$fit2)/($fit+$fit2+$nonfit)*100 < 95) $errorTrack = 1;//軌跡比對結果小於90%就算是錯誤軌跡
	//echo (($fit+$fit2)/($fit+$fit2+$nonfit)*100)."#".$errorTrack."<br>";
	//抓起始點(比對兩組第一個比對正確的序號, 序號小的為起始點)
		
	if ($startFit != "0" || ($startFit2 < $startFit)){
		$startFit = $startFit2;
	}
	
	//計算時間
	$count=0;
	foreach( $nodes2 as $rt ) {
		
		if ($TrackType == 2){//gpx
			$time = $rt ->getElementsByTagName( "time" );
		}else if ($TrackType == 1){//tcx
			$time = $rt ->getElementsByTagName( "Time" );
		}
		$nValue_time = $time->item(0)->nodeValue;
		
		if ($count == $startFit){ 
			$startTime = $nValue_time; 
		}else if ($count == $j-1){
			$endTime = $nValue_time; 
		}
		
		$count++;
	}
	
	//echo $j."@".$startTime."/".$endTime."<br>";

	list($startY, $startM, $startD, $starth, $startm, $starts)=sscanf($startTime,"%d-%d-%dT%d:%d:%dZ");
	list($endY, $endMm, $starD, $endh, $endm, $ends)=sscanf($endTime,"%d-%d-%dT%d:%d:%dZ");
	
	$endTime = ($endh * 3600 + $endm * 60 + $ends) - ($starth * 3600 + $startm * 60 + $starts);
	//echo $endTime."<br>";
	$endH = floor($endTime / 3600);
	$endM = floor(($endTime -  ($endH * 3600)) / 60);
	$endS = floor($endTime -  ($endH * 3600) - ($endM * 60));
	$endD = $endY."/".$endMm."/".$starD;
	//echo $endD."$".$endH."$".$endM."$".$endS."<br>";
}
?>