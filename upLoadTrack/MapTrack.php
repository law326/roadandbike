<?php
session_start();

$TrackType = explode(".", $uploadRute);//以.為區隔將名稱存成陣列, 以確保檔名內有多個符合條件

$realName = array_pop($TrackType);//抓最後一組陣列

//沒有檔名的不予採用, 並讀出真正檔名
if (strcmp($realName, "tcx") == 0){ 
	$TrackType = 1;//是在比對兩組數據的大小, 如果0代表比對相等
	//echo 1;
}else if (strcmp($realName, "gpx") == 0){ 
	$TrackType = 2;
	//echo 2;
}else{ 
	header('Location: ' . $editFormAction . "?ERROR=0"); 
	echo 0;
	exit;
}//不是規範中的軌跡檔

$dom1=new DOMDocument();

	$dom1->load("upLoadTrack/uploaded_files/".$uploadRute);

	$root1 = $dom1 -> documentElement;
	
if ($TrackType == 2){//.gpx
	$nodes1 = $root1 -> getElementsByTagName("trkpt");

	//時間
	$count=0;
	foreach( $nodes1 as $rt ) {
		$ele = $rt->getElementsByTagName( "ele" );
		$nValue_ele = $ele->item(0)->nodeValue;
		
		$time = $rt->getElementsByTagName( "time" );
		$nValue_time = $time->item(0)->nodeValue;
		
		//echo "dom1第 $count 點 ele= $nValue_ele  【 $nValue_time 】 <br>";
		$count =1+$count;
		$tcx=$tcx.$nValue_time.",";
	}
	//exit;
		
	$node1_length=$nodes1->length;	
	$record1_length=$nodes1->length-1;

	for($s = $CreatorInfo['StartLatLon'] ; $s <= $CreatorInfo['EndLatLon'] ; $s=$s+10)
	{
		
		$points=$nodes1->item($s);
		
		$lats=$points->getAttribute("lat");
		$lons=$points->getAttribute("lon");
		

		if ($s == $CreatorInfo['EndLatLon']){
			$non=$non."new google.maps.LatLng(".$lats.",".$lons.")";
			$non1=$non1.$lats;
			$non2=$non2.$lons;
		}else{
			$non=$non."new google.maps.LatLng(".$lats.",".$lons."),";
			$non1=$non1.$lats.",";
			$non2=$non2.$lons.",";
		}
		
		$Math++;

	}//for
	if ($s > $CreatorInfo['EndLatLon']){
			
			$points=$nodes1->item($CreatorInfo['EndLatLon']);
		
			$lats=$points->getAttribute("lat");
			$lons=$points->getAttribute("lon");
		
			$non=$non."new google.maps.LatLng(".$lats.",".$lons.")";
			$non1=$non1.$lats;
			$non2=$non2.$lons;
	}
		
}else if($TrackType == 1){//.tcx
	$nodes1 = $root1 -> getElementsByTagName("Trackpoint");

	//時間
	$count=0;
	foreach( $nodes1 as $rt ) {
		$TrackLat = $rt->getElementsByTagName( "LatitudeDegrees" );
		$nValue_lat = $TrackLat->item(0)->nodeValue;		
		
		$TrackLon = $rt->getElementsByTagName( "LongitudeDegrees" );
		$nValue_lon = $TrackLon->item(0)->nodeValue;		
		
		if (empty($nValue_lat) || empty($nValue_lon)){ $nValue_lat = $temp_lat; $nValue_lon = $temp_lon;}//如果沒有軌跡就拿上一點回補
		
		$temp_lat = $nValue_lat;
		$temp_lon = $nValue_lon;
		
		//$count =1+$count;
		$lats=$lats.$nValue_lat.",";
		$lons=$lons.$nValue_lon.",";
		
	}
		
	$node1_length=$nodes1->length;	
	$record1_length=$nodes1->length-1;
	
	$NewLats = explode(",", $lats);
	$NewLons = explode(",", $lons);
	
	for($s = $CreatorInfo['StartLatLon'] ; $s <= $CreatorInfo['EndLatLon'] ; $s=$s+10)
	{
		
		if ($s == $CreatorInfo['EndLatLon']){
			$non=$non."new google.maps.LatLng(".$NewLats[$s].",".$NewLons[$s].")";
			$non1=$non1.$NewLats[$s];
			$non2=$non2.$NewLons[$s];
		}else{
			$non=$non."new google.maps.LatLng(".$NewLats[$s].",".$NewLons[$s]."),";
			$non1=$non1.$NewLats[$s].",";
			$non2=$non2.$NewLons[$s].",";
		}
		
		$Math++;
	}//for
	if ($s > $CreatorInfo['EndLatLon']){
			$s = $CreatorInfo['EndLatLon'];
			$non=$non."new google.maps.LatLng(".$NewLats[$s].",".$NewLons[$s].")";
			$non1=$non1.$NewLats[$s];
			$non2=$non2.$NewLons[$s];
	}
}else{ 

	header('Location: ' . "../races.php?ERROR=0"); 
	
}//不是規範中的軌跡檔
echo "<script type='text/javascript'> var tracks=[".$non."]; var lats=[".$non1."]; var lons=[".$non2."];  var size=".$Math."; </script>";

?>

<script type="text/javascript" charset="utf-8">
  var berlin = new google.maps.LatLng(25.0346990,121.6846020);
  var p = new Array(2);

  var map;
  
  function initialize() {
    var mapOptions = {
      zoom: 13,
	  disableDefaultUI: true,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      center: berlin
    };
	
    map = new google.maps.Map(document.getElementById("map"),mapOptions);
	path();
	
  }
 
  function path() {
	
	var newPoints = new Array(size);

	//起始與終點MARKER
	var imageS = 'images/cycling.png';
	var myLatLngS = tracks[0];
	var beachMarkerS = new google.maps.Marker({
		position: myLatLngS,
		map: map,
		icon: imageS
	});
	
	var imageE = 'images/finish.png';
	var myLatLngE = tracks[size-1];
	var beachMarkerE = new google.maps.Marker({
		position: myLatLngE,
		map: map,
		icon: imageE
	});

	
		var latsmin = lats[0], latsmax = lats[0];
	for (var i = 1; i < size; i++){
		if (latsmax < lats[0+i]){
			latsmax = lats[0+i];
		}else if(latsmin > lats[0+i]){
			latsmin = lats[0+i]
		}
	}
	var lonsmin = lons[0], lonsmax = lons[0];
	for (var i = 1; i < size; i++){
		if (lonsmax < lons[0+i]){
			lonsmax = lons[0+i];
		}else if(lonsmin > lons[0+i]){
			lonsmin = lons[0+i]
		}
	}

	var southWest = new google.maps.LatLng(latsmin,lonsmin);
  	var northEast = new google.maps.LatLng(latsmax,lonsmax);
  	var bounds = new google.maps.LatLngBounds(southWest,northEast);
  	map.fitBounds(bounds);
	
    var flightPath = new google.maps.Polyline({
      path: tracks,
	  strokeColor: "#00ADE2",
	  strokeOpacity: 0.8,
	  strokeWeight: 5
	});
    flightPath.setMap(map);
	
	
  }
	
	
	
</script> 