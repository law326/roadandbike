<?php
		//echo 'EndDate='.$EndDate;
	
		$EndYMD = explode("-",$EndDate);			
		$thisYear = date("Y");
		$thisMonth = date("m");
		$thisMonthA = date("m")-1;
		
		$monthDays = Array("31", "28", "31", "30", "31", "30", "31", "31", "30", "31", "30", "31", "31", "28", "31", "30", "31", "30");
		
		//閏月
		if (((fmod($thisYear,4) == 0) && (fmod($thisYear,100) !=0)) || (fmod($thisYear,400) == 0)){
			$monthDays[1] = 29;
		}elseif(((fmod($thisYear+1,4) == 0) && (fmod($thisYear+1,100) !=0)) || (fmod($thisYear+1,400) == 0)){
			$monthDays[13] = 29;
		}
		
		$thisDay = date("d");
		$thisMonthD = $monthDays[$thisMonthA];
		
		//echo 'EndYMD='.$EndYMD;
		//echo 'date("Y-m-d")='.date("Y-m-d");
		//exit;
		
		
		if ($EndDate >= date("Y-m-d")){//判斷比賽是否結束
			//echo "比賽進行中<br>";
			$Timer = TRUE;
			
			if ($EndYMD[0] > $thisYear){//需要否換年
				//echo "換年<br>";
				$EndYMD[1] = $EndYMD[1] + 12;
			}
			if ($EndYMD[1] == $thisMonth){//同月份
				//echo "同月份<br>";
				$thisMonthD = $EndYMD[2] - $thisDay;
			}else{
				if ($EndYMD[1] - $thisMonth == 1){//相差一個月
					//echo "不需要累加<br>";
				}else{
					for ($i = $thisMonthA ; $i <= $EndYMD[1] - 3 ; $i++){					
						$thisMonthD = $thisMonthD + $monthDays[$i+1];//累加天數
						//echo $i."累加結果".$thisMonthD."<br>";
					}
				}
				$thisMonthD = $thisMonthD + $EndYMD[2] - $thisDay;
			}
			
			//剩餘時間
			$HaveD = $thisMonthD;
			$HaveH = 23 - date('H');
			$HaveM = 59 - date('i');
			$HaveS = 60 - date('s');
			
		}else{//比賽結束
			$Timer = false;
		}
?>