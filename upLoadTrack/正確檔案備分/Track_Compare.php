<?php

$dom1=new DOMDocument();
$dom2=new DOMDocument();

if(($dom1->load("uploaded_files/".$raceTrack))&($dom2->load("uploaded_files/".$joinTrack)))
{
	$root1=$dom1->documentElement;
	$root2=$dom2->documentElement;
	
	$nodes1 = $root1->getElementsByTagName("trkpt");
	$nodes2 = $root2->getElementsByTagName("trkpt");	
		
	$record1_length = $EndLatLon - $StartLatLon + 1 ;//賽事軌跡數
	$record2_length = $nodes2->length-1;
	//echo "$record2_length".$record2_length."<br>";
	$km=20;//每單位取樣一次

	$fitj=0;
	$fit=0;
	$nonfit=0;
	
	//取樣次數
	$x=intval($record1_length/$km);
	
	for($i=$StartLatLon;$i<=$EndLatLon;$i)//i
	{
		$isFit=false;
		$isBreak=false;
		$point1=$nodes1->item($i);
		$lowerbound=0;
		
		$lat1=$point1->getAttribute("lat");
		$lon1=$point1->getAttribute("lon");
		
		if ($j-1 == $record2_length) $errorTrack = 2;//您還沒抵達終點

		for($j=$fitj;$j<=$record2_length;$j++)//j
		{
			$point2=$nodes2->item($j);
			
			$lat2=$point2->getAttribute("lat");
			$lon2=$point2->getAttribute("lon");
			
			$lat_differ=abs($lat1-$lat2);
			$lon_differ=abs($lon1-$lon2);

			if(($lat_differ<=0.00025)&&($lon_differ<=0.00025))//0.00025為二十公尺,羅誠預設(($lat_differ<=0.00013889)&&($lon_differ<=0.00013889))//if1
			{
				if ($fit == 0) $startFit = $j;//抓起始點
				$fit++;
				$isFit=true;
				$fitj=$j;

				break;
			}
			//else if(($lat_differ<=0.0007)&&($lon_differ<=0.0007))
			{
				$point3=$nodes2->item($j+1);

				if($record2_length<$j+1)$point3=$nodes2->item($j-1);
				
				$lat3=$point3->getAttribute("lat");
				$lon3=$point3->getAttribute("lon");

				//與前一點之間再分成四個點比對
				for($k=1;$k<=4;$k++)//k
				{
					$lat_new=$lat2+($lat3-$lat2)*$k/5;
					$lon_new=$lon2+($lon3-$lon2)*$k/5;

					$lat_differ=abs($lat1-$lat_new);
					$lon_differ=abs($lon1-$lon_new);	
					

					if(($lat_diffe<=0.00025)&&($lon_differ<=0.00025))//if2
					{
						if ($fit2 == 0) $startFit2 = $j;//抓起始點
						$fit2++;
						$isFit=true;
						$fitj=$j;
						$isBreak=true;

						break;
					}else{
						if ($i == $EndLatLon && $k == 4) $errorTrack = 2;//您還沒抵達終點
					}//if2
				}//k
				if($isBreak)break;
			}//if1
		}//j
		//echo "j".$j."re".$record2_length."i".$i."en".$record1_length."<br>";
		
		
		if($isFit==false)
		{					
			$non=$non."new google.maps.LatLng(".$lat1.",".$lon1."),";
			$nonfit++;
		}
		$i=$i+$km;
		//確保終點也有被比對到, 並抓出參賽者的終點值(因為適用取樣地所以可能會跳過終點座標)
		if ($i == $EndLatLon + $km){ 
			break;
		}else if ($i >= $EndLatLon){
			$i = $EndLatLon;
		}
	}//i
	
	if (($fit+$fit2)/($fit+$fit2+$nonfit)*100 < 90) $errorTrack = 1;//軌跡比對結果小於90%就算是錯誤軌跡
	//echo (($fit+$fit2)/($fit+$fit2+$nonfit)*100)."#".$errorTrack."<br>";
	//抓起始點(比對兩組第一個比對正確的序號, 序號小的為起始點)
	if (empty($startFit) || ($startFit2 < $startFit)){
		$startFit = $startFit2;
	}
	
	//計算時間
	$count=0;
	foreach( $nodes2 as $rt ) {
		
		$time = $rt ->getElementsByTagName( "time" );
		$nValue_time = $time->item(0)->nodeValue;
		
		if ($count == $startFit){ 
			$startTime = $nValue_time; 
		}else if ($count == $j-1){
			$endTime = $nValue_time; 
		}

		$count++;
	}
	
	//echo $j."@".$startTime."/".$endTime."<br>";

	list($startY, $startM, $startD, $starth, $startm, $starts)=sscanf($startTime,"%d-%d-%dT%d:%d:%dZ");
	list($endY, $endMm, $starD, $endh, $endm, $ends)=sscanf($endTime,"%d-%d-%dT%d:%d:%dZ");
	
	$endTime = ($endh * 3600 + $endm * 60 + $ends) - ($starth * 3600 + $startm * 60 + $starts);
	//echo $endTime."<br>";
	$endH = floor($endTime / 3600);
	$endM = floor(($endTime -  ($endH * 3600)) / 60);
	$endS = floor($endTime -  ($endH * 3600) - ($endM * 60));
	$endD = $endY."/".$endMm."/".$starD;
	//echo $endD."$".$endH."$".$endM."$".$endS."<br>";
}
?>