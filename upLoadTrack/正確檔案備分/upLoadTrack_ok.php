<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php session_start();?>
<html >
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>上傳成功</title>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript" charset="utf-8">
  var ttmstr = "<?php echo $tcx; ?>";
  var ttm = ttmstr.split(",");
  var berlin = new google.maps.LatLng(25.0346990,121.6846020);
  var p = new Array(2);
  p[0] = 0;
  p[1] = y;
  var map;
  
  function initialize() {
    var mapOptions = {
      zoom: 13,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      center: berlin
    };

    map = new google.maps.Map(document.getElementById("map_canvas"),mapOptions);
	path();
	
  }
 
  function path() {
	var size = p[1]-p[0];
	var newPoints = new Array(size);
	var newtimes = new Array(size);
	var R = 6378137; // In meters
	var c = 0;

	for (var i = 0; i < size; i++) { 
		newPoints[i] = tracks[ p[0] + i ];
						
	    var dLat = (lats[p[0] + i + 1] - lats[p[0] + i]) * Math.PI / 180;
        var dLon = (lons[p[0] + i + 1] - lons[p[0] + i]) * Math.PI / 180;
        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(lats[p[0] + i] * Math.PI / 180) * Math.cos(lats[p[0] + i + 1] * Math.PI / 180) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
        c = c + (2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a)));
      
    }
	var d = R * c;
	var getkm = ((Math.round(d)/1000).toFixed(3)+"km");
	document.getElementById("sRes").innerHTML =(getkm);
	//"<?php echo $tcxx; ?>" = getkm;
	var endtime = (
		(
			ttm[ p[1] ].substr(11,2)*3600+
			ttm[ p[1] ].substr(14,2)*60+
			ttm[ p[1] ].substr(17,2)*1
		)-(
			ttm[ p[0] ].substr(11,2)*3600+
			ttm[ p[0] ].substr(14,2)*60+
			ttm[ p[0] ].substr(17,2)*1
		)
	);
	var endhour = Math.floor(endtime/3600);
	var endmin = Math.floor((endtime-(endhour*3600))/60);
	var endsec = Math.floor(endtime-(endhour*3600)-(endmin*60));
	document.getElementById("sRes2").innerHTML = (endhour+"h"+endmin+"m"+endsec+"s");
	
		var latsmin = lats[p[0]], latsmax = lats[p[0]];
	for (var i = 1; i < size; i++){
		if (latsmax < lats[p[0]+i]){
			latsmax = lats[p[0]+i];
		}else if(latsmin > lats[p[0]+i]){
			latsmin = lats[p[0]+i]
		}
	}
	var lonsmin = lons[p[0]], lonsmax = lons[p[0]];
	for (var i = p[0]; i < size; i++){
		if (lonsmax < lons[p[0]+i]){
			lonsmax = lons[p[0]+i];
		}else if(lonsmin > lons[p[0]+i]){
			lonsmin = lons[p[0]+i]
		}
	}

	var southWest = new google.maps.LatLng(latsmin,lonsmin);
  	var northEast = new google.maps.LatLng(latsmax,lonsmax);
  	var bounds = new google.maps.LatLngBounds(southWest,northEast);
  	map.fitBounds(bounds);
	
    var flightPath = new google.maps.Polyline({
      path: newPoints,
	  strokeColor: "#00ADE2",
	  strokeOpacity: 0.8,
	  strokeWeight: 5
	});
    flightPath.setMap(map);
	
	
  }
	
	function updateClock(p1, p2){
		$('#clock').html(p1); 
		$('#clock2').html(p2);
	}
  
	$(function() {
		$( "#Slider4" ).slider({
			range: true,
			min: 0,
			max: y,
			values: [ 0, y ],
			slide: function( event, ui ) {
				p[0] = parseInt( ui.values[ 0 ], 10 );
				p[1] = parseInt( ui.values[ 1 ], 10 );
				updateClock(p[0],p[1]);
				initialize();
			}
		});
		
		updateClock(p[0],p[1]);
		initialize();
	});
</script> 
	</head>
	
	<body>
	
		<div id="Upload">
			<h1>您的圖示已經上傳成功!</h1>
			<p>圖示位置:<img border="0" src="<?php echo $_SESSION["uploadURL"]?>"  /><?php echo $_SESSION["uploadURL"]?></p>
		</div>
	<div id="map_canvas" style="height:320px;" class="block"></div>
	</body>

</html>