<?php
session_start();
$dom1=new DOMDocument();

	$dom1->load("upLoadTrack/uploaded_files/".$uploadRute);

	$root1 = $dom1 -> documentElement;

	$nodes1 = $root1 -> getElementsByTagName("trkpt");

	//時間
	$count=0;
	foreach( $nodes1 as $rt ) {
		$ele = $rt->getElementsByTagName( "ele" );
		$nValue_ele = $ele->item(0)->nodeValue;
		
		$time = $rt->getElementsByTagName( "time" );
		$nValue_time = $time->item(0)->nodeValue;
		
		//echo "dom1第 $count 點 ele= $nValue_ele  【 $nValue_time 】 <br>";
		$count =1+$count;
		$tcx=$tcx.$nValue_time.",";
	}
	//exit;
		
	$node1_length=$nodes1->length;	
	$record1_length=$nodes1->length-1;

	for($s = $CreatorInfo['StartLatLon'] ; $s <= $CreatorInfo['EndLatLon'] ; $s=$s+10)
	{
		
		$points=$nodes1->item($s);
		
		$lats=$points->getAttribute("lat");
		$lons=$points->getAttribute("lon");
		
		$non=$non."new google.maps.LatLng(".$lats.",".$lons."),";
		$non1=$non1.$lats.",";
		$non2=$non2.$lons.",";
		
		$Math++;
		if ($s > $CreatorInfo['EndLatLon']) $s = $CreatorInfo['EndLatLon'];
	}
echo "<script type='text/javascript'> var tracks=[".$non."]; var lats=[".$non1."]; var lons=[".$non2."];  var size=".$Math."; </script>";

?>

<script type="text/javascript" charset="utf-8">
  var berlin = new google.maps.LatLng(25.0346990,121.6846020);
  var p = new Array(2);

  var map;
  
  function initialize() {
    var mapOptions = {
      zoom: 13,
	  disableDefaultUI: true,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      center: berlin
    };
	
    map = new google.maps.Map(document.getElementById("map"),mapOptions);
	path();
	
  }
 
  function path() {
	
	var newPoints = new Array(size);

	//起始與終點MARKER
	var imageS = 'images/cycling.png';
	var myLatLngS = tracks[0];
	var beachMarkerS = new google.maps.Marker({
		position: myLatLngS,
		map: map,
		icon: imageS
	});
	
	var imageE = 'images/finish.png';
	var myLatLngE = tracks[size-1];
	var beachMarkerE = new google.maps.Marker({
		position: myLatLngE,
		map: map,
		icon: imageE
	});

	
		var latsmin = lats[0], latsmax = lats[0];
	for (var i = 1; i < size; i++){
		if (latsmax < lats[0+i]){
			latsmax = lats[0+i];
		}else if(latsmin > lats[0+i]){
			latsmin = lats[0+i]
		}
	}
	var lonsmin = lons[0], lonsmax = lons[0];
	for (var i = 1; i < size; i++){
		if (lonsmax < lons[0+i]){
			lonsmax = lons[0+i];
		}else if(lonsmin > lons[0+i]){
			lonsmin = lons[0+i]
		}
	}

	var southWest = new google.maps.LatLng(latsmin,lonsmin);
  	var northEast = new google.maps.LatLng(latsmax,lonsmax);
  	var bounds = new google.maps.LatLngBounds(southWest,northEast);
  	map.fitBounds(bounds);
	
    var flightPath = new google.maps.Polyline({
      path: tracks,
	  strokeColor: "#00ADE2",
	  strokeOpacity: 0.8,
	  strokeWeight: 5
	});
    flightPath.setMap(map);
	
	
  }
	
	
	
</script> 