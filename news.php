<?php
session_start();

require_once('Connections/autologin.php');//判斷訪客狀態(登入、未登入)，再決定您"目前所在的城市"資訊
require_once('Connections/find_LatLng.php');//由GeoID找出對應的經緯度 這裡給Gmap用

$editFormAction = $_SERVER['PHP_SELF']; //目前正在執行的檔案

$RaceID = $_GET['ID'];


//統計資訊
$resultRace = mysql_query("
	SELECT MIN(LapTime), AVG(Evaluation) FROM tb_race as r, tb_race_mcareer as rm 
	WHERE r.RaceID = rm.RaceID AND r.RaceID = '$RaceID' AND Status = 1")
	or die(mysql_error());
	$RaceInfo = mysql_fetch_assoc($resultRace);


//地頭蛇 & 賽事資訊
$resultCreator = mysql_query("
	SELECT * FROM tb_race as r, tb_race_mcareer as rm, tb_members as m, tb_bike as b, tb_bike_brand as bb, tb_bike_model as bm
	WHERE b.BrandID = bb.BrandID AND b.ModelID = bm.ModelID AND m.MemberID = r.CreatorID AND m.MemberID = rm.MemberID AND m.MemberID = b.MemberID AND r.RaceID = rm.RaceID AND r.RaceID = '$RaceID'")
	or die(mysql_error());
	$CreatorInfo = mysql_fetch_assoc($resultCreator);
	$uploadRute = $CreatorInfo['GPSrecord'];
	$EndDate = $CreatorInfo['RaceExpires'];//與副程式連結

//領先集團
$resultFirst = mysql_query("
	SELECT * FROM tb_race as r, tb_race_mcareer as rm, tb_members as m, tb_bike as b, tb_bike_brand as bb, tb_bike_model as bm
	WHERE b.BrandID = bb.BrandID AND b.ModelID = bm.ModelID AND  m.MemberID = rm.MemberID AND m.MemberID = b.MemberID AND r.RaceID = rm.RaceID AND r.RaceID = '$RaceID'
	Order BY rm.LapTime ASC LIMIT 3")
	or die(mysql_error());
	
//完成車友
$resultFin = mysql_query("
	SELECT * FROM tb_members as m, tb_race as r, tb_race_mcareer as rm 
	WHERE m.MemberID = rm.MemberID AND r.RaceID = rm.RaceID AND r.RaceID = '$RaceID' AND rm.Status = 1")
	or die(mysql_error());
	$FinNum = mysql_num_rows($resultFin);
	
//報名車友
$resultJoin = mysql_query("
	SELECT * FROM tb_members as m, tb_race as r, tb_race_mcareer as rm 
	WHERE m.MemberID = rm.MemberID AND r.RaceID = rm.RaceID AND r.RaceID = '$RaceID' AND rm.Status = 0")
	or die(mysql_error());
	$JoinNum = mysql_num_rows($resultJoin);

//完成賽事與否
$resultFinish = mysql_query("
	SELECT * FROM tb_race_mcareer as rm 
	WHERE MemberID = '$MemberID' AND RaceID = '$RaceID' ")
	or die(mysql_error());
	$Finish = mysql_num_rows($resultFinish);

require('upLoadTrack/Timer.php');//時間計數器

//if ((isset($_POST["From_insert"])) && ($_POST["From_insert"] == "form1") ) { 

// 取此檔所在位置
$directory_self = str_replace(basename($_SERVER['PHP_SELF']), '', $_SERVER['PHP_SELF']);

// 上傳處理的位置
$uploadHandler = 'http://' . $_SERVER['HTTP_HOST'] . $directory_self . 'upLoadTrack/upLoadTrack_process.php?Fit=true&ID=' . $RaceID . '&IsGroup=' . $CreatorInfo['IsGroup'];


$max_file_size = 1000000; // 限1MB , 硬性則要修改php.ini

//header('Location: ' .$uploadHandler);
//}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>RoadandBike - Tour in Your Life</title> 

<meta name="title" content="RoadandBike"> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="Tour in Your Life"> 
 
<meta name="keywords" content="軌跡、分享、單車、賽事、免費"> 
<link rel="shortcut icon" type="image/x-icon" href="images/icon.png">
<link href="css/page_style.css" rel="stylesheet" type="text/css" />
<!--停用JavaScript時-->
<noscript>
<META HTTP-EQUIV="Refresh" CONTENT="0;URL=noJavaScript.html">
</noscript>
<!--停用JavaScript時-->
<!--所在地區-->
<script src="CollapsiblePanel/CollapsiblePanel.js" type="text/javascript"></script>
<!--所在地區-->
<!--lightbox-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<script type="text/javascript" src="lightbox/custom.js"></script>
<!--lightbox-->
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
</head>
<body onLoad="initialize()">
	<?php require_once('include_header.php'); ?>    
		<div id="container">
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
            	<tr>
                	<td valign="top">
                    	<div id="main">
                            <div id="title">
                           	  <div class="button"><a href="race.php?ID=<?php echo $CreatorInfo['RaceID']; ?>" class="word_type_bb14">動態</a>．<a href="race_rank.php?ID=<?php echo $CreatorInfo['RaceID']; ?>" class="word_type_bb14">車手</a>．<a href="race_rank.php?ID=<?php echo $CreatorInfo['RaceID']; ?>" class="word_type_bb14">車隊</a></div>
                            <div class="text word_type_bb24">最新動態</div>
                                <div class="box">歡迎歸隊</div>
                        </div>
                            <?php if ($_SESSION["islogin"] == FALSE) require_once('tips/tips.html'); ?>		
                            <div id="event" >

</div><!--event-->
</div><!--main-->

                    </td>
                    <td valign="top">
                        <div id="side">
                          <div id="visiblebox"></div>
                          <div id="cadres">
                            <div class="box"><a href="#" class="word_type_bb12">您可能認識的車手</a></div>
                            <div class="block">
                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td><div class="pic"><img border="0" src="images/41636_100000069404320_7306_q.jpg" width="30" /></div>
                                    <a href="#" class="word_type_bb12">李大偉</a> (黃衫排名#12)<br />
                                    <span class="word_type_g12">Giant TCR2</span></td>
                                </tr>
                              </table>
                            </div>
                            <div class="block">
                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td><div class="pic"><img border="0" src="images/41636_100000069404320_7306_q.jpg" width="30" /></div>
                                    <a href="#" class="word_type_bb12">李大偉</a> (黃衫排名#12)<br />
                                    <span class="word_type_g12">Giant TCR2</span></td>
                                </tr>
                              </table>
                            </div>
                            <div class="block">
                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td><div class="pic"><img border="0" src="images/41636_100000069404320_7306_q.jpg" width="30" /></div>
                                    <a href="#" class="word_type_bb12">李大偉</a> (黃衫排名#12)<br />
                                    <span class="word_type_g12">Giant TCR2</span></td>
                                </tr>
                              </table>
                            </div>
                            <div class="block">
                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td><div class="pic"><img border="0" src="images/41636_100000069404320_7306_q.jpg" width="30" /></div>
                                    <a href="#" class="word_type_bb12">李大偉</a> (黃衫排名#12)<br />
                                    <span class="word_type_g12">Giant TCR2</span></td>
                                </tr>
                              </table>
                            </div>
                            <div class="block">
                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td><div class="pic"><img border="0" src="images/41636_100000069404320_7306_q.jpg" width="30" /></div>
                                    <a href="#" class="word_type_bb12">李大偉</a> (黃衫排名#12)<br />
                                    <span class="word_type_g12">Giant TCR2</span></td>
                                </tr>
                              </table>
                            </div>
                          </div>
                          <div id="cadres">
                            <div class="box"><a href="#" class="word_type_bb12">未完成的賽事</a></div>
                            <div class="block">
                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td>
                                    <a href="#" class="word_type_bb12">觀音山挑戰賽</a><br />
                                    <span class="word_type_g12">關門時間 14D23H12M43S</span></td>
                                </tr>
                              </table>
                            </div>
                            <div class="block">
                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td><a href="#" class="word_type_bb12">觀音山挑戰賽</a><br />
                                    <span class="word_type_g12">關門時間 14D23H12M43S</span></td>
                                </tr>
                              </table>
                            </div>
                            <div class="block">
                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td><a href="#" class="word_type_bb12">觀音山挑戰賽</a><br />
                                    <span class="word_type_g12">關門時間 14D23H12M43S</span></td>
                                </tr>
                              </table>
                            </div>
                            <div class="block">
                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td><a href="#" class="word_type_bb12">觀音山挑戰賽</a><br />
                                    <span class="word_type_g12">關門時間 14D23H12M43S</span></td>
                                </tr>
                              </table>
                            </div>
                            <div class="block">
                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td><a href="#" class="word_type_bb12">觀音山挑戰賽</a><br />
                                    <span class="word_type_g12">關門時間 14D23H12M43S</span></td>
                                </tr>
                              </table>
                            </div>
                          </div>

                        </div>
                	</td>
            	</tr>
			</table>
		</div>
		<?php require_once('include_footer.php'); ?>	
	</div>
<!--lightbox_upload-->
	<div id="popupre" class="popupbox">
		<form name="form1" id="form1" action="<?php echo $uploadHandler; ?>" enctype="multipart/form-data" method="post" >
			<div class="back_sb"></div>
  			<div id="login">
            	<div class="box_top word_type_bb18">完成比賽</div>
            	<div class="text">上傳賽事軌跡(限.gpx檔以及大小1MB以內)<br />
            		<input id="file" type="file" name="file">               
				</div>
                <div class="text">賽事困難度<br />
					<select name="evaluation" id="evaluation">
						<option value="1">Level 4</option>
						<option value="2">Level 3</option>
						<option value="3">Level 2</option>
						<option value="4">Level 1</option>
						<option value="5">Level HC</option>
					</select>
				</div>
                <div class="box_bottom">
                	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                    	<tr>
							<td align="right">
								<input class="word_type_wb12" type="submit" name="submit" id="submit" value="上傳" />
								
							</td>
						</tr>
					</table>
				</div>
			</div>
    	</form>
	</div>
<!--lightbox_upload-->
</body>
</html>
