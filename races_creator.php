<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php
$page = "racecreator";
require_once('include_webtitle.php');//標題檔
?>
<?php

session_start();

require_once('Connections/autologin.php');//判斷訪客狀態(登入、未登入)，再決定您"目前所在的城市"資訊
require_once('Connections/find_LatLng.php');//由GeoID找出對應的經緯度 這裡給Gmap

/* 經緯度轉換 這裡給Gmap用*/
$latlng = geo_to_latlng($GeoID);


//標題欄位
$result = mysql_query("SELECT SUM(Mileage), count(*) FROM tb_race WHERE GeoID='$GeoID' ")
	or die(mysql_error());
	$rowInfo = mysql_fetch_assoc($result);
	$Mileage = $rowInfo['SUM(Mileage)'];//計算累積里程
	$RaceNum = $rowInfo['count(*)'];//計算當地賽事
	
$now = date("Y-m-d");
	
//MAP & 資訊欄位(顯示數量需要同步)
$resultMark = mysql_query("
	SELECT StartLon, StartLat
	FROM tb_race as r, tb_race_mcareer as rm 
	WHERE r.RaceID = rm.RaceID AND r.GeoID = '$GeoID' AND rm.Status = 1 AND r.RaceExpires >= '$now'
	GROUP BY rm.RaceID ORDER BY r.RaceExpires ASC LIMIT 20")
	or die(mysql_error());
$resultMark2 = mysql_query("SELECT * FROM tb_race WHERE  GeoID='$GeoID' ORDER BY RaceExpires DESC LIMIT 20")
	or die(mysql_error());
$resultRaceInfo = mysql_query("
	SELECT Title, COUNT(MemberID), AVG(Evaluation), Mileage, RaceExpires, IsGroup, StartLon, StartLat, r.RaceID
	FROM tb_race as r, tb_race_mcareer as rm 
	WHERE r.RaceID = rm.RaceID AND r.GeoID = '$GeoID' AND rm.Status = 1 AND r.RaceExpires >= '$now'
	GROUP BY rm.RaceID ORDER BY r.RaceExpires ASC LIMIT 20")
	or die(mysql_error());//計算資訊
	$RaceNumInfo = mysql_num_rows($resultRaceInfo);//資訊欄位顯示的數
	

$resultRaceInfo2 = mysql_query("
	SELECT Title, COUNT(MemberID), AVG(Evaluation), Mileage, RaceExpires, IsGroup, StartLon, StartLat, r.RaceID
	FROM tb_race as r, tb_race_mcareer as rm 
	WHERE r.RaceID = rm.RaceID AND r.GeoID = '$GeoID' AND rm.Status = 1 AND r.RaceExpires >= '$now'
	GROUP BY rm.RaceID ORDER BY r.RaceExpires ASC LIMIT 20")
	or die(mysql_error());//計算資
	
$resultRaceInfo3 = mysql_query("
	SELECT m.IDImg, r.RaceID
	FROM tb_members as m, tb_race_mcareer as rm, tb_race as r
	WHERE r.RaceID = rm. RaceID AND rm.MemberID = m.MemberID AND r.GeoID = '$GeoID' AND rm.Status = 1 
	LIMIT 6")
	or die(mysql_error());//計算資訊

list($Country_reg,$City_reg) = geoid_to_city( $GeoID); //將會員的GeoID轉換成City

//車友動態
$resultFriend = mysql_query("SELECT * FROM tb_race as r, tb_race_mcareer as rm, tb_members as m, tb_bike as b, tb_follow_member as fm, tb_bike_brand as bb, tb_bike_model as bm
	WHERE b.BrandID = bb.BrandID AND b.ModelID = bm.ModelID AND m.MemberID = rm.MemberID AND m.MemberID = b.MemberID AND m.MemberID = fm.BeFID AND r.RaceID=rm.RaceID AND r.GeoID='$GeoID' AND fm.FID = '$MemberID' ORDER BY rand() LIMIT 5 ")
	or die(mysql_error());
	$FriendNum = mysql_num_rows($resultFriend);//找出關注車手

//最新賽況
$resultRider = mysql_query("SELECT * FROM tb_race as r, tb_race_mcareer as rm, tb_members as m
	WHERE (m.MemberID=rm.MemberID AND r.RaceID=rm.RaceID AND r.GeoID='$GeoID' AND rm.Status=1) ORDER BY rand() LIMIT 5 ")
	or die(mysql_error());
	$RiderNum = mysql_num_rows($resultRider);//找出完賽車手


?>
<!--所有jQuery-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
<!---所有jQuery--->
<!--lightbox-->
<script type="text/javascript" src="lightbox/custom.js"></script>
<!--lightbox-->
<!--關注按鈕-->
<link href="follow_unfollow/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.livequery.js"></script>
<script type="text/javascript"> 
   $(document).ready(function() { 
   
			$('.buttons2 > a').livequery("click",function(e){
				
				var parent  = $(this).parent();
				var getID   =  parent.attr('id').replace('button_','');//取得被click的車手or車隊id  格式長作這樣 button_id

				$.post("follow_unfollow/follow_member.php?id="+getID, {
	
				}, function(response){
					
					$('#button_'+getID).html($(response).fadeIn('slow'));
				});
		});	
	});
</script>
<!--關注按鈕-->
<!--所在地區-->
<script src="CollapsiblePanel/CollapsiblePanel.js" type="text/javascript"></script>
<!--所在地區-->
</head>

<body>
<?php require_once('include_header.php'); ?>     
		<div id="container">
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
            	<tr>
                	<td valign="top">
                    	<div id="main">
                            <div id="title">
                            	<div class="button"><a href="races.php" class="word_type_bb14">動態</a>．<a href="races_creator.php" class="word_type_bb14">地頭蛇</a></div>
                           	  <div class="button"></div>
                            <div class="text word_type_bb24"><?php echo $City_reg; ?>, 賽事</div>
                                <div class="box">累積賽事 <span class="word_type_blueb12"><?php echo $RaceNum ;?></span> 場．當地賽事累積里程 <span class="word_type_blueb12"><?php if (empty($Mileage)){ echo "0";}else{ echo $Mileage ;}?></span> km</div>
                                
                                    
                                
                        </div>
                            <?php if ($_SESSION["islogin"] == FALSE) require_once('tips/tips.html'); ?>	

<div id="event">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td>
                          
<?php  
/*當地車手 (畫面中間大版面區) Start */
$result = mysql_query("
	SELECT * 
	FROM tb_members as m, tb_race as r
	WHERE m.GeoID = r.GeoID AND m.GeoID = '$GeoID' AND m.AccessLevel IN(2,3,99) AND r.CreatorID = m.MemberID 
	GROUP BY m.MemberID 
	ORDER BY rand() LIMIT 50 ")or die(mysql_error());

$team_count = mysql_num_rows($result);//查車隊數量


if ($team_count > 0){
//"有車手資料";
while ($row_team = mysql_fetch_assoc($result)) {
	
	$msg_id = $row_team['MemberID'];//給click Load More按鈕使用
	
		/* 找出車子資訊 */
	require_once('Connections/find_rider_bike.php');//給車手ID找出對應的車子資/
	list($BrandName,$BikeModel) = rider_bike($row_team['MemberID']);

	/* 找出所屬車隊 */
	$aMemberID = $row_team['MemberID'];
	$result_mm = mysql_query("SELECT t.TeamName 
						FROM tb_team as t, tb_team_attendee as a 
							WHERE (a.MemberID='$aMemberID' AND t.TeamID=a.TeamID ) ")or die(mysql_error());
	$row_mm = mysql_fetch_assoc($result_mm);
	$TeamName = $row_mm['TeamName'];
	if (empty($TeamName)) $TeamName="自由車手";


?>
                                      <div class="blocks">
                                        <table width="100%" cellspacing="0" cellpadding="0">
                                          <tr>
                                            <td width="60" valign="top"><div class="pic"><a href="memb_career.php?ID=<?php echo $row_team['MemberID']; ?>"><img border="0" src="<?php echo $row_team['IDImg'];?>" width="50" height="50" /></a></div></td>
                                            <td><div class="bar">
                                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                  <td>
                                                 <?php 

/* 關注按鈕 */
$res = mysql_query("SELECT * FROM tb_follow_member WHERE FID = '$MemberID' AND BeFID = ".$row_team['MemberID']);
					
					$check_result = @mysql_num_rows(@$res);
if($row["AccessLevel"] > 1){//非正式車手不能關注
					if($check_result > 0 ){
						//此商品有被關注中,而且有登入,自己不能follow自己
						if($_SESSION['islogin'] == TRUE AND $MemberID != $row_team['MemberID']){
					?>  
                      <span class="buttons2" id="button_<?php echo $row_team['MemberID'];?>"><a class="btn-following" href="javascript: void(0)">正在關注</a></span>
<?php
					}}
					else
						//不存在關注,而且有登入,自己不能follow自己
					{	if($_SESSION['islogin'] == TRUE AND $MemberID != $row_team['MemberID']){
					?>
						<span class="buttons2" id="button_<?php echo $row_team['MemberID'];?>"><a class="btn-follow" href="javascript:void(0)">關主他</a></span>
						<?php
					}}}?>
                    						</td>
                                                </tr>
                                              </table>
                                            </div>
                                              <div class="text"><a href="memb_career.php?ID=<?php echo $row_team['MemberID']; ?>" class="word_type_bb14"><?php echo $row_team['MemberName']; ?></a> / <a  class="word_type_bb14"><?php echo $BrandName.'  '.$BikeModel; ?></a>
                                              </div>
                                              <div class="text"><span class="word_type_b12"><?php echo $TeamName; ?></span></div>
                                              <div class="text word_type_g12"><span class="text"><?php echo $Country ;?></span></div>
                                              </td>
                                          </tr>
                                        </table>
                                      </div>
<?php

			}//while End
		}
		else{
?>
<div class="block">
	<table width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td width="60" valign="top">
<?php
	echo "本地還沒有地頭蛇出沒！";
?>
			</td>
		</tr>
	</table>
</div>
<?php
		}//if ($result) End
		
?>

                                      </td>
                                  </tr>
                                </table>
</div>
                        </div>
                    </td>
                    <td valign="top">
                        <?php require_once('include_races_rightSide.php'); ?>
                	</td>
            	</tr>
			</table>
		</div>
		<?php require_once('include_footer.php'); ?>

	</div>

</body>
</html>
