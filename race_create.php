<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php
$page = "racecreate";
require_once('include_webtitle.php');//標題檔
$pagestyle = "setting";//提供header樣式判斷
?>
<?php
session_start();

require_once('Connections/autologin.php');//判斷訪客狀態(登入、未登入)，再決定您"目前所在的城市"資訊
require_once('Connections/find_LatLng.php');//由GeoID找出對應的經緯度 這裡給Gmap用
//●未登入不可瀏覽 
if ($_SESSION["islogin"] == FALSE) header("Location:  index.php");
//if ($_SESSION["create"] != 1) header("Location:  races.php");//讓已經建立比賽的不能再按上一頁
$_SESSION["create"] = false;
$editFormAction = $_SERVER['PHP_SELF']; //目前正在執行的檔案
$editFormAction = $editFormAction .'?route=' .$_GET['route'];
$upload = true; //提供給upLoadTrack判斷的上傳模式

if ((isset($_POST["From_insert"])) && ($_POST["From_insert"] == "form1") ) { 

	$raceName=$_POST["raceName"]; //賽事名稱/
	$StartLon=$_POST["StartLon"]; //賽事起點經度/
	$StartLat=$_POST["StartLat"]; //賽事起點緯度/
	$StartLatLon=$_POST["StartLatLon"]; //賽事終點經度/
	$EndLatLon=$_POST["EndLatLon"]; //賽事終點緯度/
	$Mileage=$_POST["Mileage"]; //賽事里程/
	$CreateDate=date("Y-m-d H:i:s"); //賽事建立日期
	$CompleteDate=$_POST["CompleteDate"]; //完成賽事所花費的時間
	$isgroup=$_POST['isgroup'];
	$Evaluation=$_POST['Evaluation'];//困難度評比
	
	require_once "Connections/PHP_formvalidator.php";//載入PHP表單驗證檔
    /*做這些PHP驗證*/
    $validator = new FormValidator();
    $validator->addValidation("raceName","req","請輸入raceName");
	
	if(!$validator->ValidateForm())	{
	//若驗證失敗進入此區
	 	echo "<B>驗證錯誤:</B>";

        $error_hash = $validator->GetErrors();
        foreach($error_hash as $inpname => $inp_err)
        {
            echo "<p>$inpname : $inp_err</p>\n";
        }    
		exit;
	}

	/*表單驗證合格才會執行下面*/

	
	//比對用軌跡產生GeoID
	$TaiwanCity = Array ("新北市","基隆","台北","桃園","新竹","苗栗","台中","彰化","南投","雲林","嘉義","台南","高雄","屏東","宜蘭","花蓮","台東");
	for ($i = 0 ; $i < count($TaiwanCity) ; $i++ ){
		$GeoID = stripos($_POST["Cityaddress"],$TaiwanCity[$i]);
		echo $GeoID;
		if (empty($GeoID)){
			 $GeoID = "false";
		}else{
			if ($i==0){
				 $GeoID = 2;
			}else{
				$GeoID = $i;
			}
			break;
		}
		if ($GeoID != "false")break;
	} 

	
	require_once('upLoadTrack/RaceExpires.php');//判斷閏月以及計算比賽剩餘時間



	if ($row["AccessLevel"] > 1) { //參賽資料填畢,准許建立賽事

		
		/* 檢查車隊名稱TeamName是否重複(不分地區,名稱只能有一組同名) */
		if ($GeoID == "false") die('目前尚未開放台灣以外的地區');
		if ($CompleteDate == 0) die('您的軌跡資料沒有紀錄時間');
		if ($row["IsTeam"] == 0) $isgroup=0;/*有參賽資格的自由車手只能建立個人賽事
			/* 開始建立賽事 */
			// 創造賽事 插入主表tb_race
			//if ($Mileage<=5.0 || $Mileage>=40.0) die('請上傳正確軌跡, 或將軌跡修剪為5~40km');
		 
			$insertSQL = sprintf("INSERT INTO tb_race (Title, RaceTypeID, GeoID, StartLon, StartLat, StartLatLon, EndLatLon, Mileage, RaceExpires ,CreateDate, CreatorID, IsGroup)
									VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s') " , 
									$raceName, 
									1,
									$GeoID,
									$StartLon,
									$StartLat,
									$StartLatLon,
									$EndLatLon, 
									$Mileage,
									$RaceExpires, 
									$CreateDate, 
									$MemberID ,
									$isgroup);
									
			$result = mysql_query($insertSQL) or die(mysql_error());	
			
			/*將接收到的 賽事轉換為ID 並且執tb_race_mcareer建立*/
			$result = mysql_query(" SELECT * FROM tb_race as r, tb_members as m WHERE r.CreatorID = m.MemberID AND r.CreatorID = '$MemberID' AND CreateDate = '$CreateDate' ")or die(mysql_error()); //賽事ID 
			$row = mysql_fetch_assoc($result);
			$race_ID = $row['RaceID'];
			$race_MemberName = $row['MemberName'];
			$race_IDImg = $row['IDImg'];

			$result = mysql_query(" SELECT * FROM tb_bike as b, tb_bike_brand as bb, tb_bike_model as bm WHERE b.MemberID = '$MemberID' AND b.ModelID = bm.ModelID AND b.BrandID = bb.BrandID ")or die(mysql_error()); //參賽單車
			$row = mysql_fetch_assoc($result);
			$bike_ID = $row['BikeID'];
			$bike_IMG = $row['BikeImg'];
			$bike_BrandName = $row['BrandName'];
			$bike_BikeModel = $row['BikeModel'];				

			$insertSQL = sprintf("INSERT INTO tb_race_mcareer ( RaceID, MemberID, JoinDate, CompleteDate, LapTime, GPSrecord, Status, Score ,Evaluation, BikeID)
									VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s') " , 
									$race_ID, 
									$MemberID,
									$CreateDate,
									$CompleteDate,
									$_POST['CompleteH'].":".$_POST['CompleteM'].":".$_POST['CompleteS'] ,
									$_GET['route'], 
									1,
									0, 
									$Evaluation,
									$bike_ID);
					
			$result = mysql_query($insertSQL) or die(mysql_error());
			
			//找出$MemberCareerID
			$result = mysql_query(" 
				SELECT MemberCareerID FROM tb_race_mcareer 
				WHERE MemberID = '$MemberID' AND RaceID = '$race_ID' AND JoinDate = '$CreateDate' ")
				or die(mysql_error());
				$row = mysql_fetch_assoc($result);
				$MemberCareerID = $row['MemberCareerID'];

if ($isgroup == 0){
	
	//發布郵件給粉絲
	$ActionBy = "MembRaceCreate";
	$result = mysql_query(" 
		SELECT * FROM tb_members as m, tb_follow_member as fm WHERE fm.FID = m.MemberID AND fm.BeFID = '$MemberID' 
		")or die(mysql_error());
echo $CreateDate;
	while ($row = mysql_fetch_assoc($result)){
		
		require_once('mail/mail.php');
		echo $row['Email'];
	}
/*
	//發布建立個人賽事
$insertSQL = sprintf("
	INSERT INTO tb_event 
	(EventType, MemberID, GeoID, BikeID, TeamID, MemberCareerID, TeamCareerID, TeamAttendeeID ,FollowMemberID, FollowTeamID, EventDate)
	VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s') " , 
		10, 
		$MemberID,
		$GeoID,
		$bike_ID,
		0,
		$MemberCareerID, 
		0,
		0, 
		0,
		0,
		$CreateDate);
					
	$result = mysql_query($insertSQL) or die(mysql_error());

//發布完成個人賽事
$insertSQL = sprintf("
	INSERT INTO tb_event 
	(EventType, MemberID, GeoID, BikeID, TeamID, MemberCareerID, TeamCareerID, TeamAttendeeID ,FollowMemberID, FollowTeamID, EventDate)
	VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s') " , 
		14, 
		$MemberID,
		$GeoID,
		$bike_ID,
		0,
		$MemberCareerID, 
		0,
		0, 
		0,
		0,
		$CreateDate);
					
	$result = mysql_query($insertSQL) or die(mysql_error());

}

			if ($isgroup==1){//寫入tb_race_mcareer

			$result = mysql_query(" 
				SELECT * FROM tb_team_attendee as ta, tb_team as t
				WHERE ta.MemberID = '$MemberID' AND ta.TeamID = t.TeamID ")
				or die(mysql_error()); //車隊ID 
				$row = mysql_fetch_assoc($result);
				$team_ID = $row['TeamID'];	
				$team_TeamName = $row['TeamName'];
				$team_TeamImg = $row['TeamImg'];
				echo $team_TeamImg."<br>";
			
			$insertSQL = sprintf(" 
				INSERT INTO tb_race_tcareer 
				(RaceID, TeamID, JoinDate, CompleteDate, LapTime, JoinMem1, JoinMem2, JoinMem3, Status, Score)
				VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s') " , 
					$race_ID, 
					$team_ID,
					$CreateDate,
					$CompleteDate,
					$_POST['CompleteH'].":".$_POST['CompleteM'].":".$_POST['CompleteS'] ,
					$MemberID, 
					0,
					0,
					0,
					0);

			$result = mysql_query($insertSQL) or die(mysql_error());
			
			//找到TeamCareerID
			$result = mysql_query(" 
				SELECT TeamCareerID FROM tb_race_tcareer 
				WHERE RaceID = '$race_ID' AND TeamID = '$team_ID' AND JoinDate = '$CreateDate' ")
				or die(mysql_error()); //車隊ID 
				$row = mysql_fetch_assoc($result);
				$TeamCareerID = $row['TeamCareerID'];	
			
			//發布團隊賽事
			$insertSQL = sprintf("
				INSERT INTO tb_event 
				(EventType, MemberID, GeoID, BikeID, TeamID, MemberCareerID, TeamCareerID, TeamAttendeeID ,FollowMemberID, FollowTeamID, EventDate)
				VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s') " , 
					12, 
					$MemberID,
					$GeoID,
					$bike_ID,
					$team_ID,
					0, 
					$TeamCareerID,
					0, 
					0,
					0,
					$CreateDate);
					
			$result = mysql_query($insertSQL) or die(mysql_error());
			
			//發布個人完成團隊賽事
			$insertSQL = sprintf("
				INSERT INTO tb_event 
				(EventType, MemberID, GeoID, BikeID, TeamID, MemberCareerID, TeamCareerID, TeamAttendeeID ,FollowMemberID, FollowTeamID, EventDate)
				VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s') " , 
					32, 
					$MemberID,
					$GeoID,
					$bike_ID,
					$team_ID,
					0, 
					$TeamCareerID,
					0, 
					0,
					0,
					$CreateDate);
					
			$result = mysql_query($insertSQL) or die(mysql_error());

	//發布郵件給粉絲與隊員
	$ActionBy = "TeamRaceCreate";
	$result = mysql_query(" 
		SELECT * FROM tb_members as m, tb_team_attendee as ta WHERE ta.MemberID = m.MemberID AND ta.TeamID = '$team_ID'
		")or die(mysql_error());

	while ($row = mysql_fetch_assoc($result)){

		require_once('mail/mail.php');
	}*/

			}
			if ($result){
				//插入成功
				
				/* 寫入車隊隊員 tb_team_attendee→Status=2幹部以及CreatorID */
				
				//header("Location:  races.php");
				exit;

		}
		else{
			echo "賽事名稱不能空白,請重新輸入"; //賽事名稱未命名,不能寫入	
			exit;
		}
			
	

	}else{
		header("Location:  index.php");//參賽資料尚未填畢,不可建立賽事
		exit;
	}
}
?>
<!--lightbox-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<script type="text/javascript" src="lightbox/custom.js"></script>
<!--lightbox-->

<!--Slider-->
	<link type="text/css" href="upLoadTrack/css/jquery-ui-1.8.2.custom.css" rel="stylesheet" />	
	<script type="text/javascript" src="upLoadTrack/js/jquery-1.4.2.min.js"></script>
	<script type="text/javascript" src="upLoadTrack/js/jquery-ui-1.8.2.custom.min.js"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>

<!--Slider-->
<!--jquery.validationEngine-->
<link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css"/>
<script src="js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
<script>
$(document).ready(function(){
	jQuery("#form1").validationEngine();
});
</script>
<!--jquery.validationEngine-->
</head>

<?php require_once('upLoadTrack/upLoadTrack.php'); ?>
<body>
<div id="signup">
<?php require_once('include_header.php'); ?>
		<div id="container">
                    	<div id="main">
                        
                       	  <div id="title" style="	border-bottom-color:#ccc;
	border-bottom-style:dotted;
	border-bottom-width:1px;">
                        	<div class="text word_type_bb24">舉辦賽事</div>
                       	  </div><!-- title -->
                          <form action="<?php echo $editFormAction; ?>" method="POST" enctype="multipart/form-data" name="form1" id="form1">
                        <div id="settings">
                          <div class="block">
                       	    <table width="100%" cellspacing="0" cellpadding="0">
                       	      <tr>
                       	        <td valign="top"><div class="topic">賽事名稱</div></td>
                       	        <td width="100%"><div class="text">
                       	          <input type="text" id="raceName" name="raceName" class="validate[required] text-input"/>
                     	          </div></td>
                   	          </tr>
                   	        </table>
                   	      </div>
                       	  <div class="block">
                       	    <table width="100%" cellspacing="0" cellpadding="0">
                       	      <tr>
                       	        <td valign="top"><div class="topic">賽事期間</div></td>
                       	        <td width="100%"><div class="text">
                       	          <select name="duedate" id="duedate">
                       	            <option value="14">兩周</option>
                       	            <option value="28">四周</option>
                       	            <option value="42">六周</option>
                       	            <option value="56">八周</option>
                       	            <option value="70">十周</option>
                       	            <option value="84">十二周</option>
                       	            <option value="98">十四周</option>
                   	              </select>
                       	        </div>
                   	            <div class="text word_type_g12">設定賽事開放的時間, 最長可以長達十六周</div></td>
                   	          </tr>
                   	        </table>
                   	      </div>
                       	  <div class="block" 
                          <?php if ($row["IsTeam"] == 0) {
										echo "style=\"display:none\"";
									}else{
										echo "";
									}
									?>
                          
                          >
                       	    <table width="100%" cellspacing="0" cellpadding="0">
                       	      <tr>
                       	        <td valign="top"><div class="topic">參賽單位</div></td>
                       	        <td width="100%"><div class="text">
                       	          <select name="isgroup" id="isgroup">
                       	            <option value="0">個人</option>
                                    <option value="1">團隊</option>
                   	              </select>
                       	        </div>
                   	            <div class="text word_type_g12">限制參賽的單位, 分為個人或團隊</div></td>
                   	          </tr>
                   	        </table>
                   	      </div>
                       	  <div class="block">
                       	    <table width="100%" cellspacing="0" cellpadding="0">
                       	      <tr>
                       	        <td valign="top"><div class="topic">賽事規劃</div></td>
                       	        <td width="100%"><div class="boxfloat word_type_g12">賽事里程 <a href="#" id="sRes" class="word_type_bb24">000.0km</a></div>
                       	          <div class="boxfloat word_type_g12"> 完成時間 <a href="#" id="sRes2" class="word_type_bb24">00h00m00s</a></div>
                                  <div style="display:none;" class="boxfloat word_type_g12"><a href="#" id="sRes3" class="word_type_bb12"></a></div></td>
                   	          </tr>
                   	        </table>
                   	      </div>
                          <div id="map_canvas" style="height:320px;" class="block"></div>
                       	  <div class="block">
                       	    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                       	      <tr>
                       	        <td valign="bottom">
                       	          <div id="Slider4" class="text"></div>
                                </td>
                   	          </tr>
                   	        </table>
                   	      </div>
                       	  <div class="box_bottom">
							<input class="word_type_wb12" type="submit" name="submit" id="submit" value="送出" />
                       	  </div>
                        </div>
									<input type="hidden" name="From_insert" value="form1" />
                                  	<input type="hidden" id="Mileage" name="Mileage" value=""  />
                                  	<input type="hidden" id="CompleteH" name="CompleteH" value=""  />
                                  	<input type="hidden" id="CompleteM" name="CompleteM" value=""  />
                                    <input type="hidden" id="CompleteS" name="CompleteS" value=""  />
                                    <input type="hidden" id="StartLon" name="StartLon" value=""  />
                                    <input type="hidden" id="StartLat" name="StartLat" value=""  />
                                    <input type="hidden" id="StartLatLon" name="StartLatLon" value=""  />
                                    <input type="hidden" id="EndLatLon" name="EndLatLon" value=""  />
                                    <input type="hidden" id="CompleteDate" name="CompleteDate" value=""  />
                                    <input type="hidden" id="Cityaddress" name="Cityaddress" value=""  />
                                    <input type="hidden" id="Evaluation" name="Evaluation" value="<?php echo $_GET['Evaluation']; ?>"  />
                    </form>
                    </div><!-- main -->
		</div><!-- container end -->
        </div><!-- base -->
	</div><!-- signup -->
<script type="text/javascript" charset="utf-8">
$('.back_sb').css({'filter' : 'alpha(opacity=40)'});
</script>
</body>
</html>
