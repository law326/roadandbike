<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php
$page = "race";
require_once('include_webtitle.php');//標題檔
?>
<?php
session_start();

require_once('Connections/autologin.php');//判斷訪客狀態(登入、未登入)，再決定您"目前所在的城市"資訊
require_once('Connections/find_LatLng.php');//由GeoID找出對應的經緯度 這裡給Gmap用

$editFormAction = $_SERVER['PHP_SELF']; //目前正在執行的檔案

$RaceID = $_GET['ID'];

$AccessLevel = $row["AccessLevel"];

//統計資訊
$resultRace = mysql_query("
	SELECT MIN(LapTime), AVG(Evaluation) FROM tb_race as r, tb_race_mcareer as rm 
	WHERE r.RaceID = rm.RaceID AND r.RaceID = '$RaceID' AND Status = 1")
	or die(mysql_error());
	$RaceInfo = mysql_fetch_assoc($resultRace);


//地頭蛇 & 賽事資訊
$resultCreator = mysql_query("
	SELECT * FROM tb_race as r, tb_race_mcareer as rm, tb_members as m, tb_bike as b, tb_bike_brand as bb, tb_bike_model as bm
	WHERE b.BrandID = bb.BrandID AND b.ModelID = bm.ModelID AND m.MemberID = r.CreatorID AND m.MemberID = rm.MemberID AND m.MemberID = b.MemberID AND r.RaceID = rm.RaceID AND r.RaceID = '$RaceID'")
	or die(mysql_error());
	$CreatorInfo = mysql_fetch_assoc($resultCreator);
	$uploadRute = $CreatorInfo['GPSrecord'];
	$EndDate = $CreatorInfo['RaceExpires'];//與副程式連結

//領先集團
$resultFirst = mysql_query("
	SELECT * FROM tb_race as r, tb_race_mcareer as rm, tb_members as m, tb_bike as b, tb_bike_brand as bb, tb_bike_model as bm
	WHERE b.BrandID = bb.BrandID AND b.ModelID = bm.ModelID AND  m.MemberID = rm.MemberID AND m.MemberID = b.MemberID AND r.RaceID = rm.RaceID AND r.RaceID = '$RaceID'
	Order BY rm.LapTime, JoinDate ASC LIMIT 3")
	or die(mysql_error());
	
//完成車友
$resultFin = mysql_query("
	SELECT * FROM tb_members as m, tb_race as r, tb_race_mcareer as rm 
	WHERE m.MemberID = rm.MemberID AND r.RaceID = rm.RaceID AND r.RaceID = '$RaceID' AND rm.Status = 1")
	or die(mysql_error());
	$FinNum = mysql_num_rows($resultFin);
	
//報名車友
$resultJoin = mysql_query("
	SELECT * FROM tb_members as m, tb_race as r, tb_race_mcareer as rm 
	WHERE m.MemberID = rm.MemberID AND r.RaceID = rm.RaceID AND r.RaceID = '$RaceID' AND rm.Status = 0")
	or die(mysql_error());
	$JoinNum = mysql_num_rows($resultJoin);

//完成賽事與否
$resultFinish = mysql_query("
	SELECT * FROM tb_race_mcareer as rm 
	WHERE MemberID = '$MemberID' AND RaceID = '$RaceID' ")
	or die(mysql_error());
	$Finish = mysql_num_rows($resultFinish);

require('upLoadTrack/Timer.php');//時間計數器
?>
<!--所在地區-->
<script src="CollapsiblePanel/CollapsiblePanel.js" type="text/javascript"></script>
<!--所在地區-->
<!--lightbox-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<script type="text/javascript" src="lightbox/custom.js"></script>
<!--lightbox-->
<!--計時-->

<script type="text/javascript" src="epiclock/jquery.dateformat.js"></script>
<script type="text/javascript" src="epiclock/jquery.epiclock.js"></script>
<script type="text/javascript">
            $(function ()
            {                
                $('#countdown').epiclock({mode: $.epiclock.modes.countdown, offset: {days: <?php echo $HaveD ; ?>, hours: <?php echo $HaveH ; ?>, minutes: <?php echo $HaveM ; ?>, seconds: <?php echo $HaveS ; ?>}});

            });
</script>
<!--計時-->
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<?php require_once('upLoadTrack/MapTrack.php'); ?>  
</head>

<body onLoad="initialize()">
	<?php require_once('include_header.php'); ?>    
		<div id="container">
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
            	<tr>
                	<td valign="top">
                    	<div id="main">
                            <div id="title">
                           	  <div class="button"><a href="race.php?ID=<?php echo $CreatorInfo['RaceID']; ?>" class="word_type_bb14">動態</a>．<a href="race_rank.php?ID=<?php echo $CreatorInfo['RaceID']; ?>" class="word_type_bb14">排名</a></div>
                            <div class="text word_type_bb24"><?php echo $CreatorInfo['Title']; ?></div>
                                <div class="box">賽事類型 <a>
                                <?php
								switch ($CreatorInfo['RaceTypeID'])
								{
								case 1:
								  echo "計時賽";
								  break;
								case 2:
								  echo "XXX";
								  break;
								case 3:
								  echo "XXX";
								  break;
								default:
								  echo "No number between 1 and 3";
								}
								?>
                                </a>．參賽條件 <a>
                                <?php
								switch ($CreatorInfo['IsGroup'])
								{
								case 0:
								  echo "個人";
								  break;
								case 1:
								  echo "團隊";
								  break;								
								}
								?>
                                </a></div>
                        </div>
                            <?php if ($_SESSION["islogin"] == FALSE) require_once('tips/tips.html'); ?>		
                            <div id="intro">
                            	<div id="map" class="block"></div>
                            </div>

<?php require_once('share_box.php'); ?>  
                            <div id="event" >
<?php
if ($CreatorInfo['IsGroup'] == 0){//個人賽事
	$result = mysql_query("
		SELECT * FROM tb_event as e, tb_members as m, tb_bike as b, tb_bike_brand as bb, tb_bike_model as bm, tb_race as r, tb_race_mcareer as rm
		WHERE e.BikeID = b.BikeID AND b.BrandID = bb.BrandID AND b.ModelID = bm.ModelID AND m.MemberID = e.MemberID AND e.MemberCareerID = rm.MemberCareerID AND r.RaceID = rm.RaceID AND r.RaceID = '$RaceID'
		ORDER BY e.EventDate DESC LIMIT 20 ") or die(mysql_error());

}else{//團隊賽事
	$result = mysql_query("
		SELECT * FROM tb_event as e, tb_members as m, tb_race as r, tb_team as t, tb_race_tcareer as rt
		WHERE e.MemberID = m.MemberID AND r.RaceID = '$RaceID' AND r.RaceID = rt.RaceID AND e.TeamID = t.TeamID AND e.TeamCareerID = rt.TeamCareerID 
		ORDER BY e.EventDate DESC LIMIT 20 ") 
		or die(mysql_error());
		
}

while ($row = mysql_fetch_assoc($result)){
/*####################個人賽事####################*/
switch ($row['EventType'])
	{
		case 14://mcareer2(個人賽事完成後)
	
$resultRank = mysql_query("	
	SELECT * 
	FROM tb_members as m, tb_race as r, tb_race_mcareer as rm
	WHERE r.RaceID = '$RaceID' AND r.RaceID = rm.RaceID AND m.MemberID = rm.MemberID AND rm.Status = 1
	ORDER BY rm.LapTime, JoinDate ASC
	") or die(mysql_error());

	while($rowRank = mysql_fetch_assoc($resultRank)){
		$MemNum++;
		if ($rowRank['MemberID'] == $row['MemberID']){
?>	
<div class="block">
	<table width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td width="120" valign="top">
				<div class="pic"><img border="0" src="<?php echo $row['IDImg']; ?>" width="50" height="50" /></div>
				<div class="pic"><img border="0" src="<?php echo $row['BikeImg']; ?>" width="50" /></div>
			</td>
			<td>
            	<div class="text"><a href="memb_career.php?ID=<?php echo $row['MemberID']; ?>" class="word_type_bb14"><?php echo $row['MemberName']; ?></a> / <a  class="word_type_bb14"><?php echo $row['BrandName']." ".$row['BikeModel']; ?></a></div>
				<div class="text">以 <?php echo $row['LapTime']; ?> 的成績完成 <a href="race.php?ID=<?php echo $row['RaceID']; ?>"><?php echo $row['Title']; ?> </a>暫居單站#<?php echo $MemNum; ?></div>
				<div class="text word_type_g12"><?php echo $row['EventDate']; ?></div>
			</td>
		</tr>
	</table>
</div>
<?php	
		}
	}
	$MemNum = 0;
	$rowRank = mysql_data_seek($resultRank, 0);
		break;
	case 15://mcareer3(個人賽事結束後)
?>
<div class="block">
	<table width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td width="120" valign="top">
				<div class="pic"><img border="0" src="<?php echo $row['IDImg']; ?>" width="50" height="50" /></div>
				<div class="pic"><img border="0" src="<?php echo $row['BikeImg']; ?>" width="50" /></div>
			</td>
			<td>
            	<div class="text"><a href="memb_career.php?ID=<?php echo $row['MemberID']; ?>" class="word_type_bb14"><?php echo $row['MemberName']; ?></a> / <a  class="word_type_bb14"><?php echo $row['BrandName']." ".$row['BikeModel']; ?></a></div>
				<div class="text">最終以 <?php echo $row['LapTime']; ?> 的成績拿下 <a href="race.php?ID=<?php echo $row['RaceID']; ?>"><?php echo $row['Title']; ?> </a>的單站冠軍</div>
				<div class="text word_type_g12"><?php echo $row['EventDate']; ?></div>
			</td>
		</tr>
	</table>
</div>
<?php	
		break;
	case 10://race1(個人賽事建立後)
?>
<div class="block">
	<table width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td width="120" valign="top">
				<div class="pic"><img border="0" src="<?php echo $row['IDImg']; ?>" width="50" height="50" /></div>
				<div class="pic"><img border="0" src="<?php echo $row['BikeImg']; ?>" width="50" /></div>
			</td>
			<td>
            	<div class="text"><a href="memb_career.php?ID=<?php echo $row['MemberID']; ?>" class="word_type_bb14"><?php echo $row['MemberName']; ?></a> / <a  class="word_type_bb14"><?php echo $row['BrandName']." ".$row['BikeModel']; ?></a></div>
				<div class="text">舉辦了 <a href="race.php?ID=<?php echo $row['RaceID']; ?>"><?php echo $row['Title']; ?></a></div>
				<div class="text word_type_g12"><?php echo $row['EventDate']; ?></div>
			</td>
		</tr>
	</table>
</div>
<?php	
		break;
/*####################團隊賽事####################*/
	case 12://race3(團隊賽事建立後)
?>
<div class="block">
	<table width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td width="60" valign="top">
				<div class="pic"><img border="0" src="<?php echo $row['TeamImg']; ?>" width="50" height="50" /></div>
			</td>
			<td>
            	<div class="text"><a href="team_career.php?ID=<?php echo $row['TeamID']; ?>" class="word_type_bb14"><?php echo $row['TeamName']; ?></a></div>
				<div class="text">舉辦了 <a href="race.php?ID=<?php echo $row['RaceID']; ?>"><?php echo $row['Title']; ?></a></div>
				<div class="text word_type_g12"><?php echo $row['EventDate']; ?></div>
			</td>
		</tr>
	</table>
</div>
<?php	
		break;
	case 31://tcareer1(非舉辦車隊第一人完成團隊賽事後)
?>
<div class="block">
	<table width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td width="60" valign="top">
				<div class="pic"><img border="0" src="<?php echo $row['TeamImg']; ?>" width="50" height="50" /></div>
			</td>
			<td>
            	<div class="text"><a href="team_career.php?ID=<?php echo $row['TeamID']; ?>" class="word_type_bb14"><?php echo $row['TeamName']; ?></a></div>
				<div class="text">報名了 <a href="race.php?ID=<?php echo $row['RaceID']; ?>"><?php echo $row['Title']; ?> </a></div>
				<div class="text word_type_g12"><?php echo $row['EventDate']; ?></div>
			</td>
		</tr>
	</table>
</div>
<?php	
		break;
	case 32://tcareer2(個人完成團隊賽)
?>
<div class="block">
	<table width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td width="120" valign="top">
				<div class="pic"><img border="0" src="<?php echo $row['IDImg']; ?>" width="50" height="50" /></div>
				<div class="pic"><img border="0" src="<?php echo $row['TeamImg']; ?>" width="50" height="50" /></div>
			</td>
			<td>
            	<div class="text"><a href="memb_career.php?ID=<?php echo $row['MemberID']; ?>" class="word_type_bb14"><?php echo $row['MemberName']; ?></a> / <a href="team_career.php?ID=<?php echo $row['TeamID']; ?>" class="word_type_bb14"><?php echo $row['TeamName']; ?></a></div>
				<div class="text">以 <?php echo $row['LapTime']; ?> 的成績完成 <a href="race.php?ID=<?php echo $row['RaceID']; ?>"><?php echo $row['Title']; ?> </a></div>
				<div class="text word_type_g12"><?php echo $row['EventDate']; ?></div>
			</td>
		</tr>
	</table>
</div>
<?php	
		break;
	case 33://tcareer3(團隊賽事完成後)
	
$resultRank = mysql_query("	
	SELECT * 
	FROM tb_team as t, tb_race as r, tb_race_tcareer as rt
	WHERE r.RaceID = '$RaceID' AND r.RaceID = rt.RaceID AND t.TeamID = rt.TeamID AND rt.Status = 1
	ORDER BY rt.LapTime, JoinDate ASC
	") or die(mysql_error());

	while($rowRank = mysql_fetch_assoc($resultRank)){
		$RankNum++;
		if ($rowRank['TeamID'] == $row['TeamID']){
?>
<div class="block">
	<table width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td width="60" valign="top">
				<div class="pic"><img border="0" src="<?php echo $row['TeamImg']; ?>" width="50" height="50" /></div>
			</td>
			<td>
            	<div class="text"><a href="team_career.php?ID=<?php echo $row['TeamID']; ?>" class="word_type_bb14"><?php echo $row['TeamName']; ?></a></div>
				<div class="text">以 <?php echo $row['LapTime']; ?> 的成績完成 <a href="race.php?ID=<?php echo $row['RaceID']; ?>"><?php echo $row['Title']; ?> </a>暫居單站#<?php echo $RankNum; ?></div>
				<div class="text word_type_g12"><?php echo $row['EventDate']; ?></div>
			</td>
		</tr>
	</table>
</div>
<?php	
		}
	}
	$RankNum = 0;
	$rowRank = mysql_data_seek($resultRank, 0);
		break;
	case 34://tcareer4(團隊賽結束成後)
?>
<div class="block">
	<table width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td width="60" valign="top">
				<div class="pic"><img border="0" src="<?php echo $row['TeamImg']; ?>" width="50" height="50" /></div>
			</td>
			<td>
            	<div class="text"><a href="team_career.php?ID=<?php echo $row['TeamID']; ?>" class="word_type_bb14"><?php echo $row['TeamName']; ?></a></div>
				<div class="text">最終以 <?php echo $row['LapTime']; ?> 的成績拿下 <a href="race.php?ID=<?php echo $row['RaceID']; ?>"><?php echo $row['Title']; ?> </a>的單站冠軍</div>
				<div class="text word_type_g12"><?php echo $row['EventDate']; ?></div>
			</td>
		</tr>
	</table>
</div>
<?php
		break;
	default:
?>
<div class="block">
	<table width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td width="100%" valign="top">
<?php
		echo $row['EventType'];
?>
			</td>
		</tr>
	</table>
</div>
<?php
	}
}
?>
</div><!--event-->
</div><!--main-->

                    </td>
                    <td valign="top">
                        <?php require_once('include_race_rightSide.php'); ?>
                	</td>
            	</tr>
			</table>
		</div>
		<?php require_once('include_footer.php'); ?>	
	</div>

</body>
</html>
