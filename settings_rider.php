<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php
$page = "settingsrider";
require_once('include_webtitle.php');//標題檔
$pagestyle = "setting";//提供header樣式判斷
?>
<?php
/* -----bobo註解-----
 * module:jQuery-Validation-Engin、jQueryTip、PHP表單驗證檔
 * note:	 
 *
 *
*/
session_start();

unset($_SESSION["upload_kind"]);
require_once('Connections/autologin.php');//判斷訪客狀態(登入、未登入)，再決定您"目前所在的城市"資訊

//●未登入不可瀏覽 
if ($_SESSION['islogin'] == FALSE) header("Location:  index.html");  

$GeoID = $row["GeoID"];
$Access = $row["AccessLevel"];

$Mode = 'rider';

$resultCreator= mysql_query(" SELECT * FROM tb_team WHERE CreatorID = '$MemberID' ")or die(mysql_error());
$rowCreator =  mysql_num_rows($resultCreator);

// 取此檔所在位置
$directory_self = str_replace(basename($_SERVER['PHP_SELF']), '', $_SERVER['PHP_SELF']);

// 上傳處理的位置
$uploadHandler = 'http://' . $_SERVER['HTTP_HOST'] . $directory_self . 'upLoadImg/upLoadImg_process.php';

$max_file_size = 100000; // 限100KB , 硬性則要修改php.ini


$IDImg = $row['IDImg'];
list($Country_reg,$City_reg) = geoid_to_city( $row['GeoID']); //將會員的GeoID轉換成City

$result_IDImg = mysql_query("SELECT * FROM tb_members WHERE MemberID = '$MemberID' ")or die(mysql_error());
$row_ID = mysql_fetch_assoc($result_IDImg);

$RideAge=$row["RideAge"];
if ($RideAge=="0000-00-00") {
	//
}
else{
	$selected_year = $RideAge;
}



//是否為自由車手
if ($row['IsTeam']==0){
	$TeamName = "自由車手";
	
}
else{
	//有加入車隊的人
	
	//找出對應車隊名稱
	$result = mysql_query("SELECT  team.TeamName 
								FROM tb_team 		    as team, 
								     tb_team_attendee   as attendee
								WHERE team.TeamID = attendee.TeamID
								 AND  attendee.MemberID = '$MemberID' ")or die(mysql_error());
	//找到所屬車隊名稱
	if ($result){
		$row_team = mysql_fetch_assoc($result);
		$TeamName = $row_team['TeamName'];	
	}
	 

}
//圖片
if(!empty($_GET['IMG'])){
	$IDImg = "upLoadImg/".$Mode."_pic/".$_GET['IMG'];
}else if(empty($IDImg)){
	$IDImg = "images/IDImg_default.jpg";
}
if (empty($TeamName)) $TeamName="自由車手";


//修改車手檔案
$editFormAction = $_SERVER['PHP_SELF']."?Mode=rider"; //目前正在執行的檔案名稱
if ((isset($_POST["From_insert"])) && ($_POST["From_insert"] == "form1") ) {

if(!empty($_POST['IMG']) || !empty($row_ID['IDImg'])){
	
	if(!empty($_POST['IMG'])) $IDImg = "upLoadImg/".$Mode."_pic/".$_POST['IMG'];

	require_once "Connections/PHP_formvalidator.php";//載入PHP表單驗證檔
    /*做這些PHP驗證*/
    $validator = new FormValidator();
	$validator->addValidation("City","req","請輸入City");	
	$validator->addValidation("City","alnum_s","只能輸入英文字母、數字(准許空白)");	
	
    $validator->addValidation("ride_year","num","請輸入ride_year");	

	if(!$validator->ValidateForm())	{
	//若驗證失敗進入此區
	 	echo "<B>驗證錯誤:</B>";

        $error_hash = $validator->GetErrors();
        foreach($error_hash as $inpname => $inp_err)
        {
            echo "<p>$inpname : $inp_err</p>\n";
        }        
		exit;
	}

	
	$id_GeoID=$_POST["id_GeoID"];
	
	
	$RideAge = $_POST['ride_year'];
	
	$Email=$_SESSION["Email"];
	 	
	if ( !empty($id_GeoID) ){//確認更改後城市以及圖片不為empty 執行UPDATE

		$query = "UPDATE  tb_members SET IDImg='$IDImg' , GeoID='$id_GeoID' , RideAge='$RideAge' WHERE Email='$Email' ";
		mysql_query($query,$dbConn) or die(mysql_error())	;
		//修改cookie GeoID
		//setcookie("GeoID", $id_GeoID, strtotime("1 January 2020")); //用來存放 您現在位置代碼
		//$_SESSION["GeoID"] = $id_GeoID;
		
		/* 確認參賽資格(tb_bike是否至少有1台車)&(有設定單車圖片)&(車手檔案有設個人照片)  
			則tb_members →AccessLevel=2.一般用戶
		*/
		
		//找出myBike	 的BikeImg	
		$result_myBike = mysql_query("SELECT BikeImg 
									FROM     tb_bike 	
									WHERE 	 MemberID = '$MemberID'")or die(mysql_error());
		$row_myBike = mysql_fetch_assoc($result_myBike);
		
		//檢查有單車照片+有個人照片
		if ( !empty($row_myBike["BikeImg"]) && !empty($IDImg) && $Access == 1) {

		//	則tb_members →AccessLevel=2.一般用戶
			$update_sql = "Update tb_members SET AccessLevel = 2 WHERE MemberID='$MemberID' ";
			mysql_query($update_sql,$dbConn) or die(mysql_error())	;
			
			$now = date("Y-m-d H:i:s");
			
			//發布正式車手事件
		$insertSQL = sprintf("
				INSERT INTO tb_event 
				(EventType, MemberID, GeoID, BikeID, TeamID, MemberCareerID, TeamCareerID, TeamAttendeeID ,FollowMemberID, FollowTeamID, EventDate)
				VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s') " , 
					4, 
					$MemberID,
					$GeoID,
					0,
					0,
					0, 
					0,
					0, 
					0,
					0,
					$now );
					
			$result = mysql_query($insertSQL) or die(mysql_error());
			
		}
		
		$editFormAction = $editFormAction."&OK=2";
		header("Location:  $editFormAction");//重新載入page

	}
}else{//沒有圖片

	$editFormAction = $editFormAction."&OK=1";
	header('Location:  $editFormAction');
}

}
?>
<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/redmond/jquery-ui.css">	
<link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css"/>
<link href="css/tooltip.css" rel="stylesheet" type="text/css" />
<!--所在地區-->
<script src="CollapsiblePanel/CollapsiblePanel.js" type="text/javascript"></script>
<!--所在地區-->
<!--lightbox-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<script type="text/javascript" src="lightbox/custom.js"></script>
<!--lightbox-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
<script src="http://cdn.jquerytools.org/1.2.5/full/jquery.tools.min.js"></script> 
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.11/jquery-ui.min.js"></script>

<script src="js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
</style>

<script>
$(document).ready(function(){
	$("#City").focus(function() {
		// 會依據國家欄位的值 去判斷 有哪些city
		var Country_city = "signup_CountryCity.php?kind=City&City_Country=" + document.getElementById("Country").value;
			$("#City").autocomplete({
			source: Country_city ,
			minLength: 0,
			select: function(event, ui) {
				$('#id_GeoID').val(ui.item.id); //將GeoID值存到hidden 方便傳送
			}
		});
	});							//國家城市欄位找值--------------------
	
	$('#submit').click(function (){ //將網頁載入時預設在#City的值利用AJAX轉換成GeoID
		   $.ajax({
		   url: 'signup_CountryCity.php',
		   cache: false,
		   dataType: 'text', //dataType 預計會從url回傳的dataType  類型有4種：html,xml,json,text
			   type:'GET',
		   data: { kind: 'City' ,City_Country: $('#Country').val() ,term: $('#City').val()},//帶過去url的變數名稱
		   error: function(xhr) {
			 alert('Ajax request 發生錯誤');
		   },
		   success: function(response) {// request succeeds的回傳值
		   var jsonContent = eval("(" + response + ")");//將JSON文字轉成物件
			 $('#id_GeoID').val(jsonContent[0].id);
		   }
		   
	   });
	});
/*	
	$("a#changePhoto").click(function(){
		$("div#upload_crop").empty().load("upload_crop/upload_crop.php?upload_kind=user");
		return false;
    });
	
		/* 錯誤顯示位置在 jquery.validationEngine.js promptPosition: "centerLeft"  
	   錯誤訊息框的寬度設定在validationEngine.jquery.css	*/
	jQuery("#form1").validationEngine(); //不可放在國家欄位找值之前 會失效
	


});
</script>

</head>

<body>
<div id="signup">
<?php require_once('include_header.php'); ?>
		<div id="container">
                    	<div id="main">
                        <div id="title" >
                        	<div class="button"><a href="settings.php" class="word_type_bb14">帳戶</a>．<a href="settings_rider.php" class="word_type_bb14">車手</a>．<a href="settings_bike.php" class="word_type_bb14">單車</a><?php if($rowCreator == 1){ ?>．<a href="settings_team.php" class="word_type_bb14">車隊</a><?php }else{ echo ""; } ?></div>
                        	<div class="text word_type_bb24">車手設定</div>
                        </div>

                        <?php
if(empty($_GET['OK'])){
}else if($_GET['OK'] == 1){
?>
<div style="background:#f8f8f8; padding:5px; font-weight: bold; margin-bottom:5px;" align="center" class="word_type_r12">存取失敗</div>
<?php
}else if($_GET['OK'] == 2){
?>
<div style="background:#f8f8f8; padding:5px; font-weight: bold; margin-bottom:5px;" align="center" class="word_type_green12" >存取成功</div>
<?php
}
?>
<form action="<?php echo $editFormAction; ?>" method="POST" enctype="multipart/form-data" name="form1" id="form1">
						<div id="settings">
                        <div class="box"><span class="word_type_bb12">下列皆為必填欄位, 若有變更請完成後再儲存</span></div>
						  <div class="block">
					      <table width="100%" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td valign="top"><div class="topic">車手照片</div></td>
                                  <td width="100%"><div class="picb"><img border="0" src="<?php echo $IDImg ;?>" width="100%" height="100%"/></div>
									<div class="text word_type_g12"><a class="popup"  rel="popupre" href="#">變更照片</a></div>
                                    <div class="text word_type_g12">建議使用方形200px*200px以上<br />
並小於100KB之.jpg或.gif圖檔</div></td>
                                </tr>
                  </table>
                        </div>
                        <div class="block">
                              <table width="100%" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td valign="top"><div class="topic">國籍</div></td>
                                  <td width="100%"><div class="text">
                                    <input name="Country" type="text" id="Country" value="<?php echo $Country_reg ;?>" disabled="disabled" />
                                  </div></td>
                                </tr>
                              </table>
                          </div>
                            <div class="block">
                              <table width="100%" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td valign="top"><div class="topic">現居都市</div></td>
                                  <td width="100%"><div class="text">
                                    <input name="City" type="text" class="validate[required,minSize[2],custom[onlyLetterSp]] text-input" id="City" title="您所在的城市(或欄位空白時按↓自動選擇)" value="<?php echo $City_reg;?>" />
                                  </div>
                                  <div class="text word_type_g12">您的居住城市將會影響系統推薦給您的賽事資訊</div></td>
                                </tr>
                              </table>
                          </div>
                        <div class="block">
                              <table width="100%" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td valign="top"><div class="topic">單車生涯</div></td>
                                  <td width="100%"><div class="text">
                                    <select name="ride_year" id="ride_year"  autocomplete="off" >
                                      
                                      <?php 
									 for ($i=1920; $i<=date("Y");$i++){
										 if ($i==$selected_year) {
											$selected="selected";
											}
										 else{
											$selected="";
											}
										echo "<option  value=\"$i\" $selected >$i</option>";
									 }?>
                                    </select>
                                    <br />
                                  </div></td>
                                </tr>
                          </table>
                          </div>
                          
                          <?php if ($row["IsTeam"]==1) {?>
                          <div class="block">
                              <table width="100%" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td valign="top"><div class="topic">效力車隊</div></td>
                                  <td width="100%"><div class="text">
                                  
                                    <input name="TeamName" type="text" disabled="disabled" id="TeamName" value="<?php echo $TeamName;?>" />
                                     
                                  </div></td>
                                </tr>
                              </table>
                          </div>
                          <?php } ?>
                          <div class="box_bottom">
                            <input class="word_type_wb12" type="submit" name="submit" id="submit" value="送出" />
<input type="hidden" id="IMG" name="IMG" value="<?php echo $_GET['IMG']; ?>"  />                             <input type="hidden" name="From_insert" value="form1" />
							<input type="hidden" id="id_GeoID" name="id_GeoID" value="<?php echo $GeoID ;?>" />  
                          </div>
                        </div>
                    </form>

                    </div>
	  </div>
	</div>
</div>
<!--lightbox_upload-->
	<div id="popupre" class="popupbox">
		<form name="form1" id="form1" action="<?php echo $uploadHandler; ?>?Mode=<?php echo $Mode; ?>" enctype="multipart/form-data" method="post" >
			<div class="back_sb"></div>
  			<div id="login">
            	<div class="box_top word_type_bb18">
                <?php
				if($Mode == "bike"){
	echo "單車";
}else if($Mode == "team"){
	echo "車隊";
}else{
	echo "車手";
}
                ?>照片</div>
            	<div class="text">上傳圖片<br />
            		<input id="file" type="file" name="file">               
				</div>
                <div class="box_bottom">
                	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                    	<tr>
							<td align="right">
								<input class="word_type_wb12" type="submit" name="submit" id="submit" value="上傳" />
								
							</td>
						</tr>
					</table>
				</div>
			</div>
    	</form>
	</div>
    <div id="fade"></div>
<!--lightbox_upload-->
<script type="text/javascript" charset="utf-8">
$('.back_sb').css({'filter' : 'alpha(opacity=40)'});
</script>
</body>
</html>
