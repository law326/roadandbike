<?php
/* 給GeoID找出對應的經緯度
input: GeoID
output: $latlng  (lat,lng)
*/
function geo_to_latlng ($GeoID)
{	
switch ($GeoID){
	case 1://Keelung
	return '25.104870, 121.701891';

	case 2://Taipei
	return '25.086526, 121.552203';

	case 3://Taoyuan
	return '24.920685, 121.218493';

	case 4://Hsinchu
	return '24.706286, 121.125109';

	
	case 5:// Miaoli
	return '24.491517, 121.004260';


	case 6://Taichung
	return '24.233810, 120.941088';


	case 7://Changhua
	return '23.993147, 120.485156';

	
	case 8://Nantou
	return '23.834967, 120.987780';

	
	case 9://Yunlin
	return '23.774657, 120.397265';

	
	case 10://Chiayi
	return '23.457571, 120.573046';


	case 11://Tainan
	return '23.162451, 120.312121';

	
	case 12://Kaohsiung
	return '23.010854, 120.669177';

	
	case 13://Pingtung
	return '22.547434, 120.619738';

	
	case 14://Yilan
	return '24.711276, 121.657946';

	
	case 15://Hualien
	return '23.759575, 121.353075';

	
	case 16://Taitung
	return '22.988100, 120.987780';


	case 17://Penghu
	return '23.540692, 119.622729';

	
	case 18://Kinmen
	return '24.476519, 118.298876';

	
	case 19://Lienchiang
	return '26.149396,119.938314';

	
	case 20://Hong Kong Island
	return '22.247860 ,114.203384 ';

	
	case 21://Kowloon
	return '22.318567 ,114.179606 ';


	case 22://New Territories
	return '22.406083 ,114.120154 ';

	
	default:
	return '25.036150,121.520119';
	}

}

?>