<?php
/* 給車手ID找出對應的車子
input: MemberID
output: 車子的相關資訊
*/
function rider_bike($MemberID)
{
	$result = mysql_query("SELECT br.BrandName ,md.BikeModel, tp.BikeTypeID , ge.GearingName, wh.WheelName ,bk.BikeImg
								FROM tb_bike 		 as bk, 
								     tb_bike_brand   as br, 
									 tb_bike_model   as md, 
									 tb_bike_type 	 as tp,
									 tb_bike_gearing as ge ,
									 tb_bike_wheel 	 as wh
								WHERE bk.BrandID = br.BrandID
								 AND  bk.ModelID = md.ModelID
								 AND  bk.BikeTypeID = tp.BikeTypeID
								 AND  bk.GearingID = ge.GearingID								 
								 AND  bk.WheelID = wh.WheelID								 					 
								 AND  bk.MemberID = '$MemberID'
								")or die(mysql_error());
	$row = mysql_fetch_assoc($result);	
	return array($row['BrandName'] ,$row['BikeModel'] ,$row['BikeTypeID'] ,$row['GearingName'] ,$row['WheelName'] ,$row['BikeImg'] );

}

?>