<?php
session_start();
/* 甲：判斷是否登入
 * 判斷訪客狀態(登入、未登入)，再決定您 目前所在的城市 資訊
 * 流程: 是否有session---- True ●登入
 *					 ｜__ False--- 是否有cookie---- True ------------------cookie驗證正確嗎?---- True ●登入 
 *							   				  ｜__ False ●未登入，一般訪客				  ｜__ False ●疑似問題訪客,將其導向首頁
 * 
*/

/* 沒登入不可瀏覽的頁面 寫在需要限制的頁面內 */

/* 乙：用session存GeoID
 * 您目前所在的城市 改用session紀錄(和眼中確認過切換city不儲存) 判別優先順序
 * 1.由IP辨識GeoID
 * 2.若IP失效，會員由所填的居住位置顯示
 * 3.非登入則預設為Taipei
*/

require_once('dbConn.php');
require_once('find_GeoID.php');//function GeoID相關

	
/* 甲：判斷是否登入 開始*/
/* 是否有session  判斷是否已登入 */
if ($_SESSION['islogin'] == TRUE) {
	//●已登入  在此會有session代表已經剛從login頁面或剛註冊過來

	/* 讀取會員資料 方便之後頁面使用, 可以直接用row[]讀取 */
	$MemberID=$_SESSION['MemberID'];

	$query = "SELECT * FROM tb_members WHERE MemberID='$MemberID' ";
	$result = mysql_query($query,$dbConn) or die(mysql_error());
	$row = mysql_fetch_assoc($result);
	$IsTeam = $row['IsTeam'];
	
	$MemberName = $row['MemberName'];
	//echo "logged!!";

}
else{
	/* 是否有cookie 登入時有勾選"記住我" 在此以cookie判斷身分 */
	if (isset($_COOKIE["isLogin_Email"]) and isset($_COOKIE["isLogin_Hash"])){
		//有cookie進行驗證	 
		
		$Email=$_COOKIE["isLogin_Email"];
		$CookieCheck=$_COOKIE["isLogin_Hash"];
		
		/* 讀取會員資料 方便之後頁面使用, 可以直接用row[]讀取 */
		$query = "SELECT * FROM tb_members WHERE Email='$Email' AND  CookieCheck='$CookieCheck' ";
		$result = mysql_query($query,$dbConn) or die(mysql_error());		
		$row = mysql_fetch_assoc($result);
		
		$num_rows = mysql_num_rows($result);
		//●驗證成功 已登入
		/* 確定登入 給這些值 統一在此配發通行證  */
		/* 網站只有3個登入點login_confirm.php 和 autologin.php 和signup_confirm.php會配發session通行證  */
		if ($num_rows !=0 ){
			$_SESSION['islogin'] = true; // 確定登入 給一個session判別
			$_SESSION['Register_from'] = $row["Register_from"]; 
			$_SESSION['MemberID']= $row["MemberID"];
			$_SESSION["Email"] = $row["Email"]; 
			//setcookie("GeoID", $row_login['GeoID'], $expiredate); //用來存放 您現在位置代碼
			
			$MemberName = $row['MemberName'];
			$MemberID=$_SESSION['MemberID'];	
			
			
		}//有cookie的登入End
		
		//●疑似問題訪客,將其導向首頁 ,似乎有bug,當登入後有cookie,但下次登入時找不到cookie(有可能這次登入是在無法保留cookie的環境)，被系統判斷為"惡意登入"
		//if ($num_rows == 0) header("location: index.php");  
	
	}
	else{
		//●未登入，一般訪客

		$MemberName ="訪客請登入~";


	}//是否有cookie End


}//是否有登入session End
// 甲 結束


/* 乙：用session存GeoID 開始*/
if ( ! isset($_SESSION["GeoID"])){
	
	/*乙1 先以IP判斷位置試試看 若失敗再考慮乙2,乙3 */
	require_once('Geoip/Geoip_include.php');
	$Country = $record->country_name;
	$City = $record->city;
	geoip_close($gi);
	
	if (empty($City)) {
		//IP判斷失敗
		
		/* 乙2 如果有登入就以註冊地來用 */
		if ($_SESSION['islogin'] == TRUE){
			$_SESSION["GeoID"] = $row["GeoID"]; //用session來存放 您現在位置代碼
			//echo "從登入判斷出 您的城市GeoID:"  . $_SESSION["GeoID"] ."HI".$Email."cookie_check".$CookieCheck;
			//exit;
		}
		else{
			/* 乙3 沒登入就用預設台北 */
			$_SESSION["GeoID"] = 2;
			//echo "沒登入就用預設 您的城市GeoID:"  . $_SESSION["GeoID"] ;
			//exit;
		}
		
	}
	else{
		//有從IP判斷出city

		list($GeoID) = city_to_geoid($City);//城市轉換成GeoID
		if (empty($GeoID)) die('GeoID not exist! autologin.php');

		//echo "從IP判斷出 您的城市GeoID:"  . $GeoID;
		$_SESSION["GeoID"] = $GeoID;
	}
	
	
}// 乙 結束

/* 丙：用將GeoID 生成國家、城市(不要放在乙,因為有可能會切換城市) 開始*/
if (isset($_SESSION["GeoID"])){

	$GeoID = $_SESSION["GeoID"];
	//將GeoID轉成 國家和城市
	list($Country,$City) = geoid_to_city($GeoID);
	if (empty($City)) die('City not exist! autologin.php');
	//echo $City;
	//exit;
}// 丙 結束