<div  style=" margin-top:-5px; height:30px; margin-bottom:10px; border-width:1px; border-color:#ccc; border-style:dotted; padding-top:5px; padding-left:10px; padding-right:10px;"><!-- share box -->

<!-- google Analytics -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-5368799-3']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!-- google Analytics -->

<!-- google+ -->
<div class="button_signup" style="float:right; padding-top:2px;" align="right">

<!-- Place this tag where you want the +1 button to render -->
<g:plusone size="medium" annotation="inline" width="200"></g:plusone>

<!-- Place this render call where appropriate -->
<script type="text/javascript">
  window.___gcfg = {lang: 'zh-TW'};

  (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/plusone.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
</script>                          

</div>
<!-- google+ -->
<!-- facebook -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) {return;}
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="fb-like" data-href="<?php $editFormAction; ?>" data-send="true" data-width="400" data-show-faces="false"></div>

<!-- facebook -->
</div><!-- share box end -->