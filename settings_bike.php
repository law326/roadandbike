<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php
$page = "settingsbike";
require_once('include_webtitle.php');//標題檔
$pagestyle = "setting";//提供header樣式判斷
?>
<?php
/* -----bobo註解-----
 * module:jQuery-Validation-Engin、jQueryTip、PHP表單驗證檔
 * note:	 
 *
 *
*/
session_start();

unset($_SESSION["upload_kind"]);
require_once('Connections/autologin.php');//判斷訪客狀態(登入、未登入)，再決定您"目前所在的城市"資訊

//●未登入不可瀏覽 
if ($_SESSION['islogin'] == FALSE) header("Location:  index.php");  

$GeoID = $row["GeoID"];

$Mode = 'bike';

$resultCreator= mysql_query(" SELECT * FROM tb_team WHERE CreatorID = '$MemberID' ")or die(mysql_error());
$rowCreator =  mysql_num_rows($resultCreator);

// 取此檔所在位置
$directory_self = str_replace(basename($_SERVER['PHP_SELF']), '', $_SERVER['PHP_SELF']);

// 上傳處理的位置
$uploadHandler = 'http://' . $_SERVER['HTTP_HOST'] . $directory_self . 'upLoadImg/upLoadImg_process.php';

$max_file_size = 100000; // 限100KB , 硬性則要修改php.ini


//撈系統出所有車種
$result_bikeType = mysql_query("SELECT * FROM tb_bike_type ")or die(mysql_error());
$result_bikeImg = mysql_query("SELECT * FROM tb_bike WHERE MemberID = '$MemberID' ")or die(mysql_error());
$row_bile = mysql_fetch_assoc($result_bikeImg);
require_once('Connections/find_rider_bike.php');//給車手ID找出對應的車子資訊
/* 找出車子資訊 */
list($BrandName,$BikeModel,$BikeTypeID,$GearingName,$WheelName,$BikeImg) = rider_bike($MemberID);


//圖片
if(!empty($_GET['IMG'])){
	$BikeImg = "upLoadImg/".$Mode."_pic/".$_GET['IMG'];
}else if(empty($BikeImg)){
	$BikeImg = "images/BikeImg_default.jpg";
}

//這台車 的車種ID 給下拉式選單
$selected_type = $BikeTypeID;


//查自己的單車數量
$result_count = mysql_query("SELECT COUNT(MemberID) FROM tb_bike WHERE MemberID='$MemberID' ")or die(mysql_error());

$row_count = mysql_fetch_assoc($result_count);

if ($row_count['COUNT(MemberID)'] == 0){
	$mess_myBike="趕快設定您的第一台車吧!";
}
else{
	$mess_myBike="目前車庫有". $row_count['COUNT(MemberID)'] ."台車";
}

//修改單車檔案
$editFormAction = $_SERVER['PHP_SELF']."?Mode=bike"; //目前正在執行的檔案名稱
if ((isset($_POST["From_insert"])) && ($_POST["From_insert"] == "form1" )) {

if(!empty($_POST['IMG']) || !empty($row_bile['BikeImg'])){
	
	if(!empty($_POST['IMG'])) $BikeImg = "upLoadImg/".$Mode."_pic/".$_POST['IMG'];

	require_once "Connections/PHP_formvalidator.php";//載入PHP表單驗證檔
    /*做這些PHP驗證*/
    $validator = new FormValidator();
    $validator->addValidation("brand","req","請輸入brand");
	$validator->addValidation("brand","alnum_s","只能輸入英文字母、數字(准許空白)");	

    $validator->addValidation("bikeModel","req","請輸入bikeModel");
	$validator->addValidation("bikeModel","alnum_s","只能輸入英文字母、數字(准許空白)");	
	
	$validator->addValidation("GearingSystem","req","請輸入GearingSystem");
	$validator->addValidation("GearingSystem","alnum_s","只能輸入英文字母、數字(准許空白)");	
		
    $validator->addValidation("Wheel","req","請輸入Wheel");
	$validator->addValidation("Wheel","alnum_s","只能輸入英文字母、數字(准許空白)");	
		
	if(!$validator->ValidateForm())	{
	//若驗證失敗進入此區
	 	echo "<B>驗證錯誤:</B>";

        $error_hash = $validator->GetErrors();
        foreach($error_hash as $inpname => $inp_err)
        {
            echo "<p>$inpname : $inp_err</p>\n";
        }        
		exit;
	}

	/*修改的資料*/
	$bikeType = $_POST["bikeType"]; //直接存車種ID
	$brand = $_POST["brand"];
	$bikeModel = $_POST["bikeModel"];
	$GearingSystem = $_POST["GearingSystem"];
	$Wheel = $_POST["Wheel"];
	
	/*更新tb_bike之前
		甲.確認更新名稱已在DB	乙.由DB得出此名稱ID
		甲的方法1 SELECT→If→INSERT  方法2 用INSERT..SELECT not exists判斷		方法3用INSERT IGNORE 省下一個判斷式
	*/

	
	/*甲的方法2也OK,優點：此欄位不需設定成unique index
	$insert = "INSERT INTO tb_bike_brand(BrandName) 
				SELECT '$brand' FROM dual 
					WHERE not exists (select BrandName from tb_bike_brand  where BrandName='$brand')";//方法2
	*/
		
	//甲的方法3 此欄位要設定成unique index 才使用IGNORE
	//$insert = "INSERT IGNORE INTO tb_bike(BikeImg) VALUES('$BikeImg') "; //插入圖片
	//$result = mysql_query($insert, $dbConn) or die(mysql_error());
	
	$insert = "INSERT IGNORE INTO tb_bike_brand(BrandName) VALUES('$brand') "; //插入品牌
	$result = mysql_query($insert, $dbConn) or die(mysql_error());
	
	$insert = "INSERT IGNORE INTO tb_bike_model(BikeModel) VALUES('$bikeModel') "; //插入型號
	$result = mysql_query($insert, $dbConn) or die(mysql_error());

	$insert = "INSERT IGNORE INTO tb_bike_gearing(GearingName) VALUES('$GearingSystem') "; //插入變速系統
	$result = mysql_query($insert, $dbConn) or die(mysql_error());
	
	$insert = "INSERT IGNORE INTO tb_bike_wheel(WheelName) VALUES('$Wheel') "; //插入輪組
	$result = mysql_query($insert, $dbConn) or die(mysql_error());	 

	
	/*將接收到的 品牌、型號...轉換為ID 並且執行tb_bike更新*/
	$brand_ID = 		"SELECT BrandID   FROM tb_bike_brand   WHERE BrandName = '$brand' "; //拿品牌ID
	$bikeModel_ID =		"SELECT ModelID   FROM tb_bike_model   WHERE BikeModel = '$bikeModel' "; //拿型號ID
	$GearingSystem_ID = "SELECT GearingID FROM tb_bike_gearing WHERE GearingName = '$GearingSystem' "; //拿變速系統ID
	$Wheel_ID =			"SELECT WheelID   FROM tb_bike_wheel   WHERE WheelName = '$Wheel' "; //拿輪組ID
		
	if ($row_count['COUNT(MemberID)']==0){
		
		//如果沒有單車 創造一台 插入主表tb_bike
		$insertSQL = "INSERT INTO tb_bike(MemberID,BrandID,ModelID, BikeTypeID, GearingID, WheelID, BikeImg) 
					VALUES ('$MemberID',({$brand_ID}), ({$bikeModel_ID}),'$bikeType', ({$GearingSystem_ID}), ({$Wheel_ID}) , '$BikeImg')" ;
		mysql_query($insertSQL,$dbConn) or die(mysql_error())	;
		
		//現在已經有單車了,只要在確認(車手檔案有設個人照片) 和 (有單車照片)就可以參賽   tb_members →AccessLevel=2
		$IDImg=$row["IDImg"];
		$MemberID=$row["MemberID"];
		
		if ($IDImg != NULL && $BikeImg != NULL){
			//將tb_members →AccessLevel=2
			$update_sql = "Update tb_members SET AccessLevel = 2 WHERE MemberID='$MemberID' ";
			mysql_query($update_sql,$dbConn) or die(mysql_error())	;
			
			$now = date("Y-m-d H:i:s");
			
			//發布正式車手事件
		$insertSQL = sprintf("
				INSERT INTO tb_event 
				(EventType, MemberID, GeoID, BikeID, TeamID, MemberCareerID, TeamCareerID, TeamAttendeeID ,FollowMemberID, FollowTeamID, EventDate)
				VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s') " , 
					4, 
					$MemberID,
					$GeoID,
					0,
					0,
					0, 
					0,
					0, 
					0,
					0,
					$now );
					
			$result = mysql_query($insertSQL) or die(mysql_error());
		}
		
	}
	else{
		
		//已經有車子, 更新它
		$update_sql = "Update tb_bike SET 
						tb_bike.BrandID = ({$brand_ID}) ,
						tb_bike.ModelID = ({$bikeModel_ID}),
						tb_bike.BikeTypeID = ({$bikeType}),
						tb_bike.GearingID = ({$GearingSystem_ID}),
						tb_bike.WheelID = ({$Wheel_ID}),
						tb_bike.BikeImg = '$BikeImg'
						 WHERE MemberID='$MemberID' ";
		mysql_query($update_sql,$dbConn) or die(mysql_error())	;
	}
	
	$editFormAction = $editFormAction."&OK=2";
	header("Location:  $editFormAction");//重新載入page
	
}else{//沒有照片

	$editFormAction = $editFormAction."&OK=1";
	header("Location:  $editFormAction");//重新載入page

}
}
?>
<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/redmond/jquery-ui.css">	
<link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css"/>
<link href="css/tooltip.css" rel="stylesheet" type="text/css" />
<!--所在地區-->
<script src="CollapsiblePanel/CollapsiblePanel.js" type="text/javascript"></script>
<!--所在地區-->
<!--lightbox-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<!--lightbox-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
<script src="http://cdn.jquerytools.org/1.2.5/full/jquery.tools.min.js"></script> 
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.11/jquery-ui.min.js"></script>

<script src="js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
</style>

<script>
$(document).ready(function(){

	$("#brand").focus(function() { //單車品牌欄位找值--------------------
		$("#brand").autocomplete({
			source: "settings_bikeAJAX.php?kind=brand", 
			minLength: 0,
			select: function(event, ui) {
				$('#id_BrandID').val(ui.item.id); //將GeoID值存到hidden 方便傳送
			}
		});
	});	//單車品牌欄位找值--------------------
	
		/* 錯誤顯示位置在 jquery.validationEngine.js promptPosition: "centerLeft"  
	   錯誤訊息框的寬度設定在validationEngine.jquery.css	*/
	jQuery("#form1").validationEngine(); //不可放在國家欄位找值之前 會失效
});
</script>

</head>

<body>
<div id="signup">
<?php require_once('include_header.php'); ?>
		<div id="container">
                    	<div id="main">
                        <div id="title" >
                        	<div class="button"><a href="settings.php" class="word_type_bb14">帳戶</a>．<a href="settings_rider.php" class="word_type_bb14">車手</a>．<a href="settings_bike.php" class="word_type_bb14">單車</a><?php if($rowCreator == 1){ ?>．<a href="settings_team.php" class="word_type_bb14">車隊</a><?php }else{ echo ""; } ?></div>
                        	<div class="text word_type_bb24">單車設定</div>
                        </div>

<?php
if(empty($_GET['OK'])){
}else if($_GET['OK'] == 1){
?>
<div style="background:#f8f8f8; padding:5px; font-weight: bold; margin-bottom:5px;" align="center" class="word_type_r12">存取失敗</div>
<?php
}else if($_GET['OK'] == 2){
?>
<div style="background:#f8f8f8; padding:5px; font-weight: bold; margin-bottom:5px;" align="center" class="word_type_green12" >存取成功</div>
<?php
}
?>
<form action="<?php echo $editFormAction; ?>" method="POST" enctype="multipart/form-data" name="form1" id="form1">
						<div id="settings">
                        <div class="box"><span class="word_type_bb12">下列皆為必填欄位, 若有變更請完成後再儲存</span></div>
						  <div class="block">
                            <table width="100%" cellspacing="0" cellpadding="0">
                              <tr>
                                <td valign="top"><div class="topic">單車照片</div></td>
                                <td width="100%"><div class="picb"><img border="0" src="<?php echo $BikeImg;?>" width="100%" height="100%"/></div>
                                  <div class="text word_type_g12"><a class="popup" href="#" rel="popupre">變更照片</a></div>
                                  <div class="text word_type_g12">建議使用方形200px*200px以上<br />
                                  並小於100KB之.jpg或.gif圖檔</div></td>
                              </tr>
                            </table>
                          </div>
                          <div class="block">
                            <table width="100%" cellspacing="0" cellpadding="0">
                              <tr>
                                <td valign="top"><div class="topic">車種</div></td>
                                <td width="100%"><div class="text">
                                  <select name="bikeType" id="bikeType">
                                  <?php 
								  while($rowType = mysql_fetch_assoc($result_bikeType)){
									  if ($rowType["BikeTypeID"]==$selected_type) {
											$selected="selected";
											}
										 else{
											$selected="";
											}
									 echo "<option  value=\"" .$rowType["BikeTypeID"].  " \" $selected>" .$rowType["TypeName"] ."</option>"; 
								  }?>
                                  </select>
                                  <br />
                                </div>
                                <div class="text word_type_g12">請慎選單車的類別, 日後將會影響比賽的權利</div></td>
                              </tr>
                            </table>
                        </div>
                            <div class="block">
                              <table width="100%" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td valign="top"><div class="topic">
                                    單車品牌
                                  </div></td>
                                  <td width="100%"><div class="text">
                                    <input name="brand" type="text" class="validate[required,custom[onlyLetterNumber]] text-input" id="brand" value="<?php echo $BrandName ;?>"/>
                                  </div></td>
                                </tr>
                              </table>
                          </div>
                            <div class="block">
                              <table width="100%" cellspacing="0" cellpadding="0">
                              <tr>
                                <td width="100%"><div class="text">
                                  <table width="100%" cellspacing="0" cellpadding="0">
                                    <tr>
                                      <td valign="top"><div class="topic">
                                        單車型號
                                      </div></td>
                                      <td width="100%"><div class="text">
                                        <input name="bikeModel" type="text" class="validate[required,custom[onlyLetterNumber]] text-input" id="bikeModel" value="<?php echo $BikeModel ;?>" />
                                      </div></td>
                                    </tr>
                                  </table>
                                </div></td>
                              </tr>
                              </table>
                            </div>
                          <div class="block">
                              <table width="100%" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td valign="top"><div class="topic">變速系統</div></td>
                                  <td width="100%"><div class="text">
                                    <input name="GearingSystem" type="text" class="validate[required,custom[onlyLetterNumber]] text-input" id="GearingSystem" value="<?php echo $GearingName ;?>" />
                                  </div></td>
                                </tr>
                              </table>
                          </div>
                          <div class="block">
                              <table width="100%" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td valign="top"><div class="topic">輪組</div></td>
                                  <td width="100%"><div class="text">
                                    <input name="Wheel" type="text" class="validate[required,custom[onlyLetterNumber]] text-input" id="Wheel" value="<?php echo $WheelName ;?>" />
                                  </div></td>
                                </tr>
                            </table>
                          </div>
                          <!--隱藏欄位 傳值 id_BrandID-->
                          <input type="hidden" id="IMG" name="IMG" value="<?php echo $_GET['IMG']; ?>"  /> 
                          <input type="hidden" id="id_BrandID" name="id_BrandID"  />  
                          <input type="hidden" name="From_insert" value="form1" />
                          <div class="box_bottom">
                            <input class="word_type_wb12" type="submit" name="submit" id="submit" value="送出" />
                            
                          </div>
                        </div>
                    </form>


                    </div>
	  </div>
	</div>
</div>
<!--lightbox_upload-->
	<div id="popupre" class="popupbox">
		<form name="form1" id="form1" action="<?php echo $uploadHandler; ?>?Mode=<?php echo $Mode; ?>" enctype="multipart/form-data" method="post" >
			<div class="back_sb"></div>
  			<div id="login">
            	<div class="box_top word_type_bb18">
                <?php
				if($Mode == "bike"){
	echo "單車";
}else if($Mode == "team"){
	echo "車隊";
}else{
	echo "車手";
}
                ?>照片</div>
            	<div class="text">上傳圖片<br />
            		<input id="file" type="file" name="file">               
				</div>
                <div class="box_bottom">
                	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                    	<tr>
							<td align="right">
								<input class="word_type_wb12" type="submit" name="submit" id="submit" value="上傳" />
								
							</td>
						</tr>
					</table>
				</div>
			</div>
    	</form>
	</div>
    <div id="fade"></div>
<!--lightbox_upload-->
<script type="text/javascript" charset="utf-8">
$('.back_sb').css({'filter' : 'alpha(opacity=40)'});
</script>
</body>
</html>
