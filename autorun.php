<?php

session_start();

require_once('Connections/autologin.php');//登入
require_once('Connections/find_LatLng.php');//GeoID

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="refresh" content="1;URL=autorun.php">
<title>autorun</title>
</head>

<body style="padding:1%;">


<!--會員數-->
<?php
$result_MemNum = mysql_query(" SELECT * FROM tb_members ") or die(mysql_error());
$row_MemNum = mysql_num_rows($result_MemNum);
?>
<div style="float:left; width:62%; padding-top:180px; padding-bottom:180px; border-style:dotted; border-color:#000; text-align:center; margin-bottom:32px; ">
<div style="font-weight:bolder; font-size:30px; "><span style="font-size:150px; "><?php echo $row_MemNum; ?></span>人 / 會員</div>
</div>
<!--會員數-->


<!--資訊欄位-->
<div style="float:right; width:35%; padding-top:20px; padding-bottom:20px; border-bottom-style:dotted; border-color:#000; text-align:center; margin-bottom:30px; ">
<div style="font-weight:bolder; "><span style="font-size:40px; "><?php echo date("Y/m/d H:i:s"); ?></span></div>
</div>
<!--資訊欄位-->


<!--賽事欄位-->
<div style="float:right; width:35%; padding-top:10px; ">
<?php 
$now = date("Y/m/d");
$result_RaceInfo = mysql_query(" SELECT * FROM tb_race WHERE RaceExpires >= '$now' ORDER BY RaceExpires ASC LIMIT 20 ") or die(mysql_error());

while($row_RaceInfo = mysql_fetch_assoc($result_RaceInfo)){ //while抓出賽是資料 起始

$RaceID = $row_RaceInfo['RaceID'];
$Title = $row_RaceInfo['Title'];
$GeoID = $row_RaceInfo['GeoID'];

$ScoreMemb = array(0, 16, 12, 9, 7, 6, 5, 4, 3, 2, 1);//個人賽前十名積分
$ScoreTeam = array(0, 32, 24, 18, 14, 12);//團隊賽前五名車隊與前三位積分車手積分
//暫不開放$ScoreTeamMemb = array(0, 16, 12, 9, 7, 6);//團隊賽前五名車隊完賽車手積

list($Country_reg,$City_reg) = geoid_to_city($GeoID);//GeoID to City 

$EndDate = $row_RaceInfo['RaceExpires'];
$EventDate = $EndDate." 23:59:59";//提前一秒結束
require('upLoadTrack/Timer.php');//時間計數器


if(date("Y-m-d H:i:s") == $EventDate){//判斷時間截止 起始
	if($row_RaceInfo['IsGroup'] == 0){//個人賽事 起始
	
		//排序
		$result_Top = mysql_query(" SELECT * FROM tb_race_mcareer as rm, tb_bike as b WHERE rm.MemberID = b.MemberID AND rm.RaceID = '$RaceID' ORDER BY LapTime ASC ") or die(mysql_error());
		$row_Top_Num = mysql_num_rows($result_Top);
		
		//找出第一名, 單車, 參賽紀錄
		while($row_Top = mysql_fetch_assoc($result_Top)){
			$MemberID = $row_Top['MemberID'];
			$BikeID = $row_Top['BikeID'];
			$MemberCareerID = $row_Top['MemberCareerID'];
			
			//寫入事件 起始
			$insertSQL = sprintf(" INSERT INTO tb_event (EventType, MemberID, GeoID, BikeID, TeamID, MemberCareerID, TeamCareerID, TeamAttendeeID ,FollowMemberID, FollowTeamID, EventDate)
				VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s') ", 15, $MemberID, $GeoID, $BikeID, 0, $MemberCareerID, 0, 0, 0, 0, $EventDate);
			$result = mysql_query($insertSQL) or die(mysql_error());
			
			break;
		}
		$row_Top = mysql_data_seek($result_Top, 0);
	
		//給積分
		if($row_Top_Num >= 10){//判斷是否給分 起始
			while($row_Top = mysql_fetch_assoc($result_Top)){
				$i++;
				$MemberCareerID = $row_Top['MemberCareerID'];
				$GetScore = $ScoreMemb[$i];
				
				//寫入生涯紀錄
				$query = " UPDATE tb_race_mcareer SET Score = '$GetScore' WHERE MemberCareerID = '$MemberCareerID' ";
					mysql_query($query,$dbConn) or die(mysql_error());
				
				if($i == 10) break;//完成前十名的車手成績寫入
			}//while 結束
		}//判斷是否給分 結束
			
	}else{//團隊賽事 起始
		
		//排序
		$result_Top = mysql_query(" SELECT * FROM tb_race_tcareer WHERE RaceID = '$RaceID' ORDER BY LapTime ASC ") or die(mysql_error());
		$row_Top_Num = mysql_num_rows($result_Top);
		
		//找出第一名, 單隊, 參賽紀錄
		while($row_Top = mysql_fetch_assoc($result_Top)){
			$MemberID = $row_Top['JoinMem1'];//車隊第一名
			$TeamID = $row_Top['TeamID'];
			$TeamCareerID = $row_Top['TeamCareerID'];
			
			//寫入事件 起始
			$insertSQL = sprintf(" INSERT INTO tb_event (EventType, MemberID, GeoID, BikeID, TeamID, MemberCareerID, TeamCareerID, TeamAttendeeID ,FollowMemberID, FollowTeamID, EventDate)
				VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s') ", 34, $MemberID, $GeoID, 0, $TeamID, 0, $TeamCareerID, 0, 0, 0, $EventDate);
			$result = mysql_query($insertSQL) or die(mysql_error());
			
			break;
		}
		$row_Top = mysql_data_seek($result_Top, 0);
		
		//給積分
		if($row_Top_Num >= 5){//判斷是否給分 起始
			while($row_Top = mysql_fetch_assoc($result_Top)){
				$i++;
				$TeamCareerID = $row_Top['TeamCareerID'];
				$JoinMemArray = array($row_Top['JoinMem1'], $row_Top['JoinMem2'], $row_Top['JoinMem3']);
				$GetScore = $ScoreTeam[$i];
				
				//寫入車隊生涯紀錄
				$query = " UPDATE tb_race_tcareer SET Score = '$GetScore' WHERE TeamCareerID = '$TeamCareerID' ";
					mysql_query($query,$dbConn) or die(mysql_error());
				
				//找隊員生涯紀錄
				for($k=0; $k<=2; $k++){//三名隊員
					$GetMem = $JoinMemArray[$k];

					$result_TopMemb = mysql_query(" SELECT * FROM tb_race_mcareer WHERE RaceID = '$RaceID' AND MemberID = '$GetMem' ") or die(mysql_error());
					$row_TopMemb = mysql_fetch_assoc($result_TopMemb);
					
					$MemberCareerID = $row_TopMemb['MemberCareerID'];
					
					//寫入隊員生涯紀錄
					$query = " UPDATE tb_race_mcareer SET Score = '$GetScore' WHERE MemberCareerID = '$MemberCareerID' ";
						mysql_query($query,$dbConn) or die(mysql_error());	
						
				}//for 結束
				
				if($i == 3) break;//完成前三名的車隊與前三名隊員成績寫入
			}//while 結束
			
		}//判斷是否給分 結束
		
	}//個人/團隊賽事 結束
	
}//判斷時間截止 結束

?>
<div style="font-weight:bolder; border-bottom-style:dotted; font-size:16px; margin-bottom:5px; "><!--框-->
<table>
<tr>
<td width="50px"><?php echo $RaceID; ?></td>
<td width="350px"><?php echo $Title; ?></td>
<td width="100px"><?php echo $City_reg; ?></td>
<td width="150px">
<?php 
//顯示格式調整
if ($HaveD == 0){ 
	echo $HaveH."H ".$HaveM."M ".$HaveS."s"; 
}else if($HaveD == 0 && $HaveH == 0){
	echo $HaveM."M ".$HaveS."s"; 
}else if($HaveD == 0 && $HaveH == 0 && $HaveM == 0){
	echo $HaveS."s"; 
}else{
	echo $HaveD."D ".$HaveH."H ".$HaveM."M ".$HaveS."s"; 
}
?>
</td>
</tr>
</table>
</div><!--框-->
<?php 
}//while抓出賽是資料 結束
?>
</div>
<!--賽事欄位-->


<!--賽事數-->
<?php
$result_RaceNum = mysql_query(" SELECT * FROM tb_race ") or die(mysql_error());
$row_RaceNum = mysql_num_rows($result_RaceNum);
?>
<div style="float:left; width:30%; padding-top:80px; padding-bottom:80px; border-style:dotted; border-color:#000; text-align:center; margin-right:1.5%; ">
<div style="font-weight:bolder; "><span style="font-size:80px; "><?php echo $row_RaceNum; ?></span>場 / 賽事</div>
</div>
<!--賽事數-->


<!--車隊數-->
<?php
$result_TeamNum = mysql_query(" SELECT * FROM tb_team ") or die(mysql_error());
$row_TeamNum = mysql_num_rows($result_TeamNum);
?>
<div style="float:left; width:30%; padding-top:80px; padding-bottom:80px; border-style:dotted; border-color:#000; text-align:center; ">
<div style="font-weight:bolder; "><span style="font-size:80px; "><?php echo $row_TeamNum; ?></span>支 / 車隊</div>
</div>
<!--車隊數-->


</body>
</html>