<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php
if ($_GET['Mode'] == 1) $page = "rankY";
if ($_GET['Mode'] == 2) $page = "rankT";
if ($_GET['Mode'] == 3) $page = "rankP";
require_once('include_webtitle.php');//標題檔
?>
<?php
session_start();

require_once('Connections/autologin.php');//判斷訪客狀態(登入、未登入)，再決定您"目前所在的城市"資訊
require_once('Connections/find_LatLng.php');//由GeoID找出對應的經緯度 這裡給Gmap

$editFormAction = $_SERVER['PHP_SELF']; //目前正在執行的檔案

//判斷顯示的排名
switch ($_GET['Mode'])
	{
	case 1://黃衫
		$resultNum= mysql_query("
			SELECT COUNT(m.MemberID) FROM tb_members as m WHERE m.AccessLevel = 2")or die(mysql_error());
		
		$result= mysql_query("
			SELECT m.MemberName, m.MemberID, m.IDImg, bb.BrandName, bm.BikeModel, b.BikeImg, m.GeoID, 
				SUM(rm.Score), SUM(r.Mileage)
			FROM tb_members as m, tb_bike as b, tb_bike_brand as bb, tb_bike_model as bm, tb_race as r, tb_race_mcareer as rm
			WHERE m.AccessLevel = 2 AND m.MemberID = b.MemberID AND b.BrandID = bb.BrandID AND b.ModelID = bm.ModelID AND
				m.MemberID = rm.MemberID AND r.RaceID = rm.RaceID
			GROUP BY m.MemberID
			ORDER BY SUM(rm.Score) DESC, m.RegistrationDate ASC
			")or die(mysql_error());
			
		break;
	case 2://團隊
		$resultNum= mysql_query("
			SELECT COUNT(t.TeamID) FROM tb_team as t WHERE t.Status > 0")or die(mysql_error());
		
		$result= mysql_query("
			SELECT t.TeamID, t.TeamName, t.TeamImg, t.GeoID, SUM(rt.Score), SUM(r.Mileage)
			FROM tb_team as t, tb_race as r, tb_race_tcareer as rt, tb_race_mcareer as rm, tb_team_attendee as ta
			WHERE t.Status > 0 AND rt.Status = 1 AND t.TeamID = rt.TeamID AND t.TeamID = ta.TeamID AND r.RaceID = rt.RaceID AND
				rm.RaceID = r.RaceID AND r.IsGroup = 1 AND rm.MemberID = ta.MemberID
			GROUP BY t.TeamID
			ORDER BY SUM(rt.Score) DESC, t.TeamDate ASC
			")or die(mysql_error());
			
		break;
	case 3://粉紅衫
		$resultNum= mysql_query("
			SELECT COUNT(m.MemberID) FROM tb_members as m WHERE m.AccessLevel = 2 AND m.Gender = 2")or die(mysql_error());
		
		$result= mysql_query("
			SELECT m.MemberName, m.MemberID, m.IDImg, bb.BrandName, bm.BikeModel, b.BikeImg, m.GeoID, 
				SUM(rm.Score), SUM(r.Mileage)
			FROM tb_members as m, tb_bike as b, tb_bike_brand as bb, tb_bike_model as bm, tb_race as r, tb_race_mcareer as rm
			WHERE m.AccessLevel = 2 AND m.Gender = 2 AND m.MemberID = b.MemberID AND b.BrandID = bb.BrandID AND
				b.ModelID = bm.ModelID AND m.MemberID = rm.MemberID AND r.RaceID = rm.RaceID
			GROUP BY m.MemberID
			ORDER BY SUM(rm.Score) DESC, m.RegistrationDate ASC
			")or die(mysql_error());

		break;
	default:
		echo "No number between 1 and 3";
	}
?>
<!--所在地區-->
<script src="CollapsiblePanel/CollapsiblePanel.js" type="text/javascript"></script>
<!--所在地區-->
<!-- 引用 jQuery 1.5 --> 
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
<!-- 引用 jQuery 1.5 --> 
</head>

<body>
	<?php require_once('include_header.php'); ?>       
		<div id="container">
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
            	<tr>
                	<td valign="top">
                    	<div id="main">
                            <div id="title">
                            	<div class="button"><a href="rank.php?Mode=1" class="word_type_bb14">所有人</a></div>
                           	  <div class="button"></div>
                            <div class="text word_type_bb24">Taiwan, <?php if ($_GET['Mode'] == 1){ echo "黃衫"; }else if ($_GET['Mode'] == 2){ echo "團隊"; }else{ echo "粉紅衫"; } ?>排名</div>
                                <div class="box">累計積分排名第一的
                                <?php if ($_GET['Mode'] == 1){ echo "車手"; }
								elseif ($_GET['Mode'] == 2){ echo "車隊"; }
								else{ echo "女性車手"; } ?>
                                即可獲得這項頭銜．競爭<?php if ($_GET['Mode'] == 2){ echo "車隊"; }
								else{ echo "車手"; } ?> <span class="word_type_blueb12"><a>
								<?php $rowNum = mysql_fetch_assoc($resultNum); 
								if ($_GET['Mode'] ==2 && !empty($rowNum['COUNT(t.TeamID)'])){ 
									echo $rowNum['COUNT(t.TeamID)']; 
								}elseif (!empty($rowNum['COUNT(m.MemberID)'])){ 
									echo $rowNum['COUNT(m.MemberID)']; 
								}else{ echo "0"; 
								} ?></a></span> 人</div>
                        </div>
                            <?php if ($_SESSION["islogin"] == FALSE) require_once('tips/tips.html'); ?>	
                            <?php require_once('share_box.php'); ?>
                          <div id="event">
<?php
if ($_GET['Mode'] == 2){
	$resultTeamMem= mysql_query("
		SELECT TeamID, COUNT(TeamID) FROM tb_team_attendee GROUP BY TeamID ")or die(mysql_error());
	
	while ($row = mysql_fetch_assoc($result)){
		$rank++;
		while ($rowTeamMem = mysql_fetch_assoc($resultTeamMem)){
			list($Country_reg,$City_reg) = geoid_to_city( $row['GeoID']);
			if ($row['TeamID'] == $rowTeamMem['TeamID']){
?>
<div class="block">
	<table width="100%" cellspacing="0" cellpadding="0">
    	<tr>
        	<td width="60" align="center" valign="middle" class="word_type_bb18">
            	<?php if ($rank == 1){ echo "<div class=\"pic\"><img src=\"images/rank1.jpg\" width=\"50\" /></div>"; }else{ echo "#".$rank; } ?>
            </td>
            <td width="60" valign="top">
            	<div class="pic"><img border="0" src="<?php echo $row['TeamImg']; ?>" width="50" height="50" /></div>
			</td>
			<td>
				<div class="bar">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td valign="middle">
                                <div class="boxfloat word_type_g12">
                                	<a  class="word_type_bb24"><?php echo $row['SUM(r.Mileage)']; ?></a><br />累計里程</div>
                                <div class="boxfloat word_type_g12">
                                	<a  class="word_type_bb24"><?php echo $row['SUM(rt.Score)']; ?></a><br />團隊積分</div>
                            </td>
                            <td>
                                <span class="button">
                                	<a class="word_type_wb12" href="team_record.php?ID=<?php echo $row['TeamID']; ?>">戰績</a></span>
                            </td>
                        </tr>
                    </table>
				</div>
				<div style=" height: 16px; overflow: hidden;" class="text"><a href="team_career.php?ID=<?php echo $row['TeamID']; ?>" class="word_type_bb14"><?php echo $row['TeamName']; ?></a></div>
				<div class="text">效力車手 <?php echo $rowTeamMem['COUNT(TeamID)']; ?> 人</div>
				<div class="text word_type_g12"><?php echo $Country_reg; ?></div>
			</td>
		</tr>
	</table>
</div>
<?php
			}//if
		}//while2
		$rowTeamMem = mysql_data_seek($resultTeamMem, 0);
	}//while1
}else{//if
	$resultTeam= mysql_query("
		SELECT * FROM tb_team as t, tb_team_attendee as ta WHERE t.TeamID = ta.TeamID ")or die(mysql_error());
	
	while ($row = mysql_fetch_assoc($result)){
		$rank++;
		while ($rowTeam = mysql_fetch_assoc($resultTeam)){
			list($Country_reg,$City_reg) = geoid_to_city( $row['GeoID']);
			$Num = mysql_num_rows($resultTeam);
			$i++;
			if ($row['MemberID'] == $rowTeam['MemberID']){				
?>
<div class="block">
	<table width="100%" cellspacing="0" cellpadding="0">
    	<tr>
        	<td width="60" align="center" valign="middle" class="word_type_bb18">
            	<?php if ($rank == 1 && $_GET['Mode'] == 1){ echo "<div class=\"pic\"><img src=\"images/Y_Shirt.png\" width=\"50\" /></div>"; }else if ($rank == 1 && $_GET['Mode'] == 3){ echo "<div class=\"pic\"><img src=\"images/P_Shirt.png\" width=\"50\" /></div>"; }else{ echo "#".$rank; } ?>
            </td>
            <td width="120" valign="top">
            	<div class="pic"><img border="0" src="<?php echo $row['IDImg']; ?>" width="50" height="50" /></div>
                <div class="pic"><img border="0" src="<?php echo $row['BikeImg']; ?>" width="50" height="50"/></div>
			</td>
			<td>
				<div class="bar">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td valign="middle">
                                <div class="boxfloat word_type_g12">
                                	<a  class="word_type_bb24"><?php echo $row['SUM(r.Mileage)']; ?></a><br />累計里程</div>
                                <div class="boxfloat word_type_g12">
                                	<a  class="word_type_bb24"><?php echo $row['SUM(rm.Score)']; ?></a><br /><?php if ($_GET['Mode'] == 1){ echo "黃衫"; }else{ echo "粉紅衫"; } ?>積分</div>
                            </td>
                            <td>
                                <span class="button">
                                	<a class="word_type_wb12" href="memb_record.php?ID=<?php echo $row['MemberID']; ?>">戰績</a></span>
                            </td>
                        </tr>
                    </table>
				</div>
				<div style=" height: 16px; overflow: hidden;" class="text">
                	<a href="memb_career.php?ID=<?php echo $row['MemberID']; ?>" class="word_type_bb14"><?php echo $row['MemberName']; ?></a> / <a  class="word_type_bb14"><?php echo $row['BrandName']; ?> <?php echo $row['BikeModel']; ?></a></div>
				<div class="text"><?php echo $rowTeam['TeamName']; ?></div>
				<div class="text word_type_g12"><?php echo $Country_reg; ?></div>
			</td>
		</tr>
	</table>
</div>
<?php
				$i = 0;
				break;
			}else{
				if ($i == $Num){
?>
<div class="block">
	<table width="100%" cellspacing="0" cellpadding="0">
    	<tr>
        	<td width="60" align="center" valign="middle" class="word_type_bb18">
            	<?php if ($rank == 1 && $_GET['Mode'] == 1){ echo "<div class=\"pic\"><img src=\"images/Y_Shirt.png\" width=\"50\" /></div>"; }else if ($rank == 1 && $_GET['Mode'] == 3){ echo "<div class=\"pic\"><img src=\"images/P_Shirt.png\" width=\"50\" /></div>"; }else{ echo "#".$rank; } ?>
            </td>
            <td width="120" valign="top">
            	<div class="pic"><img border="0" src="<?php echo $row['IDImg']; ?>" width="50" height="50" /></div>
                <div class="pic"><img border="0" src="<?php echo $row['BikeImg']; ?>" width="50" height="50"/></div>
			</td>
			<td>
				<div class="bar">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td valign="middle">
                                <div class="boxfloat word_type_g12">
                                	<a  class="word_type_bb24"><?php echo $row['SUM(r.Mileage)']; ?></a><br />累計里程</div>
                                <div class="boxfloat word_type_g12">
                                	<a  class="word_type_bb24"><?php echo $row['SUM(rm.Score)']; ?></a><br /><?php if ($_GET['Mode'] == 1){ echo "黃衫"; }else{ echo "粉紅衫"; } ?>積分</div>
                            </td>
                            <td>
                                <span class="button">
                                	<a class="word_type_wb12" href="memb_record.php?ID=<?php echo $row['MemberID']; ?>">戰績</a></span>
                            </td>
                        </tr>
                    </table>
				</div>
				<div style=" height: 16px; overflow: hidden;" class="text">
                	<a href="memb_career.php?ID=<?php echo $row['MemberID']; ?>" class="word_type_bb14"><?php echo $row['MemberName']; ?></a> / <a  class="word_type_bb14"><?php echo $row['BrandName']; ?> <?php echo $row['BikeModel']; ?></a></div>
				<div class="text">自由車手</div>
				<div class="text word_type_g12"><?php echo $Country_reg; ?></div>
			</td>
		</tr>
	</table>
</div>
<?php
					$i = 0;
				}
			}//if
		}//while2
		$rowTeam = mysql_data_seek($resultTeam, 0);
	}//while1
}//if
?>
							  
                              
							
							
                          </div>
                    	</div>
                    </td>
                    <td valign="top">
                        <div id="side">
                          <div id="buttonblock">
                            <div class="button" <?php echo "style=\"visibility:hidden;\"" ?> ><a >賽事規則</a></div>
                          </div>
                          <div class="menu">
                          	<div class="box"><a href="rank.php?Mode=1" class="word_type_bb12">官方獎項</a>．企業贊助獎項</div>
                            <div class="block">
                              <a href="rank.php?Mode=1" class="word_type_bb18">黃衫排名</a><br /></div>
                            <div class="block">
                              <a href="rank.php?Mode=2" class="word_type_bb18">團隊排名</a><br />
                            </div>
                            <div class="block">
                        <a href="rank.php?Mode=3" class="word_type_bb18">粉紅衫排名</a><br />
                            </div>
                            <div class="block"> <a  class="word_type_bb18">白衫排名</a><br />
                            <span class="word_type_g12">近期開放, 拭目以待...</span></div>
                            <div class="block">
                        <a  class="word_type_bb18">綠衫排名</a><br />
                                    <span class="word_type_g12">近期開放, 拭目以待...</span>
                            </div>
                            <div class="block">
                              <a  class="word_type_bb18">紅點衫排名</a><br />
                                    <span class="word_type_g12">近期開放, 拭目以待...</span>
                            </div>
                            <div class="block">
                              <a  class="word_type_bb18">綠點衫排名</a><br />
                                    <span class="word_type_g12">近期開放, 拭目以待...</span>
                            </div>
                            <div class="block">
                             <a  class="word_type_bb18">黑衫排名</a><br />
                                    <span class="word_type_g12">近期開放, 拭目以待...</span>
                            </div>
                          </div>
<?php require_once('sideADS.php'); ?>
                      </div>
                	</td>
            	</tr>
			</table>
		</div>
		<?php require_once('include_footer.php'); ?>
	</div>

</body>
</html>
