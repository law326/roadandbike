<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<img src="http://www.roadandbike.com/images/fbimage.jpg" style="display:none" /><!--google+-->
<?php 
require_once('Connections/dbConn.php');
$TeamID = $_GET['ID']; 
$FBresult =  mysql_query("
	SELECT *
	FROM tb_team as t
	WHERE t.TeamID = '$TeamID' ")
	or die(mysql_error());
$FBtitle = mysql_fetch_assoc($FBresult);
$webTitle = $FBtitle['TeamName']." 的車隊生涯 - RoadandBike";//web & share(facebook, google+) title
?>
<title><?php echo $webTitle; ?></title><!--for web-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta property="og:image" content="http://www.roadandbike.com/images/fbimage.jpg" /><!--facebook-->
<meta property="og:title" content="<?php echo $webTitle; ?>" /><!--for facebook & google+-->
<meta property="og:type" content="website" />
<meta property="og:url" content="http://www.roadandbike.com" />
<meta property="og:description" content="加入R&B, 跟著我們一起征服您的城市, 成為街頭達人! 比賽不用人擠人, 在這裡天天有比賽, 處處是戰場,您還可以自訂規則, 在您拿手的路段跟各路高手一較高下!" />
<meta name="keywords" content="軌跡、分享、單車、賽事、免費">
<link href="css/page_style.css" rel="stylesheet" type="text/css" /> 
<link rel="shortcut icon" type="image/x-icon" href="images/icon.png">
<!--no JavaScript-->
</head><noscript>
<META HTTP-EQUIV="Refresh" CONTENT="0;URL=noJavaScript.html" />
</noscript>
<!--no JavaScript-->
<?php
/* -----bobo註解-----
 * module: jQuery-Validation-Engin、GeoIP、jQueryTip
 * note: 等資料表確認好 才能讀取其他資訊
 * map顯示5筆戰績完成位置
 * 計算車齡
*/

session_start();

require_once('Connections/autologin.php');//判斷訪客狀態(登入、未登入)，再決定您"目前所在的城市"資訊
require_once('Connections/find_LatLng.php');//由GeoID找出對應的經緯度 這裡給Gmap用

$editFormAction = $_SERVER['PHP_SELF']; //目前正在執行的檔案

$TeamID = $_GET['ID'];

$Access = $row["AccessLevel"];
$MemberIDRow = $row["MemberID"];
//車隊資訊
$resultTeam = mysql_query("
	SELECT t.TeamID, t.TeamName, t.TeamImg, t.GeoID, SUM(rt.Score), SUM(r.Mileage), t.TeamDate
	FROM tb_race_mcareer as rm, tb_team_attendee as ta, tb_race as r, tb_race_tcareer as rt, tb_team as t
	WHERE rm.MemberID = ta.MemberID AND ta.TeamID = t.TeamID AND ta.TeamID = '$TeamID' AND rm.Status = 1 AND 
		r.RaceID = rm.RaceID AND r.IsGroup = 1 AND ta.TeamID = rt.TeamID AND r.RaceID = rt.RaceID AND rt.Status = 1 ")
	or die(mysql_error());
	$TeamInfo = mysql_fetch_assoc($resultTeam);
	
list($Country_reg,$City_reg) = geoid_to_city( $TeamInfo['GeoID']); //將會員的GeoID轉換成City

/* 經緯度轉換 這裡給Gmap用*/
$latlng = geo_to_latlng($TeamInfo['GeoID']);

//里程計算
$resultSUM = mysql_query("
	SELECT SUM(rt.Score), SUM(r.Mileage)
	FROM tb_race_mcareer as rm, tb_team_attendee as ta, tb_race as r, tb_race_tcareer as rt
	WHERE rm.MemberID = ta.MemberID AND ta.TeamID = '$TeamID' AND rm.Status = 1 AND r.RaceID = rm.RaceID AND r.IsGroup = 1 AND
		ta.TeamID = rt.TeamID AND r.RaceID = rt.RaceID AND rt.Status = 1 ")
	or die(mysql_error());
	$SUM = mysql_fetch_assoc($resultSUM);

//完賽紀錄
$resultAboutRace = mysql_query("
	SELECT *
	FROM tb_race_tcareer as rt, tb_race as r 
	WHERE rt.TeamID = '$TeamID' AND rt.Status = 1 AND r.RaceID = rt.RaceID 
	ORDER BY JoinDate DESC ")
	or die(mysql_error());
	$numRace = mysql_num_rows($resultAboutRace);

/* 找出隊員*/
$result_follow = mysql_query("
	SELECT * 
	FROM tb_team_attendee as ta, tb_members as m
	WHERE ta.TeamID='$TeamID' AND ta.MemberID=m.MemberID 
	ORDER BY rand() ")or die(mysql_error());
	$num_follow = mysql_num_rows($result_follow);

/* 找出粉絲*/
$result_fan = mysql_query("
	SELECT * 
	FROM tb_follow_team as f, tb_members as m 
	WHERE f.BeFID='$TeamID' AND f.FID=m.MemberID 
	ORDER BY rand() ")
	or die(mysql_error());
	$num_fan = mysql_num_rows($result_fan);
	
	$RaceNumSeek = 5;//輪播數量
	
//地圖
$resultMark = mysql_query("
	SELECT *
	FROM tb_race_tcareer as rt, tb_race as r 
	WHERE rt.TeamID = '$TeamID' AND rt.Status = 1 AND r.RaceID = rt.RaceID 
	ORDER BY JoinDate DESC LIMIT $RaceNumSeek ")
	or die(mysql_error());
	
$resultMark2 = mysql_query("
	SELECT *
	FROM tb_race_tcareer as rt, tb_race as r 
	WHERE rt.TeamID = '$TeamID' AND rt.Status = 1 AND r.RaceID = rt.RaceID 
	ORDER BY JoinDate DESC LIMIT $RaceNumSeek ")
	or die(mysql_error());
	$MarkNum = mysql_num_rows($resultMark);
	
//地圖資訊
$resultRaceInfo2 = mysql_query("
	SELECT *
	FROM tb_race_tcareer as rt, tb_race as r 
	WHERE rt.TeamID = '$TeamID' AND rt.Status = 1 AND r.RaceID = rt.RaceID 
	ORDER BY JoinDate DESC LIMIT $RaceNumSeek ")
	or die(mysql_error());//計算資
?> 
<!--google map-->
<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.6/jquery-ui.min.js"></script>
<script type="text/javascript" charset="utf-8">

$(function(){        
	var burnsvilleMN = new google.maps.LatLng(<?php echo $latlng ;?>);
	// Creating a map
    var map = new google.maps.Map($('#map')[0], {
          zoom: 10,
          center: burnsvilleMN,
          disableDefaultUI: true,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        });	
		       
    var boundsChange = google.maps.event.addListener(map, "bounds_changed", function() {
				//地圖置中
		
  		<?php
while ($race_latlon = mysql_fetch_assoc($resultMark))	{
		$m++;
		$lats=$race_latlon['StartLat'];
		$lons=$race_latlon['StartLon'];
		
		$non1=$non1.$lats.",";
		$non2=$non2.$lons.",";
		if ($m == $RaceNumEach) break;
	}
	echo " var lats=[".$non1."]; var lons=[".$non2."]; var size=".$RaceNumSeek."; ";
?>
if (size > 0){
var latsmin = lats[0], latsmax = lats[0];
	for (var i = 1; i < size; i++){
		if (latsmax < lats[i]){
			latsmax = lats[i];
		}else if(latsmin > lats[i]){
			latsmin = lats[i];
		}
	}
	var lonsmin = lons[0], lonsmax = lons[0];
	for (var i = 1; i < size; i++){
		if (lonsmax < lons[i]){
			lonsmax = lons[i];
		}else if(lonsmin > lons[i]){
			lonsmin = lons[i];
		}
	}

	var southWest = new google.maps.LatLng(latsmin,lonsmin);
  	var northEast = new google.maps.LatLng(latsmax,lonsmax);
  	var bounds = new google.maps.LatLngBounds(southWest,northEast);
  	map.fitBounds(bounds);
}//地圖置中結束
  		var markers = [];
  		for (var i = 0; i < <?php echo $RaceNumSeek;?>; i++) {
			var latLng = [
				<?php while ($rowInfo_latlon2 = mysql_fetch_assoc($resultMark2)) {  ?> 
					new google.maps.LatLng(<?php echo $rowInfo_latlon2['StartLat']; ?>,<?php echo $rowInfo_latlon2['StartLon']; ?>),
				<?php }//while End ?> 
			];
  				
			var image = 'images/cycling.png';	
			var marker = new google.maps.Marker({
				position: latLng[i],
				map: map,
				icon: image
			});
				  
			markers[i] = marker;

		}
	
		google.maps.event.removeListener(boundsChange);
					
		$(markers).each(function(i,marker){
			$("<li />").addClass('ui-state-default ui-corner-all')
			.html(+i)//.html("Point "+i)
			.click(function(){
				message.open(marker, i);
			})
			.appendTo("#list" + i);
	
			google.maps.event.addListener(marker, "mouseover", function(){
				message.open(this, i);
			});
		});
  	});
	function MessageWindow(){
  		this.setMap(map);
	}
	MessageWindow.prototype = new google.maps.OverlayView();
	MessageWindow.prototype.onAdd = function() {
		this.$div_ = $('<div id="message" />').appendTo(this.getPanes().floatPane);//.getPanes()傳回能繪製此 OverlayView 的窗格
	}
	MessageWindow.prototype.draw = function() {
		var me = this;
    	var $div = this.$div_;
    	var point = this.getProjection().fromLatLngToDivPixel(this.latlng_);
    	if (point) {
        	$div.css({ left: point.x, top: point.y});//框的位置(x-100/y-120)
		}
	};
	var openTimeout;
	MessageWindow.prototype.open = function(marker, index) {

    	clearTimeout(openTimeout);
		$div = this.$div_.stop().css('opacity',1).hide().empty();
		this.latlng_ = marker.position;
    	this.draw();
        
			
		
		var msgbox_i = "#tabs-template" + index ;
		var closeButton = $(msgbox_i).clone().show().appendTo($div);
		//alert(index);

		var left = map.getBounds().getSouthWest().lat();
		var right = map.getBounds().getNorthEast().lat();
		var offset = (right - left) * 0.25;//視窗的位置
		var newCenter = new google.maps.LatLng(marker.position.lat(), marker.position.lng()+offset);//控制點的座標
		map.panTo(newCenter);
		openTimeout = setTimeout(function(){
			$div.show("drop", { direction:"right" });//滑入方向
		}, 50);//速度
	};
	var message = new MessageWindow();
	
});

function iconHTML(type) {
	switch (type) {
		case "close"	: iconClass = 'ui-icon-closethick'; break;
	}	
}
</script>
<style type="text/css">
	#message{ position:relative; padding: 5px; }
	.mes{ background-color: #fff; width: 192px; padding: 5px; }
	.map_pic{ float: left; margin-right: 5px; height: 50px; width: 50px; }
	.map_text{ line-height: 16px; margin-bottom: 2px; }
	.map_bar{ float: right;	width: 200px; }	
	.map_pics{ float: left; margin-right: 2px; width: 30px; height: 30px; }
</style>
<!--google map-->

<!--關注按鈕-->
<link href="follow_unfollow/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.livequery.js"></script>
<script type="text/javascript"> 
   $(document).ready(function() { 
   
			$('.buttons2 > a').livequery("click",function(e){
				
				var parent  = $(this).parent();
				var getID   =  parent.attr('id').replace('button_','');//取得被click的車手or車隊id  格式長作這樣 button_id
				
				$.post("follow_unfollow/follow_team.php?id="+getID, {
	
				}, function(response){
					
					$('#button_'+getID).html($(response).fadeIn('slow'));
				});
		});	
	});
</script>
<!--關注按鈕-->
<!--所在地區-->
<script src="CollapsiblePanel/CollapsiblePanel.js" type="text/javascript"></script>
<!--所在地區-->
</head>


<body>
<?php require_once('include_header.php'); ?>	    
		<div id="container">
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
            	<tr>
                	<td valign="top">
                    	<div id="main">
                            <div id="title">
                           	  <div class="button"><a href="team_career.php?ID=<?php echo $TeamID; ?>" class="word_type_bb14">動態</a>．<a href="team_record.php?ID=<?php echo $TeamID; ?>" class="word_type_bb14">戰績</a>．<a href="team_friend.php?ID=<?php echo $TeamID; ?>" class="word_type_bb14">車友</a></div>
                            <div class="text word_type_bb24"><?php echo $TeamInfo['TeamName'] ;?></div>
                                <div class="box">國籍 <a ><?php echo $Country_reg ;?></a>．活耀賽區 <a ><?php echo $City_reg ;?></a>．隊齡 <a >
<?php
$RideAge = $TeamInfo['TeamDate'];
echo (date("Y") - $RideAge);
?>
                                </a>年</div>
                        </div>
<?php //未登入提示趕快加入RoadandBike
if ($_SESSION['islogin'] != TRUE) require_once('tips/tips.html'); ?>	
                            <div id="intro">
                            	<div id="map"></div>
	<!--地圖的訊息欄位-->                  
<?php
	$msg_i=0;
	while ($rowInfo_msg = mysql_fetch_assoc($resultRaceInfo2)) {  
	$n++;
	$EndYMD = explode(":",$rowInfo_msg['LapTime']);
?> 
	<div id="tabs-template<?php echo $msg_i; ?>" style="display:none">
<?php $msg_i = $msg_i + 1; ?>
		<div class="back_sb"></div>
		<div class="map_block mes">
			<table width="100%" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<div class="map_text" style=" height: 16px; overflow: hidden; width:190px;">
                        	<a href="race.php?ID=<?php echo $rowInfo_msg['RaceID']; ?>" id="RaceMsg" class="word_type_bb14"><?php echo $rowInfo_msg['Title']; ?></a>
						</div>
						<div class="button"></div>
						<div class="map_text word_type_g12">完成時間 <?php echo $EndYMD[0]; ?>h<?php echo $EndYMD[1]; ?>m<?php echo $EndYMD[2]; ?>s</div>
					</td>
				</tr>
			</table>
		</div>
	</div>
<?php 
		if ($n == $RaceNumSeek) break;
	}  
	
?> 
                            </div>
                            <?php require_once('share_box.php'); ?>
                            <div id="event" >
<?php

$result = mysql_query("
	SELECT * FROM tb_event as e, tb_members as m, tb_team as t
	WHERE e.TeamID = '$TeamID' AND e.MemberID = m.MemberID AND e.TeamID = t.TeamID
	ORDER BY e.EventDate DESC LIMIT 20 ") 
	or die(mysql_error());
	
$resultRace = mysql_query("
	SELECT * FROM tb_event as e, tb_race as r, tb_team as t, tb_race_tcareer as rt
	WHERE e.TeamID = '$TeamID' AND r.RaceID = rt.RaceID AND e.TeamID = t.TeamID AND e.TeamCareerID = rt.TeamCareerID ") 
	or die(mysql_error());
	
while ($row = mysql_fetch_assoc($result)){

switch ($row['EventType'])
	{
	case 21://team1(車隊成立後)
?>
<div class="block">
	<table width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td width="120" valign="top">
            	<div class="pic"><img border="0" src="<?php echo $row['IDImg']; ?>" width="50" height="50" /></div>
				<div class="pic"><img border="0" src="<?php echo $row['TeamImg']; ?>" width="50" height="50" /></div>
			</td>
			<td>
            	<div class="text"><a href="memb_career.php?ID=<?php echo $row['MemberID']; ?>" class="word_type_bb14"><?php echo $row['MemberName']; ?></a></div>
				<div class="text">成立了 <a href="team_career.php?ID=<?php echo $row['TeamID']; ?>"><?php echo $row['TeamName']; ?> </a></div>
				<div class="text word_type_g12"><?php echo $row['EventDate']; ?></div>
			</td>
		</tr>
	</table>
</div>
<?php
		break;
	case 29://tpoll3(加入車隊後)
?>
<div class="block">
	<table width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td width="120" valign="top">
            	<div class="pic"><img border="0" src="<?php echo $row['IDImg']; ?>" width="50" height="50" /></div>
				<div class="pic"><img border="0" src="<?php echo $row['TeamImg']; ?>" width="50" height="50" /></div>
			</td>
			<td>
            	<div class="text"><a href="memb_career.php?ID=<?php echo $row['MemberID']; ?>" class="word_type_bb14"><?php echo $row['MemberName']; ?></a></div>
				<div class="text">正式效力 <a href="team_career.php?ID=<?php echo $row['TeamID']; ?>"><?php echo $row['TeamName']; ?> </a></div>
				<div class="text word_type_g12"><?php echo $row['EventDate']; ?></div>
			</td>
		</tr>
	</table>
</div>
<?php	
		break;
	case 12://race3(團隊賽事建立後)
		while ($row2 = mysql_fetch_assoc($resultRace)){
			if ($row['EventID'] == $row2['EventID']){
?>
<div class="block">
	<table width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td width="60" valign="top">
				<div class="pic"><img border="0" src="<?php echo $row['TeamImg']; ?>" width="50" height="50" /></div>
			</td>
			<td>
            	<div class="text"><a href="team_career.php?ID=<?php echo $row['TeamID']; ?>" class="word_type_bb14"><?php echo $row['TeamName']; ?></a></div>
				<div class="text">舉辦了 <a href="race.php?ID=<?php echo $row2['RaceID']; ?>"><?php echo $row2['Title']; ?></a></div>
				<div class="text word_type_g12"><?php echo $row['EventDate']; ?></div>
			</td>
		</tr>
	</table>
</div>
<?php	
			}
		}
		$row2 = mysql_data_seek($resultRace, 0);
		break;
	case 31://tcareer1(非舉辦車隊第一人完成團隊賽事後)
		while ($row2 = mysql_fetch_assoc($resultRace)){
			if ($row['EventID'] == $row2['EventID']){
?>
<div class="block">
	<table width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td width="60" valign="top">
				<div class="pic"><img border="0" src="<?php echo $row['TeamImg']; ?>" width="50" height="50" /></div>
			</td>
			<td>
            	<div class="text"><a href="team_career.php?ID=<?php echo $row['TeamID']; ?>" class="word_type_bb14"><?php echo $row['TeamName']; ?></a></div>
				<div class="text">報名了 <a href="race.php?ID=<?php echo $row2['RaceID']; ?>"><?php echo $row2['Title']; ?> </a></div>
				<div class="text word_type_g12"><?php echo $row['EventDate']; ?></div>
			</td>
		</tr>
	</table>
</div>
<?php	
			}
		}
		$row2 = mysql_data_seek($resultRace, 0);
		break;
case 32://tcareer2(非舉辦車隊第一人完成團隊賽事後)
		while ($row2 = mysql_fetch_assoc($resultRace)){
			if ($row['EventID'] == $row2['EventID']){
?>
<div class="block">
	<table width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td width="120" valign="top">
            	<div class="pic"><img border="0" src="<?php echo $row['IDImg']; ?>" width="50" height="50" /></div>
				<div class="pic"><img border="0" src="<?php echo $row['TeamImg']; ?>" width="50" height="50" /></div>
			</td>
			<td>
            	<div class="text"><a href="memb_career.php?ID=<?php echo $row['MemberID']; ?>" class="word_type_bb14"><?php echo $row['MemberName']; ?></a> / <a href="team_career.php?ID=<?php echo $row['TeamID']; ?>" class="word_type_bb14"><?php echo $row['TeamName']; ?></a></div>
				<div class="text">以 <?php echo $row2['LapTime']; ?> 的成績完成 <a href="race.php?ID=<?php echo $row2['RaceID']; ?>"><?php echo $row2['Title']; ?> </a></div>
				<div class="text word_type_g12"><?php echo $row['EventDate']; ?></div>
			</td>
		</tr>
	</table>
</div>
<?php	
			}
		}
		$row2 = mysql_data_seek($resultRace, 0);
		break;
	case 33://tcareer3(團隊賽事完成後)
		while ($row2 = mysql_fetch_assoc($resultRace)){
			if ($row['EventID'] == $row2['EventID']){
?>
<div class="block">
	<table width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td width="60" valign="top">
				<div class="pic"><img border="0" src="<?php echo $row['TeamImg']; ?>" width="50" height="50" /></div>
			</td>
			<td>
            	<div class="text"><a href="team_career.php?ID=<?php echo $row['TeamID']; ?>" class="word_type_bb14"><?php echo $row['TeamName']; ?></a></div>
				<div class="text">以 <?php echo $row2['LapTime']; ?> 的成績完成 <a href="race.php?ID=<?php echo $row2['RaceID']; ?>"><?php echo $row2['Title']; ?> </a>暫居單站#12</div>
				<div class="text word_type_g12"><?php echo $row['EventDate']; ?></div>
			</td>
		</tr>
	</table>
</div>
<?php	
			}
		}
		$row2 = mysql_data_seek($resultRace, 0);
		break;
	case 34://tcareer4(團隊賽結束成後)
		while ($row2 = mysql_fetch_assoc($resultRace)){
			if ($row['EventID'] == $row2['EventID']){
?>
<div class="block">
	<table width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td width="60" valign="top">
				<div class="pic"><img border="0" src="<?php echo $row['TeamImg']; ?>" width="50" height="50" /></div>
			</td>
			<td>
            	<div class="text"><a href="team_career.php?ID=<?php echo $row['TeamID']; ?>" class="word_type_bb14"><?php echo $row['TeamName']; ?></a></div>
				<div class="text">最終以 <?php echo $row2['LapTime']; ?> 的成績拿下 <a href="race.php?ID=<?php echo $row2['RaceID']; ?>"><?php echo $row2['Title']; ?> </a>的單站冠軍</div>
				<div class="text word_type_g12"><?php echo $row['EventDate']; ?></div>
			</td>
		</tr>
	</table>
</div>
<?php
			}
		}
		$row2 = mysql_data_seek($resultRace, 0);
		break;
	default:
?>
<div class="block">
	<table width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td width="100%" valign="top">
<?php
		echo "EventType".$row['EventType'];
?>
			</td>
		</tr>
	</table>
</div>
<?php
	}
}
?> 
                            </div>
                        </div>
                    </td>
                    <td valign="top">


<?php require_once('include_teamcareer_rightSide.php'); ?>	
                	</td>
            	</tr>
			</table>
		</div>
<?php require_once('include_footer.php'); ?>	 
</body>
</html>
