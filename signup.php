<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php
$page = "signup";
require_once('include_webtitle.php');//標題檔
$pagestyle = "setting";//提供header樣式判斷
$_SERVER['REQUEST_URI'] = "";//讓從這登陸的人導向個人頁面
?>
<?php
/* -----bobo註解-----
 * module:jQuery-Validation-Engin、GeoIP、jQueryTip、PHP表單驗證檔
 * note:	
 *
 *
*/

require_once('Connections/dbConn.php'); 
require_once('Connections/find_GeoID.php');//function GeoID相關

$editFormAction = $_SERVER['PHP_SELF']; //目前正在執行的檔案名

//用IP判斷國家城市
require_once('Geoip/Geoip_include.php');
$geo_country = $record->country_name;
$geo_city = $record->city;
geoip_close($gi);

if ((isset($_POST["From_insert"])) && ($_POST["From_insert"] == "form1") ) {
	
	require_once "Connections/PHP_formvalidator.php";//載入PHP表單驗證檔
    /*做這些PHP驗證*/
    $validator = new FormValidator();
    $validator->addValidation("memName","req","請輸入memName");
	
    $validator->addValidation("email","req","請輸入email");
    $validator->addValidation("email","email","email格式不正確");
	
    $validator->addValidation("password","req","請輸入password");
    $validator->addValidation("password","eqelmnt=password2","密碼不相符");
    $validator->addValidation("password","alnum","只能輸入英文字母、數字");
	
    $validator->addValidation("birthday_year","num","請輸入birthday_year");	
	$validator->addValidation("birthday_month","num","請輸入birthday_month");
	$validator->addValidation("birthday_day","num","請輸入birthday_day");
		
	$validator->addValidation("City","req","請輸入City");	
	$validator->addValidation("City","alnum_s","只能輸入英文字母、數字(准許空白)");	
	
	
	if(!$validator->ValidateForm())	{
	//若驗證失敗進入此區
	 	echo "<B>驗證錯誤:</B>";

        $error_hash = $validator->GetErrors();
        foreach($error_hash as $inpname => $inp_err)
        {
            echo "<p>$inpname : $inp_err</p>\n";
        }        
		exit;
	}

	/*表單驗證合格才會執行下面*/
	$email=$_POST['email'];
	
	$sql_checkMail="SELECT * FROM tb_members WHERE Email ='$email'"; 
	$result1=mysql_query($sql_checkMail)or die(mysql_error()); 
	if(!$result1) die('you had been our member!');
	
	$count=mysql_num_rows($result1); 
	if($count==0){ //確認mail沒有註冊過
		
			$Confirm_Code=md5(uniqid(rand()));//啟用確認碼 
			
			list($GeoID) = city_to_geoid( $_POST["City"] );//user輸入城市轉換成GeoID
			if (empty($GeoID)) die('Please check your city ,GeoID not exist!');
		
			  $insertSQL = sprintf("INSERT INTO  tb_members_temp (Email,MemberPWD, MemberName, Gender, Birthday,GeoID,Confirm_Code) VALUES ( '%s', '%s', '%s', '%s', '%s', '%s', '%s')",
								   $_POST['email'],
								   $_POST['password'],
								   $_POST['memName'],
								   $_POST['Gender'],
								   $_POST['birthday_year']."/".$_POST['birthday_month']."/".$_POST['birthday_day'] ,
								   $GeoID,
								   $Confirm_Code );
								  // 拿掉RideAge  $_POST['ride_year']."/".$_POST['ride_month']."/".$_POST['ride_day'],	
			
			   mysql_query($insertSQL, $dbConn) or die(mysql_error());
			
			  $insertGoTo = "signup_ok.php?memEmail=" . $_POST['email'] ."&memName=" .$_POST['memName'] . "&Confirm_Code=" .$Confirm_Code;
			  if (isset($_SERVER['QUERY_STRING'])) {
				$insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
				$insertGoTo .= $_SERVER['QUERY_STRING'];
			  }
			  header(sprintf("Location: %s", $insertGoTo));
	}//確認mail沒有註冊過End
	
	
}
?>
<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/redmond/jquery-ui.css">	
<link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css"/>
<link href="css/tooltip.css" rel="stylesheet" type="text/css" />

<!--lightbox-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<!--lightbox-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
<script src="http://cdn.jquerytools.org/1.2.5/full/jquery.tools.min.js"></script> 
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.11/jquery-ui.min.js"></script>

<script src="js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
<!--geo-->
<script>
$(document).ready(function(){

	$("#Country").focus(function() { //國家欄位找值--------------------
		$("#Country").autocomplete({
			source: "signup_CountryCity.php?kind=Country", //判斷國家 資料處理檔kind=Country
			minLength: 0,
		});
	});


	$("#City").focus(function() {
		// 會依據國家欄位的值 去判斷 有哪些city
		var Country_city = "signup_CountryCity.php?kind=City&City_Country=" + document.getElementById("Country").value;
			$("#City").autocomplete({
			source: Country_city ,
			minLength: 0,
			select: function(event, ui) {
				$('#id_GeoID').val(ui.item.id); //將GeoID值存到hidden 方便傳送
			}
		});
	});							//國家城市欄位找值--------------------
	
	$('#memName').click(function (){ //將網頁載入時預設在#City的值利用AJAX轉換成GeoID
		   $.ajax({
		   url: 'signup_CountryCity.php',
		   cache: false,
		   dataType: 'text', //dataType 預計會從url回傳的dataType  類型有4種：html,xml,json,text
			   type:'GET',
		   data: { kind: 'City' ,City_Country: $('#Country').val() ,term: $('#City').val()},//帶過去url的變數名稱
		   error: function(xhr) {
			 alert('Ajax request 發生錯誤');
		   },
		   success: function(response) {// request succeeds的回傳值
		   var jsonContent = eval("(" + response + ")");//將JSON文字轉成物件
			 $('#id_GeoID').val(jsonContent[0].id);
		   }
		   
	   });
	});
		/* 錯誤顯示位置在 jquery.validationEngine.js promptPosition: "centerLeft"  
	   錯誤訊息框的寬度設定在validationEngine.jquery.css	*/
	jQuery("#form1").validationEngine(); //不可放在國家欄位找值之前 會失效
});
</script>

<script language="javascript">
$(document).ready(function()//判斷email帳號是否存在
{
 
	$("#email").blur(function()
	{
		if(document.getElementById("email").value.length > 6){//if email字元大於6再做AJAX確認帳號
			//remove all the class add the messagebox classes and start fading
			$("#msgbox").removeClass().addClass('messagebox').text('Checking...').fadeIn("slow");
			//check 帳號是否存在
			$.post("signup_chkID.php",{ user_name:$(this).val() } ,function(data)
			{
			  if(data=='no') //if 帳號已被使用
			  {
					$("#msgbox").fadeTo(200,0.1,function() //fading訊息效果
					{ 
					  //加入訊息 and change the class of the box and start fading
					  document.getElementById("idErrMsg").innerHTML = "<img src='images/block1.gif' /><font color='red'> 此帳號已被使用!!</font>";
					  $('input[type="submit"]').attr('disabled','disabled');//submit不讓他送出
					});		
			  }
			  else
			  {
				$("#msgbox").fadeTo(200,0.1,function() //fading訊息效果
				{ 
					  //加入訊息 and change the class of the box and start fading
					  document.getElementById("idErrMsg").innerHTML = "<img src='images/apply2.gif' /><font color='green'> 恭喜,此帳號可註冊使用!</font>";
						 $('input[type="text"]').blur(function(){//把submit的disabled拿掉
									if($(this).val() != ''){
									   $('input[type="submit"]').removeAttr('disabled');
									}
						 });
				});
			  }
       		 });
		}//if email字元大於6
	});
});
</script>

</head>

<body>
<div id="signup">
<?php require_once('include_header.php'); ?>
                    	<div id="container">
                        <div id="title">
                        	<div class="text word_type_bb24">註冊為車手</div>
                        </div>
                        <form id="form1" class="formular" name="form1" method="POST" action="<?php echo $editFormAction; ?>">
						<div id="settings">
                        	<div class="box"><span class="word_type_bb12">下列欄位將影響您的參賽權, 請仔細完成</span></div>
                   	      <div class="block">
                       	      <table width="100%" cellspacing="0" cellpadding="0">
                       	        <tr>
                       	          <td valign="top"><div class="topic">Facebook</div></td>
                       	          <td width="100%"><div class="text"> <img border="0" src="images/fb-login-button.png" width="194" height="25" /><br />
                     	            </div>
                       	            <div class="text word_type_g12">連結您的Facebook帳號, 與朋友分享您的車手生涯</div></td>
                   	            </tr>
                   	        </table>
                   	      </div>
                   	      <div class="block">
                        	  <table width="100%" cellspacing="0" cellpadding="0">
                        	    <tr>
                        	      <td valign="top"><div class="topic">姓名</div></td>
                        	      <td width="100%"><div class="text">
                        	        <input name="memName" type="text" size="25" maxlength="30" id="memName" title="請輸入真實姓名或暱稱." class="validate[required] text-input" />
                        	      </div></td>
                      	      </tr>
                      	    </table>
                      	  </div>
                       	  <div class="block">
                       	    <table width="100%" cellspacing="0" cellpadding="0">
                       	      <tr>
                       	        <td valign="top"><div class="topic">電子信箱</div></td>
                       	        <td width="100%"><div class="text">
                       	          <input name="email" type="text" id="email" title="請輸入您的e-mail，以作為登入帳號使用!" size="25" class="validate[required,custom[email]] text-input" />
                     	          </div> <span id="idErrMsg"> </span><span id="msgbox" style="display:none"></span></td>
                   	          </tr>
                   	        </table>
                   	      </div>
                       	  <div class="block">
                        	  <table width="100%" cellspacing="0" cellpadding="0">
                        	    <tr>
                        	      <td valign="top"><div class="topic">密碼</div></td>
                        	      <td width="100%"><div class="text">
                        	        <input name="password" type="password" id="password" title="請輸入至少6字元,密碼請避免和帳號相似~" size="25" maxlength="30" class="validate[required,minSize[6]]  text-input"  />
                      	        </div></td>
                      	      </tr>
                      	    </table>
                      	  </div>
                       	  <div class="block">
                        	  <table width="100%" cellspacing="0" cellpadding="0">
                        	    <tr>
                        	      <td valign="top"><div class="topic">確認密碼</div></td>
                        	      <td width="100%"><div class="text">
                        	        <input name="password2" type="password" id="password2" title="請再次輸入密碼." size="25" maxlength="30"  class="validate[required,equals[password]] text-input" />
                      	        </div></td>
                      	      </tr>
                      	    </table>
                      	  </div>
                        	<div class="block">
                        	  <table width="100%" cellspacing="0" cellpadding="0">
                        	    <tr>
                        	      <td valign="top"><div class="topic">性別</div></td>
                        	      <td width="100%"><div class="text">
                        	        <select name="Gender" id="Gender">
                        	          <option value="1">男</option>
                        	          <option value="0">女</option>
                      	          </select>
                      	        </div></td>
                      	      </tr>
                      	    </table>
                      	  </div>
                        	<div class="block">
                        	  <table width="100%" cellspacing="0" cellpadding="0">
                        	    <tr>
                        	      <td valign="top"><div class="topic">生日</div></td>
                        	      <td width="100%"><div class="text">
               	            <select name="birthday_year" id="birthday_year"  autocomplete="off" >
                        	         
 									<?php 
									 for ($i=1920; $i<=date("Y");$i++){
										echo "<option  value=\"$i\" $selected >$i</option>";
									}?>
                      	          </select>
                            <select id="birthday_month" name="birthday_month" >
                                      
                                      <?php 
									 for ($i=1; $i<=12;$i++){
										echo "<option  value=\"$i\" $selected >$i</option>";
									}?>
                                    </select>
                                    	
                            <select name="birthday_day" id="birthday_day"  autocomplete="off" >
                                      
                                    <?php 
									 for ($i=1; $i<=31;$i++){
										echo "<option  value=\"$i\" $selected >$i</option>";
									}?>   
                           </select>
                        	      </div></td>
                      	      </tr>
                      	    </table>
                      	  </div>
                       	  <div class="block">
                        	  <table width="100%" cellspacing="0" cellpadding="0">
                        	    <tr>
                        	      <td valign="top"><div class="topic">國籍</div></td>
                        	      <td width="100%"><div class="text">
                        	        <input name="Country" type="text" id="Country" title="請選擇您所在位置的國籍" value="<?php echo $geo_country;?>" class="validate[required,minSize[3],custom[onlyLetterSp]] text-input"/>
                      	        </div>
                        	        <div class="text word_type_g12">代表您的國家, 並為她奮戰</div></td>
                      	      </tr>
                      	    </table>
                      	  </div>
                        	<div class="block">
                        	  <table width="100%" cellspacing="0" cellpadding="0">
                        	    <tr>
                        	      <td valign="top"><div class="topic">現居都市</div></td>
                        	      <td width="100%"><div class="text">
                        	        <input name="City" type="text" id="City" title="您所在的城市(或欄位空白時按↓自動選擇)" value="<?php echo $geo_city;?>" class="validate[required,minSize[2],custom[onlyLetterSp]] text-input" />
                      	        </div>
                        	        <div class="text word_type_g12">您的居住城市將會影響系統推薦給您的賽事資訊</div></td>
                      	      </tr>
                      	    </table>
                      	  </div>
                       	  <div class="box_bottom">
                            <input class="word_type_wb12" type="submit" name="submit" id="submit" value="送出" />
                            <input type="hidden" name="From_insert" value="form1" />
                            
                            <!--隱藏欄位 傳值 id_GeoID-->
							<input type="hidden" id="id_GeoID" name="id_GeoID"  />  
                            按下送出即表示您同意 RoadandBike <a href="test.html" target="_blank">隱私政策</a>及<a href="test.html" target="_blank">使用條款</div>
                        </div>
                    </form>
                    </div>
    </div>
</div>
<script type="text/javascript" charset="utf-8">
$('.back_sb').css({'filter' : 'alpha(opacity=40)'});
</script>
<!--tooltip-->
<script>
// select all desired input fields and attach tooltips to them
$("#form1 :text" ).tooltip({
	// place tooltip on the right edge
	position: "center right",
	// a little tweaking of the position
	offset: [-2, 10],
	// use the built-in fadeIn/fadeOut effect
	effect: "fade",
	// custom opacity setting
	opacity: 0.7
});
$("#form1 :password" ).tooltip({
	position: "center right",
	offset: [-2, 10],
	effect: "fade",
	opacity: 0.7
});
</script>
<!--tooltip-->
</body>
</html>
