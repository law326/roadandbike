<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<style type="text/css">
  html { height: 100% }
  body { height: 100%; margin: 0px; padding: 0px }
  #map_canvas { height: 100% }
</style>
<title>GPS Tracks Compare</title>

<?php
//echo "軌跡比對 <br>";

$slat=25.0254570;
$slon=121.6242190;

$elat=25.0164950;
$elon=121.5887670;

$dom1=new DOMDocument();
$dom2=new DOMDocument();

//if(($dom1->load("A-B1.gpx"))&($dom2->load("A-B2.gpx")))//後山2 728:648 0fail
//if(($dom1->load("A-B1.gpx"))&($dom2->load("A-B3.gpx")))//後山3 728:792 0fail
//if(($dom1->load("A-B1.gpx"))&($dom2->load("A-B4.gpx")))//後山4 728:672 0fail
//if(($dom1->load("A-B1.gpx"))&($dom2->load("A-B5.gpx")))//後山5 728:719 0fail
//if(($dom1->load("A-B-D.gpx"))&($dom2->load("A-C-D.gpx")))//五指山 1499:1324 1fail
//if(($dom1->load("A-B-D.gpx"))&($dom2->load("A-C-D2.gpx")))//五指山 1499:1377 1000pass
//if(($dom1->load("A-B-D.gpx"))&($dom2->load("20110424.gpx")))//五指山 1499:1364 0fail
if(($dom1->load("A-B(GPS).gpx"))&($dom2->load("A-B(PHONE).gpx")))//平溪 6472:1974 25fail
//if(($dom1->load("test1.gpx"))&($dom2->load("testu.gpx")))//矽碇 339:893 0fail
//if(($dom1->load("test2.gpx"))&($dom2->load("testu.gpx")))//舊庄 319:893 0fail
//if(($dom1->load("20110402c.gpx"))&($dom2->load("20110402u.gpx")))//風櫃嘴 1013:3508 1fail
//if(($dom1->load("20110402c.gpx"))&($dom2->load("20091017.gpx")))//風櫃嘴 1013:1987 1000pass
//if(($dom1->load("wulaic.gpx"))&($dom2->load("wulaiu.gpx")))//烏來 498:1856 0fail
//if(($dom1->load("wulaic.gpx"))&($dom2->load("bikemap.gpx")))//烏來 
{
	//echo "成功讀取軌跡 <p>";	
	
	$root1=$dom1->documentElement;
	$root2=$dom2->documentElement;
	
	
	$nodes1 = $root1->getElementsByTagName("trkpt");
	$nodes2 = $root2->getElementsByTagName("trkpt");


	/*時間
	$count=0;
	foreach( $nodes1 as $rt ) {
		$ele = $rt->getElementsByTagName( "ele" );
		$nValue_ele = $ele->item(0)->nodeValue;
		
		$time = $rt->getElementsByTagName( "time" );
		$nValue_time = $time->item(0)->nodeValue;
		
		echo "dom1第 $count 點 ele= $nValue_ele  【 $nValue_time 】 <br>";
		$count =1+$count;
	}
	exit;*/
	
	
	$node1_length=$nodes1->length;
	$node2_length=$nodes2->length;
	
	
	//軌跡數量
	echo "賽事軌跡共".$node1_length."點<br>";
	echo "參賽軌跡共".$node2_length."點<br>";
	
	
	$record1_length=$nodes1->length-1;
	$record2_length=$nodes2->length-1;
	
	
	//抓出起始位置
	for($s=0;$s<=$record1_length;$s++)
	{
		
		//echo "算里程";
		$points=$nodes1->item($s);
		
		$lats=$points->getAttribute("lat");
		$lons=$points->getAttribute("lon");
		
		$non=$non."new google.maps.LatLng(".$lats.",".$lons."),";
		
		if(($lats==$slat)&($lons==$slon))
		{
			echo "抓到起始座標".$s."<br>";
			break;
		}
	}
	
	//抓出終點位置
	for($e=$record1_length;$e>=0;$e--)
	{
		
		//echo "算里程";
		$pointe=$nodes1->item($e);
		
		$late=$pointe->getAttribute("lat");
		$lone=$pointe->getAttribute("lon");
		
		if(($late==$elat)&($lone==$elon))
		{
			echo "抓到終止座標".$e."<br>";
			break;
		}
	}
	$record1_length=$e;
	
	//算里程
	for($w=$s;$w<=$record1_length;$w++)
	{
		
		//echo "算里程";
		$pointw1=$nodes1->item($w);
		$pointw2=$nodes1->item($w+1);
		
		$latw1=$pointw1->getAttribute("lat");
		$lonw1=$pointw1->getAttribute("lon");
		//$ele=$pointw1->getElementByid("ele");
		
		//echo $ele;
		
		$latw2=$pointw2->getAttribute("lat");
		$lonw2=$pointw2->getAttribute("lon");
		
		//ΔlatinM=Δlat×111199.233米/度
		$latw_length=abs($latw1-$latw2)*111199.233;
		//ΔloninM=Δlon×111199.233米/度×cos(lat)
		$lonw_length=abs($lonw1-$lonw2)*111199.233*cos(abs($latw1-$latw2)/2);
		
		$race_length=sqrt(pow($latw_length,2)+pow($lonw_length,2));
		
		$race_all=$race_all+$race_length;
		
		
		if($w==$record1_length-1)
		{
			$race_all=$race_all*0.94483883/1000;
			echo "賽事總里程".$race_all."km<br>";
			break;
		}
	}
	
	$km=100;//intval($race_all)*5;
	
	
	$fitj=0;
	$fit=0;
	$nonfit=0;
	
	//取樣
	$x=intval($record1_length/$km);
	
	echo "每".$km."點取樣一次<br>";
	echo "賽事軌跡共".$record1_length."點<br>";
	echo "共".$x."點錯誤<br>";
	
}
echo "PHP結束";
?>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>

<?php 
echo "<script type='text/javascript'> var wrongPoints=[".$non."]; </script>";
?>
<script type="text/javascript"> 
  var berlin = new google.maps.LatLng(25.0346990,121.6146020);
 
  var polyline = [];
  var points = [];
 
  var map;
 
  function initialize() {
    var mapOptions = {
      zoom: 13,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      center: berlin
    };
 
    map = new google.maps.Map(document.getElementById("map_canvas"),mapOptions);
	path();
  }
 
  function path() {
	 for (var i = 0; i < wrongPoints.length; i++) { 
		  polyline = wrongPoints[i];
		  points.push(polyline);
	 }
	  
	  var flightPath = new google.maps.Polyline({
		path: points,
		strokeColor: "#00ADE2",
		strokeOpacity: 0.8,
		strokeWeight: 5
	  });
	  
	  flightPath.setMap(map);
	  
  }
</script>
</head>
<body onload="initialize()">
  <div id="map_canvas" style="width:100%; height:70%;">
</body>
</html>