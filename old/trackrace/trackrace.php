<?php
$StartLatLon = 0;
$EndLatLon = 727;

$dom1=new DOMDocument();
$dom2=new DOMDocument();

$raceTrack = "A-B1.gpx";
$joinTrack = "A-B5.gpx";

if(($dom1->load($raceTrack))&($dom2->load($joinTrack)))
{
	$root1=$dom1->documentElement;
	$root2=$dom2->documentElement;
	
	$nodes1 = $root1->getElementsByTagName("trkpt");
	$nodes2 = $root2->getElementsByTagName("trkpt");	
	
	$node1_length=$nodes1->length;
	$node2_length=$nodes2->length;
	
	
	//軌跡數量
	echo "賽事軌跡的原始數量".$node1_length."<br>";
	echo "參賽軌跡的原始數量".$node2_length."<br>";
		
	$record1_length = $EndLatLon - $StartLatLon + 1 ;//賽事軌跡數
	$record2_length = $nodes2->length-1;

	/*
	//抓出起始位置
	for($s=0;$s<=$record1_length;$s++)
	{
		
		//echo "算里程";
		$points=$nodes1->item($s);
		
		$lats=$points->getAttribute("lat");
		$lons=$points->getAttribute("lon");
		
		if(($lats==$slat)&($lons==$slon))
		{
			echo "抓到起始座標".$s."<br>";
			break;
		}
	}
	
	//抓出起始位置
	for($e=$record1_length;$e>=0;$e--)
	{
		
		//echo "算里程";
		$pointe=$nodes1->item($e);
		
		$late=$pointe->getAttribute("lat");
		$lone=$pointe->getAttribute("lon");
		
		if(($late==$elat)&($lone==$elon))
		{
			echo "抓到終止座標".$e."<br>";
			break;
		}
	}
	$record1_length=$e;
	
	//算里程
	for($w=$s;$w<=$record1_length;$w++)
	{
		
		//echo "算里程";
		$pointw1=$nodes1->item($w);
		$pointw2=$nodes1->item($w+1);
		
		$latw1=$pointw1->getAttribute("lat");
		$lonw1=$pointw1->getAttribute("lon");
		//$ele=$pointw1->getElementByid("ele");
		
		//echo $ele;
		
		$latw2=$pointw2->getAttribute("lat");
		$lonw2=$pointw2->getAttribute("lon");
		
		//ΔlatinM=Δlat×111199.233米/度
		$latw_length=abs($latw1-$latw2)*111199.233;
		//ΔloninM=Δlon×111199.233米/度×cos(lat)
		$lonw_length=abs($lonw1-$lonw2)*111199.233*cos(abs($latw1-$latw2)/2);
		
		$race_length=sqrt(pow($latw_length,2)+pow($lonw_length,2));
		
		$race_all=$race_all+$race_length;
		
		
		if($w==$record1_length-1)
		{
			$race_all=$race_all*0.94483883/1000;
			echo "賽事總里程".$race_all."km<br>";
			break;
		}
	}*/
	
	$km=20;//intval($race_all)*5;
	
	
	$fitj=0;
	$fit=0;
	$nonfit=0;
	
	//取樣
	$x=intval($record1_length/$km);
	
	echo "每".$km."取樣一次<br>";
	echo $record1_length."<br>";
	echo "共取樣".$x."次<br>";
	
	for($i=$StartLatLon;$i<=$EndLatLon;$i)//i
	{
		//echo "i=".$i;
		
		//echo "在這一***";
		$isFit=false;
		$isBreak=false;
		$point1=$nodes1->item($i);
		$lowerbound=0;
		
		$lat1=$point1->getAttribute("lat");
		$lon1=$point1->getAttribute("lon");
		
		//$non=$non."new google.maps.LatLng(".$lat1.",".$lon1."),";
		
		for($j=$fitj;$j<=$record2_length;$j++)//j
		{
			//echo "j=".$j;
			
			//echo "在這二***";
			$point2=$nodes2->item($j);
			
			$lat2=$point2->getAttribute("lat");
			$lon2=$point2->getAttribute("lon");
			
			//$non=$non."new google.maps.LatLng(".$lat2.",".$lon2."),";
			
			$lat_differ=abs($lat1-$lat2);
			$lon_differ=abs($lon1-$lon2);
			
			//echo $j."<br>";
			
			
			if(($lat_differ<=0.00025)&&($lon_differ<=0.00025))//0.00025為二十公尺,羅誠預設(($lat_differ<=0.00013889)&&($lon_differ<=0.00013889))//if2
			{
				if ($fit == 0) $startFit = $j;
				$fit++;
				$isFit=true;
				$fitj=$j;
				//echo "第二迴圈成立***".$j."<br>";
				//$non=$non."new google.maps.LatLng(".$lat1.",".$lon1."),";
				break;
			}
			else if(($lat_differ<=0.0007)&&($lon_differ<=0.0007))
			{
				//echo "在這三***";
				//抓前一以及後一點的座標
				$point3=$nodes2->item($j+1);
				//$point4=$nodes2->item($j+1);
				if($record2_length<$j+1)$point3=$nodes2->item($j-1);
				
				$lat3=$point3->getAttribute("lat");
				$lon3=$point3->getAttribute("lon");
				//$lat4=$point4->getAttribute("lat");
				//$lon4=$point4->getAttribute("lon");
				//$non =$non."new google.maps.LatLng(".$lat3.",".$lon3."),";
				//echo "Lat:".$lat2."Lon:".$lon2."*error<br>";
				//echo "Lat:".$lat3."Lon:".$lon3."*error<br>";
					
							
				
				//與前一點之間再分成五個點比對
				for($k=1;$k<=4;$k++)//k
				{
					
					//echo "k=".$k;
					
					//echo "在這四***";
					//新創造的座標點	
					$lat_new=$lat2+($lat3-$lat2)*$k/5;
					$lon_new=$lon2+($lon3-$lon2)*$k/5;
					
					//echo $k."Lat:".$lat_new."Lon:".$lon_new."<br>";
					//$non=$non."new google.maps.LatLng(".$lat_new.",".$lon_new."),";

					$lat_differ=abs($lat1-$lat_new);
					$lon_differ=abs($lon1-$lon_new);	
					
					
					if(($lat_diffe<=0.00025)&&($lon_differ<=0.00025))//if2
					{
						if ($fit2 == 0) $startFit2 = $j;
						$fit2++;
						$isFit=true;
						$fitj=$j;
						$isBreak=true;
						
						//echo "第三迴圈成立***".$j."<br>";
						//$non=$non."new google.maps.LatLng(".$lat1.",".$lon1."),";
						break;
					}//if2
				}//k
				if($isBreak)break;
			}//if1
		}//j
		if($isFit==false)
		{					
			$non=$non."new google.maps.LatLng(".$lat1.",".$lon1."),";
			$nonfit++;
		}
		//echo "回到第一迴圈***";
		$i=$i+$km;
		//確保終點也有被比對到, 並抓出參賽者的終點值
		if ($i == $EndLatLon + $km){ 
			break;
		}else if ($i >= $EndLatLon){
			$i = $EndLatLon;
		}
	}//i
	echo '@'.$startFit."-".$startFit2."<br>";
	if (empty($startFit) || ($startFit2 < $startFit)){
		$startFit = $startFit2;
	}
	echo $startFit."<br>";
	echo " <p>比對完成<p>";
	echo "判斷1比對成功 ".$fit."點<br>";
	echo "判斷2比對成功 ".$fit2."點<br>";
	echo "比對失敗 ".$nonfit."點<br>";
	echo "比對成功率 ".($fit+$fit2)/($fit+$fit2+$nonfit)*100 ."%<br>";
	echo $i."-".$j."-".$k."<br>";

	//時間
	$count=0;
	foreach( $nodes2 as $rt ) {
		
		$time = $rt ->getElementsByTagName( "time" );
		$nValue_time = $time->item(0)->nodeValue;
		
		if ($count == $startFit){ 
			$startTime = $nValue_time; 
		}else if ($count == $j){
			$endTime = $nValue_time; 
		}

		$count =1+$count;
	}
	echo $startTime."<br>";
	echo $endTime."<br>";
	list($startY, $startM, $startD, $starth, $startm, $starts)=sscanf($startTime,"%d-%d-%dT%d:%d:%dZ");
	list($endY, $endMm, $starD, $endh, $endm, $ends)=sscanf($endTime,"%d-%d-%dT%d:%d:%dZ");
	
	$endTime = ($endh * 3600 + $endm * 60 + $ends) - ($starth * 3600 + $startm * 60 + $starts);
	
	$endH = floor($endTime / 3600);
	$endM = floor(($endTime -  ($endH * 3600)) / 60);
	$endS = floor($endTime -  ($endH * 3600) - ($endM * 60));
	$endD = $endY."/".$endMm."/".$starD;
	
	echo $endD."-".$endH."-".$endM."-".$endS;
	if (($fit+$fit2)/($fit+$fit2+$nonfit)*100 < 90) $errorTrack = 1;//軌跡比對結果小於90%就算是錯誤軌跡
	echo "$".$errorTrack."$";
}
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>

<?php 
echo "<script type='text/javascript'> var wrongPoints=[".$non."]; </script>";
?>
<script type="text/javascript"> 
  var berlin = new google.maps.LatLng(25.0346990,121.6146020);
 
  var polyline = [];
  var points = [];
 
  var map;
 
  function initialize() {
    var mapOptions = {
      zoom: 13,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      center: berlin
    };
 
    map = new google.maps.Map(document.getElementById("map_canvas"),mapOptions);
	path();
  }
 
  function path() {
	 for (var i = 0; i < wrongPoints.length; i++) { 
		  polyline = wrongPoints[i];
		  points.push(polyline);
	 }
	  
	  var flightPath = new google.maps.Polyline({
		path: points,
		strokeColor: "#00ADE2",
		strokeOpacity: 0.8,
		strokeWeight: 5
	  });
	  
	  flightPath.setMap(map);
	  
  }
</script>
<body onLoad="initialize()">
  <div id="map_canvas" style="width:100%; height:70%;">
</body>