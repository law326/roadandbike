<?php
/* -----bobo註解-----
 * module:jQuery-Validation-Engin、GeoIP、jQueryTip、PHP表單驗證檔
 * note:幹部不能退出、單車生涯為選填、照片上傳丟到羅那無法動作
 * 車隊判斷 	退出車隊
 *
*/

session_start();
unset($_SESSION["upload_kind"]);

require_once('Connections/autologin.php');//判斷訪客狀態(登入、未登入)，再決定您"目前所在的城市"資訊

//●未登入不可瀏覽 
if ($_SESSION['islogin'] == FALSE) header("Location:  index.php");  



list($Country_reg,$City_reg) = geoid_to_city( $row['GeoID']); //將會員的GeoID轉換成City



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>RoadandBike - Tour in Your Life</title> 

<meta name="title" content="RoadandBike"> 
 
<meta name="description" content="Tour in Your Life"> 
 
<meta name="keywords" content="軌跡、分享、單車、賽事、免費"> 
<link rel="shortcut icon" type="image/x-icon" href="images/icon.png">
<link href="css/page_style.css" rel="stylesheet" type="text/css" />
<!--停用JavaScript時-->
<noscript>
<META HTTP-EQUIV="Refresh" CONTENT="0;URL=noJavaScript.html">
</noscript>
<!--停用JavaScript時-->

<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/redmond/jquery-ui.css">	
<link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css"/>
<link href="css/tooltip.css" rel="stylesheet" type="text/css" />
<!--所在地區-->
<script src="CollapsiblePanel/CollapsiblePanel.js" type="text/javascript"></script>
<!--所在地區-->
<!--lightbox-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<script type="text/javascript" src="lightbox/custom.js"></script>
<!--lightbox-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
<script src="http://cdn.jquerytools.org/1.2.5/full/jquery.tools.min.js"></script> 
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.11/jquery-ui.min.js"></script>

<script src="js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
<script>
$(document).ready(function(){
	$("#City").focus(function() {
		// 會依據國家欄位的值 去判斷 有哪些city
		var Country_city = "signup_CountryCity.php?kind=City&City_Country=" + document.getElementById("Country").value;
			$("#City").autocomplete({
			source: Country_city ,
			minLength: 0,
			select: function(event, ui) {
				$('#id_GeoID').val(ui.item.id); //將GeoID值存到hidden 方便傳送
			}
		});
	});							//國家城市欄位找值--------------------
	
	$('#submit').click(function (){ //將網頁載入時預設在#City的值利用AJAX轉換成GeoID
		   $.ajax({
		   url: 'signup_CountryCity.php',
		   cache: false,
		   dataType: 'text', //dataType 預計會從url回傳的dataType  類型有4種：html,xml,json,text
			   type:'GET',
		   data: { kind: 'City' ,City_Country: $('#Country').val() ,term: $('#City').val()},//帶過去url的變數名稱
		   error: function(xhr) {
			 alert('Ajax request 發生錯誤');
		   },
		   success: function(response) {// request succeeds的回傳值
		   var jsonContent = eval("(" + response + ")");//將JSON文字轉成物件
			 $('#id_GeoID').val(jsonContent[0].id);
		   }
		   
	   });
	});
	
	$("a#changePhoto").click(function(){
		$("div#upload_crop").empty().load("upload_crop/upload_crop.php?upload_kind=user");
		return false;
    });
	
		/* 錯誤顯示位置在 jquery.validationEngine.js promptPosition: "centerLeft"  
	   錯誤訊息框的寬度設定在validationEngine.jquery.css	*/
	jQuery("#form1").validationEngine(); //不可放在國家欄位找值之前 會失效
	


});
</script>

</head>

<body>
	<?php require_once('include_header.php'); ?>
        <!--lightbox_imgupload-->
        <div id="popupre" class="popupboxb">
  			<div class="back_sb"></div>
  			<div id="imgupload">
            	<div class="box_top word_type_bb18">
                	上傳車手照片 
                </div>
                <div class="text">
					<iframe width="100%" height="380" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="upload_crop/upload_crop.php?upload_kind=user"></iframe>
                </div>
            </div>
		</div>
		<div id="fade"></div>
		<!--lightbox_imgupload-->
		<div id="container">
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
            	<tr>
                	<td valign="top">
                    	<div id="main">
                        <div id="title2">
                        	<div class="text word_type_bb24">車手檔案</div>
                        </div>
                       <form action="<?php echo $editFormAction; ?>" method="POST" enctype="multipart/form-data" name="form1" id="form1">
						<div id="settings">
						  <div class="block">
                            <table width="100%" cellspacing="0" cellpadding="0">
                              <tr>
                                <td valign="top"><div class="topic">車隊照片</div></td>
                                <td width="100%"><div class="picb"><img border="0" src="<?php echo $TeamImg;?>" width="100%" /></div>
                                  <div class="text word_type_g12"><a class="popup" href="#" rel="popupre">變更照片</a></div>
                                <div class="text word_type_g12">選一張照片來代表您的車隊</div></td>
                              </tr>
                            </table>
                          </div>
                          <div class="block">
                          <table width="100%" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td valign="top"><div class="topic">
                                    車隊名稱
                                  </div></td>
                                  <td width="100%"><div class="text">
                                    <input name="TeamName" type="text" id="TeamName" value="<?php echo $row_Team['TeamName']; ?>"  class="validate[required] text-input"/>
                                   <span id="idErrMsg"> </span><span id="msgbox" style="display:none"></span>
                                  </div>
                                  <div class="text word_type_g12">告訴大家您車隊的名子</div></td>
                                </tr>
                              </table>
                          </div>
                            <div class="block">
                              <table width="100%" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td valign="top"><div class="topic">成立時間</div></td>
                                  <td width="100%"><div class="text">
                                    <select name="TeamCreate_year" id="TeamCreate_year"  autocomplete="off" class="validate[required]">
                                      <option value="">年</option>
                                      <?php 
									 for ($i=1920; $i<=date("Y");$i++){
										 if ($i==$selected_year) {
											$selected="selected";
											}
										 else{
											$selected="";
											}
										echo "<option  value=\"$i\" $selected >$i</option>";
									 }?>
                                    </select>
                                    <select  id="TeamCreate_month" name="TeamCreate_month" class="validate[required]" >
                                      <option  value="">月</option>
                                     <?php 
									 for ($i=1; $i<=12;$i++){
										 if ($i==$selected_month) {
											$selected="selected";
											}
										 else{
											$selected="";
											}
										echo "<option  value=\"$i\" $selected >$i</option>";
									}?>
                                    </select>
                                    <select name="TeamCreate_day" id="TeamCreate_day"  autocomplete="off" class="validate[required]" >
                                      <option  value="">日</option>
                                     <?php 
									 for ($i=1; $i<=31;$i++){
										 if ($i==$selected_day) {
											$selected="selected";
											}
										 else{
											$selected="";
											}
										echo "<option  value=\"$i\" $selected >$i</option>";
									}?> 
                                    </select>
                                  </div>
                                  <div class="text word_type_g12">告訴大家您車隊的成立時間</div></td>
                                </tr>
                              </table>
                            </div>
                            <div class="block">
                              <table width="100%" cellspacing="0" cellpadding="0">
                              <tr>
                                <td width="100%"><div class="text">
                                  <table width="100%" cellspacing="0" cellpadding="0">
                                    <tr>
                                      <td valign="top"><div class="topic">
                                        成立地點
                                      </div></td>
                                      <td width="100%"><div class="text">
                                        <input name="City" type="text" class="validate[required,minSize[2],custom[onlyLetterSp]] text-input" id="City" title="車隊成立地點(或欄位空白時按↓自動選擇)" value="<?php echo $City_reg;?>" />
                                      </div>
                                        <div class="text word_type_g12">告訴大家您車隊的成立地</div></td>
                                    </tr>
                                  </table>
                                </div></td>
                              </tr>
                              </table>
                            </div>
                          <div class="block" style="display:none;">
                              <table width="100%" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td valign="top"><div class="topic">車隊幹部</div></td>
                                  <td width="100%"><div class="block">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                      <tr>
                                        <td><div class="pic"><img border="0" src="images/41636_100000069404320_7306_q.jpg" width="30" /></div>
                                          <a href="#" class="word_type_bb12">李大偉</a> (黃衫排名#12)<br />
                                          <span class="word_type_g12">Giant TCR2</span></td>
                                      </tr>
                                    </table>
                                  </div>
                                    <div class="block">
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                          <td><div class="pic"><img border="0" src="images/41636_100000069404320_7306_q.jpg" width="30" /></div>
                                            <a href="#" class="word_type_bb12">李大偉</a> (黃衫排名#12)<br />
                                            <span class="word_type_g12">Giant TCR2</span></td>
                                        </tr>
                                      </table>
                                    </div>
                                    <div class="block">
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                          <td><div class="pic"><img border="0" src="images/41636_100000069404320_7306_q.jpg" width="30" /></div>
                                            <a href="#" class="word_type_bb12">李大偉</a> (黃衫排名#12)<br />
                                            <span class="word_type_g12">Giant TCR2</span></td>
                                        </tr>
                                      </table>
                                  </div>                                    
                                  <div class="text"><a href="#">新增車隊幹部</a></div>
                                  <div class="text word_type_g12">成為正式車隊並取得參賽權需要先擁有三位車隊幹部</div></td>
                                </tr>
                              </table>
                          </div>
                          <div class="box_bottom">
                            <input class="word_type_wb12" type="submit" name="submit" id="submit" value="送出" />
                            <input name="Country" type="hidden" id="Country" value="<?php echo $Country_reg ;?>" disabled="disabled" />
                            <input type="input" id="id_GeoID" name="id_GeoID" value="<?php echo $GeoID ;?>" />  
                            <input type="hidden" name="From_insert" value="form1" />
                          </div>
                        </div>
                    </form>
                    </div>
                    </td>
                    <td valign="top">
                        <div id="side">
                          <div class="menu">
                          	<div id="visiblebox"></div>
                            <div class="block3">
                              <a href="settings.php" class="word_type_bb14">帳戶資訊</a><br />
  </div>
                          <div class="block2"> <a href="settings_rider.php" class="word_type_bb14">車手檔案</a></div>
                          <div class="block3"> <a href="settings_bike.php" class="word_type_bb14">單車檔案</a><br />
                          </div>
                          <div class="block3"> <a href="settings_team.php" class="word_type_bb14">車隊檔案</a><br />
                          </div>
                          </div>
                          
                        </div>
                	</td>
            	</tr>
			</table>
	  </div>
	<?php require_once('include_footer.php'); ?>	 
</body>
</html>
