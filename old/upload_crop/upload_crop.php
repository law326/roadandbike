<?php
/* -----bobo註解-----
 * note: 50行	default://$upload_dir = "user_pic"; 
 * 
 *
*/

error_reporting (E_ALL ^ E_NOTICE);
session_start(); 

require_once('Connections/dbConn.php');
$Email="bobo52310@gmail.com"; //測試用帳號0327待刪除  $_COOKIE["isLogin_Email"];
		
$result = mysql_query("select * from tb_members where Email='$Email' ")or die(mysql_error());
$row = mysql_fetch_assoc($result);


$IDImg=$row["IDImg"];
$BikeImg=$row["BikeImg"];

if (empty($IDImg)) {//如果資料庫撈出來no data
	$IDImg="../images/IDImg_default.jpg";
	$BikeImg="..images/BikeImg_default.jpg";		
}
else{
	//$large_photo_exists=$row["IDImg"];
	//$large_image_location=$row["IDImg"];
	//$thumb_photo_exists="132";

}


//session不存在產生一個 timestampy
if (!isset($_SESSION['random_key']) || strlen($_SESSION['random_key'])==0){
    $_SESSION['random_key'] = strtotime(date('Y-m-d H:i:s')). chr(rand(65,90)). chr(rand(65,90)); //用timestamp和rand避免同時間上傳檔名
	$_SESSION['user_file_ext']= "";
}


if (!isset($_SESSION["upload_kind"])) $_SESSION["upload_kind"]=$_GET["upload_kind"];
$kind=$_SESSION["upload_kind"];

//------------基本設定---------------------
switch ($kind){
	case "user":
	$upload_dir = "user_pic"; //要儲存的資料夾名稱
	break;
	case "bike":
	$upload_dir = "bike_pic"; 
	break;
	case "team":
	$upload_dir = "team_pic"; 
	break;
	default:	
	//$upload_dir = "user_pic"; 
}

$upload_path = $upload_dir."/";				// The path to where the image will be saved
$large_image_prefix = "ori_"; 			// 大圖 原始檔 檔名字首
$thumb_image_prefix = "re_";			// 縮圖 檔名字首
$large_image_name = $large_image_prefix.$_SESSION['random_key'];     
$thumb_image_name = $thumb_image_prefix.$_SESSION['random_key'];     
$max_file = "1"; 							// Maximum file size in MB
$max_width = "500";							// Max width allowed for the large image
$thumb_width = "180";						// Width of thumbnail image
$thumb_height = "180";						// Height of thumbnail image
//與許的圖片格式
$allowed_image_types = array('image/pjpeg'=>"jpg",'image/jpeg'=>"jpg",'image/jpg'=>"jpg",'image/png'=>"png",'image/x-png'=>"png",'image/gif'=>"gif");
$allowed_image_ext = array_unique($allowed_image_types); // do not change this
$image_ext = "";	// initialise variable, do not change this.
foreach ($allowed_image_ext as $mime_type => $ext) {
    $image_ext.= strtoupper($ext)." ";
}



#################### Image 相關 ########################################################################
function resizeImage($image,$width,$height,$scale) {
	list($imagewidth, $imageheight, $imageType) = getimagesize($image);
	$imageType = image_type_to_mime_type($imageType);
	$newImageWidth = ceil($width * $scale);
	$newImageHeight = ceil($height * $scale);
	$newImage = imagecreatetruecolor($newImageWidth,$newImageHeight);
	switch($imageType) {
		case "image/gif":
			$source=imagecreatefromgif($image); 
			break;
	    case "image/pjpeg":
		case "image/jpeg":
		case "image/jpg":
			$source=imagecreatefromjpeg($image); 
			break;
	    case "image/png":
		case "image/x-png":
			$source=imagecreatefrompng($image); 
			break;
  	}
	imagecopyresampled($newImage,$source,0,0,0,0,$newImageWidth,$newImageHeight,$width,$height);
	
	switch($imageType) {
		case "image/gif":
	  		imagegif($newImage,$image); 
			break;
      	case "image/pjpeg":
		case "image/jpeg":
		case "image/jpg":
	  		imagejpeg($newImage,$image,90); 
			break;
		case "image/png":
		case "image/x-png":
			imagepng($newImage,$image);  
			break;
    }
	
	chmod($image, 0777);
	return $image;
}

function resizeThumbnailImage($thumb_image_name, $image, $width, $height, $start_width, $start_height, $scale){
	list($imagewidth, $imageheight, $imageType) = getimagesize($image);
	$imageType = image_type_to_mime_type($imageType);
	
	$newImageWidth = ceil($width * $scale);
	$newImageHeight = ceil($height * $scale);
	$newImage = imagecreatetruecolor($newImageWidth,$newImageHeight);
	switch($imageType) {
		case "image/gif":
			$source=imagecreatefromgif($image); 
			break;
	    case "image/pjpeg":
		case "image/jpeg":
		case "image/jpg":
			$source=imagecreatefromjpeg($image); 
			break;
	    case "image/png":
		case "image/x-png":
			$source=imagecreatefrompng($image); 
			break;
  	}
	imagecopyresampled($newImage,$source,0,0,$start_width,$start_height,$newImageWidth,$newImageHeight,$width,$height);
	switch($imageType) {
		case "image/gif":
	  		imagegif($newImage,$thumb_image_name); 
			break;
      	case "image/pjpeg":
		case "image/jpeg":
		case "image/jpg":
	  		imagejpeg($newImage,$thumb_image_name,90); 
			break;
		case "image/png":
		case "image/x-png":
			imagepng($newImage,$thumb_image_name);  
			break;
    }
	chmod($thumb_image_name, 0777);
	return $thumb_image_name;
}

function getHeight($image) {
	$size = getimagesize($image);
	$height = $size[1];
	return $height;
}

function getWidth($image) {
	$size = getimagesize($image);
	$width = $size[0];
	return $width;
}
####################以上 Image 相關 ########################################################################

//Image Locations
$large_image_location = $upload_path.$large_image_name.$_SESSION['user_file_ext'];
$thumb_image_location = $upload_path.$thumb_image_name.$_SESSION['user_file_ext'];

//若上傳資料夾不存在則建立
if(!is_dir($upload_dir)){
	mkdir($upload_dir, 0777);
	chmod($upload_dir, 0777);
}

//check 是否有相同檔名圖片存在
if (file_exists($large_image_location)){
	if(file_exists($thumb_image_location)){
		$thumb_photo_exists = "<img src=\"".$upload_path.$thumb_image_name.$_SESSION['user_file_ext']."\" alt=\"Thumbnail Image\"/>";
	}else{
		$thumb_photo_exists = "";
	}
   	$large_photo_exists = "<img src=\"".$upload_path.$large_image_name.$_SESSION['user_file_ext']."\" alt=\"Large Image\"/>";
} else {
   	$large_photo_exists = "";
	$thumb_photo_exists = "";
}


/*按下上傳按鈕*/
if (isset($_POST["upload"])) { 
	//取得檔案資訊
	$userfile_name = $_FILES['image']['name'];
	$userfile_tmp = $_FILES['image']['tmp_name'];
	$userfile_size = $_FILES['image']['size'];
	$userfile_type = $_FILES['image']['type'];
	$filename = basename($_FILES['image']['name']);
	$file_ext = strtolower(substr($filename, strrpos($filename, '.') + 1));
	
	//Only process if the file is a JPG, PNG or GIF and below the allowed limit
	if((!empty($_FILES["image"])) && ($_FILES['image']['error'] == 0)) {
		
		foreach ($allowed_image_types as $mime_type => $ext) {
			//loop through the specified image types and if they match the extension then break out
			//everything is ok so go and check file size
			if($file_ext==$ext && $userfile_type==$mime_type){
				$error = "";
				break;
			}else{
				$error = "Only <strong>".$image_ext."</strong> images accepted for upload<br />";
			}
		}
		//check if the file size is above the allowed limit
		if ($userfile_size > ($max_file*1048576)) {
			$error.= "Images must be under ".$max_file."MB in size";
		}
		
	}else{
		$error= "請選擇圖片上傳!";
	}
	//Everything is ok 準備upload the image
	if (strlen($error)==0){
		
		if (isset($_FILES['image']['name'])){
			//this file could now has an unknown file extension (we hope it's one of the ones set above!)
			$large_image_location = $large_image_location.".".$file_ext;
			$thumb_image_location = $thumb_image_location.".".$file_ext;
			
			//put the file ext in the session so we know what file to look for once its uploaded
			$_SESSION['user_file_ext']=".".$file_ext;
			
			move_uploaded_file($userfile_tmp, $large_image_location);
			chmod($large_image_location, 0777);
			
			$width = getWidth($large_image_location);
			$height = getHeight($large_image_location);
			//Scale the image if it is greater than the width set above
			if ($width > $max_width){
				$scale = $max_width/$width;
				$uploaded = resizeImage($large_image_location,$width,$height,$scale);
			}else{
				$scale = 1;
				$uploaded = resizeImage($large_image_location,$width,$height,$scale);
			}
			//Delete the thumbnail file so the user can create a new one
			if (file_exists($thumb_image_location)) {
				unlink($thumb_image_location);
			}
		}
		//Refresh the page to show the new uploaded image
		header("location:".$_SERVER["PHP_SELF"]  );
		exit();
	}
}

/*按下建立圖片剪裁後*/
if (isset($_POST["upload_thumbnail"]) && strlen($large_photo_exists)>0) {
	//建立一個新的 縮圖.
	$x1 = $_POST["x1"];
	$y1 = $_POST["y1"];
	$x2 = $_POST["x2"];
	$y2 = $_POST["y2"];
	$w = $_POST["w"];
	$h = $_POST["h"];
	//Scale the image to the thumb_width set above
	$scale = $thumb_width/$w;
	$cropped = resizeThumbnailImage($thumb_image_location, $large_image_location,$w,$h,$x1,$y1,$scale);
	
	//把大圖刪掉
	if (file_exists($large_image_location)) {
		unlink($large_image_location);
	}
	
	//將縮圖位置update至IDImg或BikeImg
	$thumb_image_location="upload_crop/".$thumb_image_location ;
	switch ($kind){
		case "user":
		$query = "UPDATE  tb_members SET IDImg='$thumb_image_location'  where Email='$Email' ";
		break;
		case "bike":
		$query = "UPDATE  tb_members SET BikeImg='$thumb_image_location'  where Email='$Email' ";
		break;
		case "team":
		//$query = "UPDATE  tb_members SET IDImg='$thumb_image_location'  where Email='$Email' ";
		break;
	}

	mysql_query($query,$dbConn) or die(mysql_error());
	
	unset($_SESSION['random_key']);
	unset($_SESSION['user_file_ext']);
	unset($_SESSION["upload_kind"]);
	

	//導回settings.php
	/*header("location:../settings_rider.php");
	exit(); */
	?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
	<script type="text/javascript">
        //使母frame 的lightboxfadeout
        $("div#popupre", parent.document).fadeOut() ;
        $("div#fade", parent.document).fadeOut();
        $("body", parent.document).reload();
    </script>    
<?
exit;
}


if ($_GET['a']=="delete" && strlen($_GET['t'])>0){
		//get the file locations 
		$large_image_location = $upload_path.$large_image_prefix.$_GET['t'];
		$thumb_image_location = $upload_path.$thumb_image_prefix.$_GET['t'];
		if (file_exists($large_image_location)) {
			unlink($large_image_location);
		}
		if (file_exists($thumb_image_location)) {
			unlink($thumb_image_location);
		}
		header("location:".$_SERVER["PHP_SELF"]);
		exit(); 
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>圖示剪裁</title>
	<script type="text/javascript" src="jquery-pack.js"></script>
	<script type="text/javascript" src="jquery.imgareaselect.min.js"></script>
</head>
<body>
<br />

<?php
//當大圖已經上傳,顯示javacript

if(strlen($large_photo_exists)>0){
			$current_large_image_width = getWidth($large_image_location);
			$current_large_image_height = getHeight($large_image_location);?>
<script type="text/javascript">
			function preview(img, selection) { 
				var scaleX = <?php echo $thumb_width;?> / selection.width; 
				var scaleY = <?php echo $thumb_height;?> / selection.height; 
				
				$('#thumbnail + div > img').css({ 
					width: Math.round(scaleX * <?php echo $current_large_image_width;?>) + 'px', 
					height: Math.round(scaleY * <?php echo $current_large_image_height;?>) + 'px',
					marginLeft: '-' + Math.round(scaleX * selection.x1) + 'px', 
					marginTop: '-' + Math.round(scaleY * selection.y1) + 'px' 
				});
				$('#x1').val(selection.x1);
				$('#y1').val(selection.y1);
				$('#x2').val(selection.x2);
				$('#y2').val(selection.y2);
				$('#w').val(selection.width);
				$('#h').val(selection.height);
			} 
			
			$(document).ready(function () { 
				$('#save_thumb').click(function() {
					var x1 = $('#x1').val();
					var y1 = $('#y1').val();
					var x2 = $('#x2').val();
					var y2 = $('#y2').val();
					var w = $('#w').val();
					var h = $('#h').val();
					if(x1=="" || y1=="" || x2=="" || y2=="" || w=="" || h==""){
						alert("請選取您的圖示範圍~");
						return false;
					}else{
						return true;
					}
				});
			}); 
			
			$(window).load(function () { 
				$('#thumbnail').imgAreaSelect({ aspectRatio: '1:<?php echo $thumb_height/$thumb_width;?>', onSelectChange: preview }); 
			});
			
			</script>
<?php }?>

<?php
//若error 顯示error訊息
if(strlen($error)>0){
	echo "<ul><li><strong>Error!</strong></li><li>".$error."</li></ul>";
}


if(strlen($large_photo_exists)>0 && strlen($thumb_photo_exists)>0){

	//Clear the time stamp session and user file extension
	$_SESSION['random_key']= "";
	$_SESSION['user_file_ext']= "";
}
else{
		if(strlen($large_photo_exists)>0){?>

		<div align="center">

			<img border="0" src="<?php echo $upload_path.$large_image_name.$_SESSION['user_file_ext'];?>" style="float: left; margin-right: 10px;" id="thumbnail" alt="Create Thumbnail" />
	  <div style="border:1px #e5e5e5 solid; float:left; position:relative; overflow:hidden; width:<?php echo $thumb_width;?>px; height:<?php echo $thumb_height;?>px;">
				<img border="0" src="<?php echo $upload_path.$large_image_name.$_SESSION['user_file_ext'];?>" style="position: relative;" alt="Thumbnail Preview" />

</div>
<form name="thumbnail" action="<?php echo $_SERVER["PHP_SELF"];?>" method="post">
            <input type="submit" name="upload_thumbnail" value="建立圖片剪裁" id="save_thumb" />
        <input type="hidden" name="x1" value="" id="x1" />
				<input type="hidden" name="y1" value="" id="y1" />
				<input type="hidden" name="x2" value="" id="x2" />
				<input type="hidden" name="y2" value="" id="y2" />
				<input type="hidden" name="w" value="" id="w" />
				<input type="hidden" name="h" value="" id="h" />
		  </form>			
		</div>
	<hr />
	<?php 	} ?>
	
    
    
    <hr />
    <br />
<br />

	<form name="photo" enctype="multipart/form-data" action="<?php echo $_SERVER["PHP_SELF"];?>" method="post">
    <!--放置區-->
  <div class="block">
    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="500" height="300" align="center" valign="middle">
             <div id="side">
               	  <p>&nbsp;</p>
                	<p><br />
                	  <br />
              	  </p>
           	   <h2>請上傳您的<?php echo $upload_kind ;?>圖示</h2>
                    <div class="text">
                      <input type="file" name="image" size="20" /> <input type="submit" name="upload" value="上傳" />
                    </div>
                    <div class="text"></div>
              </div>
            </td>
            <td valign="top">
           	 
            </td>
          </tr>
        </table>
    </div>
<!--放置區-->
	</form>
<?php } ?>

</body>
</html>
