<?php
/* -----bobo註解-----
 * module:jQuery-Validation-Engin、GeoIP、jQueryTip、PHP表單驗證檔
 * note:幹部不能退出、單車生涯為選填、照片上傳丟到羅那無法動作
 * 車隊判斷 	退出車隊
 *
*/

session_start();
unset($_SESSION["upload_kind"]);

require_once('Connections/autologin.php');//判斷訪客狀態(登入、未登入)，再決定您"目前所在的城市"資訊

//●未登入不可瀏覽 
if ($_SESSION['islogin'] == FALSE) header("Location:  index.php");  

list($Country_reg,$City_reg) = geoid_to_city( $row['GeoID']); //將會員的GeoID轉換成City

$RideAge=$row["RideAge"];
if ($RideAge=="0000-00-00") {
	//
}
else{
	$utime=strtotime($RideAge);
	$selected_year =date("Y",$utime);
	$selected_month =date("n",$utime);
	$selected_day =date("j",$utime);
}



$IDImg=$row["IDImg"];


//是否為自由車手
if ($row['IsTeam']==0){
	$TeamName = "自由車手";
	
}
else{
	//有加入車隊的人
	
	//找出對應車隊名稱
	$result = mysql_query("SELECT  team.TeamName 
								FROM tb_team 		    as team, 
								     tb_team_attendee   as attendee
								WHERE team.TeamID = attendee.TeamID
								 AND  attendee.MemberID = '$MemberID' ")or die(mysql_error());
	//找到所屬車隊名稱
	if ($result){
		$row_team = mysql_fetch_assoc($result);
		$TeamName = $row_team['TeamName'];	
	}
	 

}


if (empty($IDImg)) $IDImg="images/IDImg_default.jpg";
if (empty($TeamName)) $TeamName="自由車手";


//修改車手檔案
$editFormAction = $_SERVER['PHP_SELF']; //目前正在執行的檔案名稱
if ((isset($_POST["From_insert"])) && ($_POST["From_insert"] == "form1") ) {
	
	require_once "Connections/PHP_formvalidator.php";//載入PHP表單驗證檔
    /*做這些PHP驗證*/
    $validator = new FormValidator();
	$validator->addValidation("City","req","請輸入City");	
	$validator->addValidation("City","alnum_s","只能輸入英文字母、數字(准許空白)");	
	
    $validator->addValidation("ride_year","num","請輸入ride_year");	
	$validator->addValidation("ride_month","num","請輸入ride_month");
	$validator->addValidation("ride_day","num","請輸入ride_day");

	if(!$validator->ValidateForm())	{
	//若驗證失敗進入此區
	 	echo "<B>驗證錯誤:</B>";

        $error_hash = $validator->GetErrors();
        foreach($error_hash as $inpname => $inp_err)
        {
            echo "<p>$inpname : $inp_err</p>\n";
        }        
		exit;
	}

	
	$id_GeoID=$_POST["id_GeoID"];
	
	/* 由utime轉成 MySQL Date Format 2010-02-10 */
	$utime=strtotime($_POST['ride_year']."-".$_POST['ride_month']."-".$_POST['ride_day']);
	$RideAge =date("Y-m-d",$utime);
	
	$Email=$_SESSION["Email"];
	 	
	if ( !empty($id_GeoID)){//確認更改後城市不為empty 執行UPDATE
		$query = "UPDATE  tb_members SET GeoID='$id_GeoID' , RideAge='$RideAge' WHERE Email='$Email' ";
		mysql_query($query,$dbConn) or die(mysql_error())	;
		//修改cookie GeoID
		//setcookie("GeoID", $id_GeoID, strtotime("1 January 2020")); //用來存放 您現在位置代碼
		//$_SESSION["GeoID"] = $id_GeoID;
		
		/* 確認參賽資格(tb_bike是否至少有1台車)&(有設定單車圖片)&(車手檔案有設個人照片)  
			則tb_members →AccessLevel=2.一般用戶
		*/
		
		//找出myBike	 的BikeImg	
		$result_myBike = mysql_query("SELECT BikeImg 
									FROM     tb_bike 	
									WHERE 	 MemberID = '$MemberID'")or die(mysql_error());
		$row_myBike = mysql_fetch_assoc($result_myBike);
		
		//檢查有單車照片+有個人照片
		if ( !empty($row_myBike["BikeImg"]) && !empty($row["IDImg"]) ) {
		//	則tb_members →AccessLevel=2.一般用戶
			$update_sql = "Update tb_members SET AccessLevel = 2 WHERE MemberID='$MemberID' ";
			mysql_query($update_sql,$dbConn) or die(mysql_error())	;
		}
		
		header("Location:  $editFormAction");//重新載入page
	}
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>RoadandBike - Tour in Your Life</title> 

<meta name="title" content="RoadandBike"> 
 
<meta name="description" content="Tour in Your Life"> 
 
<meta name="keywords" content="軌跡、分享、單車、賽事、免費"> 
<link rel="shortcut icon" type="image/x-icon" href="images/icon.png">
<link href="css/page_style.css" rel="stylesheet" type="text/css" />
<!--停用JavaScript時-->
<noscript>
<META HTTP-EQUIV="Refresh" CONTENT="0;URL=noJavaScript.html">
</noscript>
<!--停用JavaScript時-->

<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/redmond/jquery-ui.css">	
<link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css"/>
<link href="css/tooltip.css" rel="stylesheet" type="text/css" />
<!--所在地區-->
<script src="CollapsiblePanel/CollapsiblePanel.js" type="text/javascript"></script>
<!--所在地區-->
<!--lightbox-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<script type="text/javascript" src="lightbox/custom.js"></script>
<!--lightbox-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
<script src="http://cdn.jquerytools.org/1.2.5/full/jquery.tools.min.js"></script> 
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.11/jquery-ui.min.js"></script>

<script src="js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
<script>
$(document).ready(function(){
	$("#City").focus(function() {
		// 會依據國家欄位的值 去判斷 有哪些city
		var Country_city = "signup_CountryCity.php?kind=City&City_Country=" + document.getElementById("Country").value;
			$("#City").autocomplete({
			source: Country_city ,
			minLength: 0,
			select: function(event, ui) {
				$('#id_GeoID').val(ui.item.id); //將GeoID值存到hidden 方便傳送
			}
		});
	});							//國家城市欄位找值--------------------
	
	$('#submit').click(function (){ //將網頁載入時預設在#City的值利用AJAX轉換成GeoID
		   $.ajax({
		   url: 'signup_CountryCity.php',
		   cache: false,
		   dataType: 'text', //dataType 預計會從url回傳的dataType  類型有4種：html,xml,json,text
			   type:'GET',
		   data: { kind: 'City' ,City_Country: $('#Country').val() ,term: $('#City').val()},//帶過去url的變數名稱
		   error: function(xhr) {
			 alert('Ajax request 發生錯誤');
		   },
		   success: function(response) {// request succeeds的回傳值
		   var jsonContent = eval("(" + response + ")");//將JSON文字轉成物件
			 $('#id_GeoID').val(jsonContent[0].id);
		   }
		   
	   });
	});
	
	$("a#changePhoto").click(function(){
		$("div#upload_crop").empty().load("upload_crop/upload_crop.php?upload_kind=user");
		return false;
    });
	
		/* 錯誤顯示位置在 jquery.validationEngine.js promptPosition: "centerLeft"  
	   錯誤訊息框的寬度設定在validationEngine.jquery.css	*/
	jQuery("#form1").validationEngine(); //不可放在國家欄位找值之前 會失效
	


});
</script>

</head>

<body>
	<?php require_once('include_header.php'); ?>
        <!--lightbox_imgupload-->
        <div id="popupre" class="popupboxb">
  			<div class="back_sb"></div>
  			<div id="imgupload">
            	<div class="box_top word_type_bb18">
                	上傳車手照片 
                </div>
                <div class="text">
					<iframe width="100%" height="380" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="upload_crop/upload_crop.php?upload_kind=user"></iframe>
                </div>
            </div>
		</div>
		<div id="fade"></div>
		<!--lightbox_imgupload-->
		<div id="container">
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
            	<tr>
                	<td valign="top">
                    	<div id="main">
                        <div id="title2">
                        	<div class="text word_type_bb24">車手檔案</div>
                        </div>
                        <form action="<?php echo $editFormAction; ?>" method="POST" enctype="multipart/form-data" name="form1" id="form1">
						<div id="settings">
						  <div class="block">
					      <table width="100%" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td valign="top"><div class="topic">車手照片</div>
                                  沒設定不能參賽喔!</td>
                                  <td width="100%"><div class="picb"><img border="0" src="<?php echo $IDImg ;?>" width="100%" /></div>
									<div class="text word_type_g12"><a class="popup" href="#" rel="popupre" id="changePhoto">變更照片</a></div>
                                    <div class="text word_type_g12">選擇一張最得意的照片來代表您</div></td>
                                </tr>
                  </table>
                        </div>
                        <div class="block">
                              <table width="100%" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td valign="top"><div class="topic">國籍*</div></td>
                                  <td width="100%"><div class="text">
                                    <input name="Country" type="text" id="Country" value="<?php echo $Country_reg ;?>" disabled="disabled" />
                                  </div>
                                  <div class="text word_type_g12">代表您的國家, 並為她奮戰</div></td>
                                </tr>
                              </table>
                          </div>
                            <div class="block">
                              <table width="100%" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td valign="top"><div class="topic">現居都市</div></td>
                                  <td width="100%"><div class="text">
                                    <input name="City" type="text" class="validate[required,minSize[2],custom[onlyLetterSp]] text-input" id="City" title="您所在的城市(或欄位空白時按↓自動選擇)" value="<?php echo $City_reg;?>" />
                                  </div>
                                  <div class="text word_type_g12">您的居住城市將會影響系統推薦給您的賽事資訊</div></td>
                                </tr>
                              </table>
                          </div>
                          <div class="block">
                              <table width="100%" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td valign="top"><div class="topic">單車生涯</div></td>
                                  <td width="100%"><div class="text">
                                    <select name="ride_year" id="ride_year"  autocomplete="off" class="validate[required]">
                                      <option value="">年</option>
                                      <?php 
									 for ($i=1920; $i<=date("Y");$i++){
										 if ($i==$selected_year) {
											$selected="selected";
											}
										 else{
											$selected="";
											}
										echo "<option  value=\"$i\" $selected >$i</option>";
									 }?>
                                    </select>
                                    <select  id="ride_month" name="ride_month" class="validate[required]" >
                                      <option  value="">月</option>
                                     <?php 
									 for ($i=1; $i<=12;$i++){
										 if ($i==$selected_month) {
											$selected="selected";
											}
										 else{
											$selected="";
											}
										echo "<option  value=\"$i\" $selected >$i</option>";
									}?>
                                    </select>
                                    <select name="ride_day" id="ride_day"  autocomplete="off" class="validate[required]" >
                                      <option  value="">日</option>
                                     <?php 
									 for ($i=1; $i<=31;$i++){
										 if ($i==$selected_day) {
											$selected="selected";
											}
										 else{
											$selected="";
											}
										echo "<option  value=\"$i\" $selected >$i</option>";
									}?> 
                                    </select>
                                    <br />
                                  </div><div class="text word_type_g12">告訴大家您的單車資歷</div></td>
                                </tr>
                              </table>
                          </div>
                          <div class="block">
                              <table width="100%" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td valign="top"><div class="topic">效力車隊*</div></td>
                                  <td width="100%"><div class="text">
                                    <input name="TeamName" type="text" disabled="disabled" id="TeamName" value="<?php echo $TeamName;?>" />
                                   <?php if ($row["IsTeam"]==1) {?> <a href="team_career.html">退出車隊</a><br /> <?php };?>
                                  </div><div class="text word_type_g12">每位車手半年賽季只能更換一次車隊, 請做慎重的決定</div></td>
                                </tr>
                              </table>
                          </div>
                          <div class="box_bottom">
                            <input class="word_type_wb12" type="submit" name="submit" id="submit" value="保存設定" />
                            <input type="hidden" name="From_insert" value="form1" />
							<input type="input" id="id_GeoID" name="id_GeoID" value="<?php echo $GeoID ;?>" />  
                             </div>
                        </div>
                    </form>
                    </div>
                    </td>
                    <td valign="top">
                        <div id="side">
                          <div class="menu">
                          	<div id="visiblebox"></div>
                            <div class="block3">
                              <a href="settings.php" class="word_type_bb14">帳戶資訊</a><br />
  </div>
                          <div class="block2"> <a href="settings_rider.php" class="word_type_bb14">車手檔案</a></div>
                          <div class="block3"> <a href="settings_bike.php" class="word_type_bb14">單車檔案</a><br />
                          </div>
                          <div class="block3"> <a href="settings_team.php" class="word_type_bb14">車隊檔案</a><br />
                          </div>
                          </div>
                          
                        </div>
                	</td>
            	</tr>
			</table>
	  </div>
	<?php require_once('include_footer.php'); ?>	 
</body>
</html>
