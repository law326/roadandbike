<?php
/* -----bobo註解-----
 * module:jQuery-Validation-Engin、jQueryTip、PHP表單驗證檔
 * note:	 
 *
 *
*/
session_start();
require_once('Connections/autologin.php');//判斷訪客狀態(登入、未登入)，再決定您"目前所在的城市"資訊

//●未登入不可瀏覽 
if ($_SESSION['islogin'] == FALSE) header("Location:  index.php");  

$Email = $_SESSION["Email"];

/*DB讀出並判別 性別*/
if ($row["Gender"]==0) {
	$select_Gender=" <option value=\"1\">男</option><option value=\"0\" selected>女</option> ";
}
else{
	$select_Gender=" <option value=\"1\" selected>男</option><option value=\"0\">女</option> ";
}

/*DB讀出並判別 生日*/
$utime=strtotime($row["Birthday"]);
$selected_year =date("Y",$utime);
$selected_month =date("n",$utime);
$selected_day =date("j",$utime);


$editFormAction = $_SERVER['PHP_SELF']; //目前正在執行的檔案
if ((isset($_POST["From_insert"])) && ($_POST["From_insert"] == "form1") ) {
	
	require_once "Connections/PHP_formvalidator.php";//載入PHP表單驗證檔
    /*做這些PHP驗證*/
    $validator = new FormValidator();
    $validator->addValidation("memName","req","請輸入memName");
	
   // $validator->addValidation("email","req","請輸入email");
   // $validator->addValidation("email","email","email格式不正確");
	
    $validator->addValidation("password","req","請輸入password");
    $validator->addValidation("password","eqelmnt=password2","密碼不相符");
    $validator->addValidation("password","alnum","只能輸入英文字母、數字");

	if(!$validator->ValidateForm())	{
	//若驗證失敗進入此區
	 	echo "<B>驗證錯誤:</B>";

        $error_hash = $validator->GetErrors();
        foreach($error_hash as $inpname => $inp_err)
        {
            echo "<p>$inpname : $inp_err</p>\n";
        }        
		exit;
	}


	$memName=$_POST["memName"];
	$Email=$_SESSION["Email"];
	$password_old=$_POST["password_old"];
	$password=$_POST["password"];
	
	//能夠到此 代通過PHP表單驗證 即確認更改後的姓名'密碼不為空白
	$Login__query=sprintf("SELECT MemberPWD FROM tb_members WHERE Email='$Email' AND MemberPWD='$password_old' ");
	$Login = mysql_query($Login__query, $dbConn) or die(mysql_error());

	$loginFoundUser = mysql_num_rows($Login); 
	
	if ($loginFoundUser==1){//帳密正確 執行UPDATE
		$query = "UPDATE  tb_members SET MemberName='$memName' , MemberPWD='$password' where Email='$Email' ";
		mysql_query($query,$dbConn) or die(mysql_error())	;

		header("Location:  $editFormAction");
	}
	else{
		echo "密碼錯誤,請重新輸入";
		//寫一個function 來顯示error msg
	}

}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>RoadandBike - Tour in Your Life</title> 

<meta name="title" content="RoadandBike"> 
 
<meta name="description" content="Tour in Your Life"> 
 
<meta name="keywords" content="軌跡、分享、單車、賽事、免費"> 
<link rel="shortcut icon" type="image/x-icon" href="images/icon.png">
<link href="css/page_style.css" rel="stylesheet" type="text/css" />
<!--停用JavaScript時-->
<noscript>
<META HTTP-EQUIV="Refresh" CONTENT="0;URL=noJavaScript.html">
</noscript>
<!--停用JavaScript時-->

<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/redmond/jquery-ui.css">	
<link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css"/>
<link href="css/tooltip.css" rel="stylesheet" type="text/css" />
<!--所在地區-->
<script src="CollapsiblePanel/CollapsiblePanel.js" type="text/javascript"></script>
<!--所在地區-->
<!--lightbox-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<script type="text/javascript" src="lightbox/custom.js"></script>
<!--lightbox-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
<script src="http://cdn.jquerytools.org/1.2.5/full/jquery.tools.min.js"></script> 
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.11/jquery-ui.min.js"></script>

<script src="js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
</style>
<script>
jQuery(document).ready(function(){
		/* 錯誤顯示位置在 jquery.validationEngine.js promptPosition: "centerLeft"  
	   錯誤訊息框的寬度設定在validationEngine.jquery.css	*/
	jQuery("#form1").validationEngine(); //不可放在國家欄位找值之前 會失效
});
</script>
</head>

<body>
<?php require_once('include_header.php'); ?>	
		<div id="container">
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
            	<tr>
                	<td valign="top">
                    	<div id="main">
                        <div id="title2">
                        	<div class="text word_type_bb24">帳戶資訊</div>
                        </div>
                        <form action="<?php echo $editFormAction; ?>" method="POST" enctype="multipart/form-data" name="form1" id="form1">
						<div id="settings">
						  <div class="block">
                        	  <table width="100%" cellspacing="0" cellpadding="0">
                        	    <tr>
                        	      <td valign="top"><div class="topic">姓名</div></td>
                        	      <td width="100%"><div class="text">
                        	        <input name="memName" type="text" id="memName" value="<?php echo $MemberName ;?>"  class="validate[required] text-input"/>
                        	      </div>
                       	          <div class="text word_type_g12">這將會代表您在出賽時的身分</div></td>
                      	      </tr>
                      	    </table>
                      	  </div>
                       	  <div class="block">
                       	    <table width="100%" cellspacing="0" cellpadding="0">
                       	      <tr>
                       	        <td valign="top"><div class="topic">電子信箱</div></td>
                       	        <td width="100%"><div class="text">
                       	          <input name="email" type="text" id="email" value="<?php echo $Email ;?>" disabled="disabled" />
                   	            </div>
                   	            <div class="text word_type_g12">任何賽事相關訊息都會透過這個郵件來通知</div></td>
                   	          </tr>
                   	        </table>
                   	      </div>
                        <div class="block">
                            <table width="100%" cellspacing="0" cellpadding="0">
                              <tr>
                                <td valign="top"><div class="topic">舊密碼</div></td>
                                <td width="100%"><div class="text">
                                  <input name="password_old" type="text" id="password_old"   class="validate[required] text-input"/>
                              </div></td>
                            </tr>
                          </table>
                        </div>
                       	  <div class="block">
                        	  <table width="100%" cellspacing="0" cellpadding="0">
                        	    <tr>
                        	      <td valign="top"><div class="topic">新密碼</div></td>
                        	      <td width="100%"><div class="text">
                        	        <input name="password" type="text" id="password"   class="validate[required,minSize[6]]  text-input"/>
                      	        </div></td>
                      	      </tr>
                      	    </table>
                      	  </div>
                       	  <div class="block">
                        	  <table width="100%" cellspacing="0" cellpadding="0">
                        	    <tr>
                        	      <td valign="top"><div class="topic">確認密碼</div></td>
                        	      <td width="100%"><div class="text">
                        	        <input name="password2" type="text" id="password2" class="validate[required,equals[password]] text-input" />
                      	        </div></td>
                      	      </tr>
                      	    </table>
                      	  </div>
                        	<div class="block">
                        	  <table width="100%" cellspacing="0" cellpadding="0">
                        	    <tr>
                        	      <td valign="top"><div class="topic">性別</div></td>
                        	      <td width="100%"><div class="text">
                        	        <select name="Gender" id="Gender" disabled="disabled">
                                    <?php echo $select_Gender;	?>
                      	          </select>
                      	        </div></td>
                      	      </tr>
                      	    </table>
                      	  </div>
                        	<div class="block">
                        	  <table width="100%" cellspacing="0" cellpadding="0">
                        	    <tr>
                        	      <td valign="top"><div class="topic">生日</div></td>
                        	      <td width="100%"><div class="text">
               	            <select name="birthday_year" id="birthday_year"  autocomplete="off" disabled="disabled" >
                        	          <option value="">年</option>
                        	         <?php echo "<option  value=\"$selected_year\" selected >$selected_year</option>"; ?>
							</select>
                            <select class="" id="birthday_month" name="birthday_month" disabled="disabled" >
                   	          <option  value="">月</option>
                        	         <?php echo "<option  value=\"$selected_month\" selected >$selected_month</option>"; ?>
                               </select>
                            <select name="birthday_day" id="birthday_day"  autocomplete="off" disabled="disabled" >
                                      <option  value="">日</option>
                        	         <?php echo "<option  value=\"$selected_day\" selected >$selected_day</option>"; ?>                           
                            </select>
                        	      </div></td>
                      	      </tr>
                      	    </table>
                      	  </div>
                        	<div class="block">
							  <table width="100%" cellspacing="0" cellpadding="0">
							    <tr>
							      <td valign="top"><div class="topic">Facebook</div></td>
							      <td width="100%"><div class="text">
							        <img border="0" src="images/fb-login-button.png" width="194" height="25" /><br />
						            
</div>
							      <div class="text word_type_g12">連結您的Facebook帳號, 與朋友分享您的車手生涯</div></td>
						        </tr>
						      </table>
						  </div>
                       	  <div class="box_bottom">
                            <input class="word_type_wb12" type="submit" name="submit" id="submit" value="保存設定" />
                            <input type="hidden" name="From_insert" value="form1" />
                          </div>
                        </div>
                    </form>
                    </div>
                    </td>
                    <td valign="top">
                        <div id="side">
                          <div class="menu">
                          	<div id="visiblebox"></div>
                            <div class="block2">
                              <a href="settings.php" class="word_type_bb14">帳戶資訊</a></div>
                          <div class="block3"> <a href="settings_rider.php" class="word_type_bb14">車手檔案</a></div>
                          <div class="block3"> <a href="settings_bike.php" class="word_type_bb14">單車檔案</a><br />
                          </div>
                          <div class="block3"> <a href="settings_team.php" class="word_type_bb14">車隊檔案</a><br />
                          </div>
                          </div>
                          
                        </div>
                	</td>
            	</tr>
			</table>
	  </div>
<?php require_once('include_footer.php'); ?>	 
</body>
</html>
