<?php
/* 給GeoID找出對應的經緯度
input: GeoID
output: $latlng  (lat,lng)
*/
function geo_to_latlng ($GeoID)
{	
switch ($GeoID){
	case 1://Keelung
	return '25.108981,121.708145';

	case 2://Taipei
	return '25.036150,121.520119';

	case 3://Taoyuan
	return '24.988889,121.311111';

	case 4://Hsinchu
	return '24.803946,120.964687';

	
	case 5:// Miaoli
	return '24.489269,120.941737';


	case 6://Taichung
	return '24.233208,120.941737';


	case 7://Changhua
	return '24.066667 ,120.533333 ';

	
	case 8://Nantou
	return '23.916667 ,120.683333 ';

	
	case 9://Yunlin
	return '23.755852 ,120.389665 ';

	
	case 10://Chiayi
	return '23.475449 ,120.447285 ';


	case 11://Tainan
	return '23.141699 ,120.251273 ';

	
	case 12://Kaohsiung
	return '22.626687  ,120.395050';

	
	case 13://Pingtung
	return '22.676111 ,120.494167';

	
	case 14://Yilan
	return '24.750000 ,121.750000 ';

	
	case 15://Hualien
	return '23.756899 ,121.354163 ';

	
	case 16://Taitung
	return '22.758333 ,121.144444 ';


	case 17://Penghu
	return '23.565479 ,119.615143 ';

	
	case 18://Kinmen
	return '24.451968 ,118.357883 ';

	
	case 19://Lienchiang
	return '26.149396,119.938314';

	
	case 20://Hong Kong Island
	return '22.247860 ,114.203384 ';

	
	case 21://Kowloon
	return '22.318567 ,114.179606 ';


	case 22://New Territories
	return '22.406083 ,114.120154 ';

	
	default:
	return '25.036150,121.520119';
	}

}

?>