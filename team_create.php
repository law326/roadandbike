<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php
$page = "teamcreate";
require_once('include_webtitle.php');//標題檔
$pagestyle = "setting";//提供header樣式判斷
?>
<?php
/* -----bobo註解-----
 * module:jQuery-Validation-Engin、jQueryTip、PHP表單驗證檔
 * note:	成立地點由創建者所屬國家的城市選取
 * 			車隊名稱TeamName是否重複(AJAX判斷)			 
 *
 *
*/
session_start();

require_once('Connections/autologin.php');//判斷訪客狀態(登入、未登入)，再決定您"目前所在的城市"資訊
require_once('Connections/find_GeoID.php');//function GeoID相關

//●未登入不可瀏覽 
if ($_SESSION['islogin'] == FALSE) header("Location:  index.php");  

$Mode = "create";

// 取此檔所在位置
$directory_self = str_replace(basename($_SERVER['PHP_SELF']), '', $_SERVER['PHP_SELF']);

// 上傳處理的位置
$uploadHandler = 'http://' . $_SERVER['HTTP_HOST'] . $directory_self . 'upLoadImg/upLoadImg_process.php';

$max_file_size = 100000; // 限100KB , 硬性則要修改php.ini

/* 若不是自由車手，不給進入 為0者表示tb_members為自由車手  才可以建立車隊 */
if ($row["IsTeam"] > 0) header("Location:  team.php");

if(!empty($_GET['IMG']))$TeamImg = "upLoadImg/team_pic/".$_GET['IMG'];
if (empty($TeamImg)) $TeamImg="images/TeamImg_default.jpg";

$editFormAction = $_SERVER['PHP_SELF']; //目前正在執行的檔案
if ((isset($_POST["From_insert"])) && ($_POST["From_insert"] == "form1") ) {

if(!empty($_POST['IMG'])){
	
	$TeamImg = "upLoadImg/team_pic/".$_POST['IMG'];

	require_once "Connections/PHP_formvalidator.php";//載入PHP表單驗證檔
    /*做這些PHP驗證*/
    $validator = new FormValidator();
    $validator->addValidation("email","req","請輸入車隊名稱");
	
    $validator->addValidation("TeamCreate_year","num","請輸入TeamCreate_year");	
		
	$validator->addValidation("City","req","請輸入City");	
	$validator->addValidation("City","alnum_s","只能輸入英文字母、數字(准許空白)");	
	
	
	if(!$validator->ValidateForm())	{
	//若驗證失敗進入此區
	 	echo "<B>驗證錯誤:</B>";

        $error_hash = $validator->GetErrors();
        foreach($error_hash as $inpname => $inp_err)
        {
            echo "<p>$inpname : $inp_err</p>\n";
        }        
		exit;
	}
	
	
	/*表單驗證合格才會執行下面*/
	
	$email=$_POST["email"]; //車隊名稱
	$Email=$_SESSION["Email"];
	

	list($GeoID) = city_to_geoid( $_POST["City"] );//user輸入城市轉換成GeoID
	if (empty($GeoID)) die('GeoID not exist!');

	/* 執行日期轉換 由utime轉成 MySQL Date Format 2010-02-10 */
	$TeamDate = $_POST['TeamCreate_year']; //車隊建立日期
	//如果年-月-日輸入的不完整,轉換出來就會錯誤! 變成1970-01-01


	if ($row["AccessLevel"] > 1) { //參賽資料填畢,准許建立車隊
		
		/* 檢查車隊名稱TeamName是否重複(不分地區,名稱只能有一組同名) */
		if (empty($email)) die('車隊名稱變數$email不存在');
		$query_IDtestRec = sprintf("SELECT TeamName FROM  tb_team WHERE TeamName = '$email'");
		$IDtestRec = mysql_query($query_IDtestRec, $dbConn) or die(mysql_error());
		$totalRows_IDtestRec = mysql_num_rows($IDtestRec);//回傳資料集中資料筆數 
		
		if ($totalRows_IDtestRec==0){//車隊名稱是否有重複
			//搜尋到0筆 表示此車隊名稱可用

			if (empty($MemberID)) die('車隊名稱變數不存在');

			/* 開始建立車隊 */
			// 創造車隊 插入主表tb_team
			$insertSQL = sprintf("INSERT INTO tb_team (TeamName, CreatorID, TeamDate ,GeoID, Status, TeamImg) 
									VALUES ('%s', '%s', '%s','%s','%s','%s') " , $email, $MemberID  ,$TeamDate ,$GeoID, 1, $TeamImg );
			$result = mysql_query($insertSQL,$dbConn) or die(mysql_error());	
			if ($result){
				//插入成功
				//取得建立的車隊編號TeamID 使用mysql_insert_id()
				
				$TeamID = mysql_insert_id();
				
				/* 寫入車隊隊員 tb_team_attendee→Status=2幹部以及CreatorID */
				$now = date ("Y-m-d H:i:s"); 
				$insertSQL = sprintf("INSERT INTO tb_team_attendee (TeamID, MemberID, JoinDate ,Status) 
									VALUES ('%s', '%s', '%s','%s') " , mysql_insert_id(), $MemberID  ,$now ,2);
				$result = mysql_query($insertSQL,$dbConn) or die(mysql_error());	
				
				$TeamAttendeeID = mysql_insert_id();
								
				/*update 創建者狀態 tb_members→IsTeam=1*/
				$query = "UPDATE tb_members  SET IsTeam=1  WHERE MemberID='$MemberID' ";
				$result_update =  mysql_query($query,$dbConn) or die(mysql_error())	;

//寫入車隊成立事件
$insertSQL = sprintf("
	INSERT INTO tb_event 
	(EventType, MemberID, GeoID, BikeID, TeamID, MemberCareerID, TeamCareerID, TeamAttendeeID ,FollowMemberID, FollowTeamID, EventDate)
	VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s') " , 
		21, 
		$MemberID,
		$GeoID,
		0,
		$TeamID,
		0, 
		0,
		$TeamAttendeeID, 
		0,
		0,
		$now);
					
	$result = mysql_query($insertSQL) or die(mysql_error());

				header("Location:  team.php");

		}
		else{
			echo "車隊名稱已被使用,請重新輸入"; //車隊名稱已被使用,不能寫入	
			exit;
		}//車隊名稱是否有重複End
		
	}
	else
	{  
		die('參賽資料尚未填畢,不可建立車隊')	;
	}//參賽資料尚未填畢,不可建立車隊
		
	

	}
	$editFormAction = $editFormAction."?OK=2";
	header("Location:  $editFormAction");//重新載入page
	
}else{//沒有照片

	$editFormAction = $editFormAction."?OK=1";
	header("Location:  $editFormAction");//重新載入page

}
}
?>
<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/redmond/jquery-ui.css">	
<link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css"/>
<link href="css/tooltip.css" rel="stylesheet" type="text/css" />
<!--所在地區-->
<script src="CollapsiblePanel/CollapsiblePanel.js" type="text/javascript"></script>
<!--所在地區-->
<!--lightbox-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<script type="text/javascript" src="lightbox/custom.js"></script>
<!--lightbox-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
<script src="http://cdn.jquerytools.org/1.2.5/full/jquery.tools.min.js"></script> 
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.11/jquery-ui.min.js"></script>

<script src="js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
<!--geo-->
<script>
$(document).ready(function(){

	//直接從創建者的國籍 AJAX帶入候選城市

	$("#City").focus(function() {
		// 會依據國家欄位的值 去判斷 有哪些city
		var Country_city = "signup_CountryCity.php?kind=City&City_Country=<?php echo $Country;?>" ;
			$("#City").autocomplete({
			source: Country_city ,
			minLength: 0,
			select: function(event, ui) {
				$('#id_GeoID').val(ui.item.id); //將GeoID值存到hidden 方便傳送
			}
		});
	});							//國家城市欄位找值--------------------
	
	$('#email').click(function (){ //將網頁載入時預設在#City的值利用AJAX轉換成GeoID
		   $.ajax({
		   url: 'signup_CountryCity.php',
		   cache: false,
		   dataType: 'text', //dataType 預計會從url回傳的dataType  類型有4種：html,xml,json,text
			   type:'GET',
		   data: { kind: 'City' ,City_Country:'<?php echo $Country;?>' ,term: $('#City').val()},//帶過去url的變數名稱
		   error: function(xhr) {
			 alert('Ajax request 發生錯誤');
		   },
		   success: function(response) {// request succeeds的回傳值
		   var jsonContent = eval("(" + response + ")");//將JSON文字轉成物件
			 $('#id_GeoID').val(jsonContent[0].id);
		   }
		   
	   });
	});
		/* 錯誤顯示位置在 jquery.validationEngine.js promptPosition: "centerLeft"  
	   錯誤訊息框的寬度設定在validationEngine.jquery.css	*/
	jQuery("#form1").validationEngine(); //不可放在國家欄位找值之前 會失效
});
</script>

<script language="javascript">
$(document).ready(function()//判斷車隊名稱是否已被註冊
{
 
	$("#email").blur(function()
	{
		if(document.getElementById("email").value.length > 1){//if email字元大於1再做AJAX確認帳號
			//remove all the class add the messagebox classes and start fading
			$("#msgbox").removeClass().addClass('messagebox').text('Checking...').fadeIn("slow");
			//check 帳號是否存在
			$.post("team_chkID.php",{ user_name:$(this).val() } ,function(data)
			{
			  if(data=='no') //if 帳號已被使用
			  {
					$("#msgbox").fadeTo(200,0.1,function() //fading訊息效果
					{ 
					  //加入訊息 and change the class of the box and start fading
					  document.getElementById("idErrMsg").innerHTML = "<img src='images/block1.gif' /><font color='red'> 此車隊名稱已被使用!!</font>";
					  $('input[type="submit"]').attr('disabled','disabled');//submit不讓他送出
					});		
			  }
			  else
			  {
				$("#msgbox").fadeTo(200,0.1,function() //fading訊息效果
				{ 
					  //加入訊息 and change the class of the box and start fading
					  document.getElementById("idErrMsg").innerHTML = "<img src='images/apply2.gif' /><font color='green'> 恭喜,此車隊名稱可使用!</font>";
						 $('input[type="text"]').blur(function(){//把submit的disabled拿掉
									if($(this).val() != ''){
									   $('input[type="submit"]').removeAttr('disabled');
									}
						 });
				});
			  }
       		 });
		}//if email字元大於6
	});
});
</script>
</head>

<body>
<div id="signup">
<?php require_once('include_header.php'); ?>
		<div id="container">
                                   	<div id="main">
                        <div id="title" style="	border-bottom-color:#ccc;
	border-bottom-style:dotted;
	border-bottom-width:1px;">
                        	<div class="text word_type_bb24">成立車隊</div>
                        </div>
                         <?php
if(empty($_GET['OK'])){
}else if($_GET['OK'] == 1){
?>
<div style="background:#f8f8f8; padding:5px; font-weight: bold; margin-bottom:5px;" align="center" class="word_type_r12">存取失敗</div>
<?php
}else if($_GET['OK'] == 2){
?>
<div style="background:#f8f8f8; padding:5px; font-weight: bold; margin-bottom:5px;" align="center" class="word_type_green12" >存取成功</div>
<?php
}
?>
                        <form action="<?php echo $editFormAction; ?>" method="POST" enctype="multipart/form-data" name="form1" id="form1">
						<div id="settings">
						  <div class="block">
                            <table width="100%" cellspacing="0" cellpadding="0">
                              <tr>
                                <td valign="top"><div class="topic">車隊照片</div></td>
                                <td width="100%"><div class="picb"><img border="0" src="<?php echo $TeamImg;?>" width="100%" /></div>
                                  <div class="text word_type_g12"><a class="popup" href="#" rel="popupre">變更照片</a></div>
                                  <div class="text word_type_g12">建議使用方形200px*200px以上<br />
                                  並小於100KB之.jpg或.gif圖檔</div></td>
                              </tr>
                            </table>
                          </div>
                          <div class="block">
                          <table width="100%" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td valign="top"><div class="topic">
                                    車隊名稱
                                  </div></td>
                                  <td width="100%"><div class="text">
                                    <input name="email" type="text"  class="validate[required] text-input" id="email"/>
                                 <span id="idErrMsg"> </span><span id="msgbox" style="display:none"></span>
                                  </div></td>
                                </tr>
                              </table>
                          </div>
                            <div class="block">
                              <table width="100%" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td valign="top"><div class="topic">成立時間</div></td>
                                  <td width="100%"><div class="text">
                                  <select name="TeamCreate_year" id="TeamCreate_year"  autocomplete="off"   class="validate[required]">
                        	          
 									<?php 
									 for ($i=1920; $i<=date("Y");$i++){
										echo "<option  value=\"$i\" >$i</option>";
									}?>
                      	          </select>
                                  </div></td>
                                </tr>
                              </table>
                            </div>
                            <div class="block">
                                  <table width="100%" cellspacing="0" cellpadding="0">
                                    <tr>
                                      <td valign="top"><div class="topic">
                                        成立地點
                                      </div></td>
                                      <td width="100%"><div class="text">
                                        <input name="City" type="text" id="City" title="您所在的城市(或欄位空白時按↓自動選擇)" value="<?php echo $City;?>" class="validate[required,minSize[2],custom[onlyLetterSp]] text-input" />
                                      </div></td>
                                    </tr>
                                  </table>
                                </div>
                                 <!--隱藏欄位 傳值 id_GeoID-->
							<input type="hidden" id="id_GeoID" name="id_GeoID"  />  
                             <input type="hidden" id="IMG" name="IMG" value="<?php echo $_GET['IMG']; ?>"  />                             
                             <input type="hidden" name="From_insert" value="form1" />
                                                         
                          <div class="box_bottom">
                            <input class="word_type_wb12" type="submit" name="submit" id="submit" value="送出" />
                          </div>
                        </div>
                    </form>
                    </div>
	  </div> 
      </div>
      </div>
      <!--lightbox_upload-->
	<div id="popupre" class="popupbox">
		<form name="form1" id="form1" action="<?php echo $uploadHandler; ?>?Mode=<?php echo $Mode; ?>" enctype="multipart/form-data" method="post" >
			<div class="back_sb"></div>
  			<div id="login">
            	<div class="box_top word_type_bb18">
                <?php
				if($Mode == "bike"){
	echo "單車";
}else if($Mode == "team"){
	echo "車隊";
}else{
	echo "車手";
}
                ?>照片</div>
            	<div class="text">上傳圖片<br />
            		<input id="file" type="file" name="file">               
				</div>
                <div class="box_bottom">
                	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                    	<tr>
							<td align="right">
								<input class="word_type_wb12" type="submit" name="submit" id="submit" value="上傳" />
								
							</td>
						</tr>
					</table>
				</div>
			</div>
    	</form>
	</div>
    <div id="fade"></div>
<!--lightbox_upload-->
</body>
</html>
